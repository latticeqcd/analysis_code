#!/usr/bin/env python3
"""Renormalization of the vector current Z_v."""


import argparse
import numpy as np
import json  # json.dumps()
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
import matplotlib.pyplot as plt
import lmfit as lm
import mass as ms
from tqdm import tqdm
from uncertainties import ufloat, ufloat_fromstr, unumpy
import inspect
import tabulate
import importlib

upper_right, upper_left, lower_left, lower_right = 1, 2, 3, 4


def module_exist(module):
    r"""Check whether the module can be found.

    Parameters
    ----------
    module : str
        String containing the modules name

    Returns
    -------
    bool
        Whether the module exists or not

    """
    return importlib.util.find_spec(module) is not None


def new_print(*args, **kwargs):
    r"""Define a new print function.

    This function as a replacement for the original print function tries first
    to write using `tqdm.write`, else uses legacy `print`.

    Other Parameters
    ----------------
    args : dict
        Positional arguments to be passed to print()
    kwargs : dict
        Keyword arguments to be passed to print()

    """
    try:
        tqdm.write(*args, **kwargs)
    except NameError:
        old_print(*args, ** kwargs)


# globaly replace print with new_print
old_print = print
inspect.builtins.print = new_print


def observable_average_ufloat(
    data,
    fit_range,
    mode='full',
    cov=None,
    verbosity=0,
    reference=ufloat(0.0, 0.0),
):
    r"""Calculate the observable for Z_v.

    The observable averages first, then uses lmfit to determin Z_v with a
    one-parameter fit to a constant, using a covariance matrix with some degree
    of freedom.

    Parameters
    ----------
    data : array_like
        The input data
    fit_range : list[int]
        A list of integers describing the fit-range
    mode : string, optional
        Mode for the covarance matrix (see `covariance_matrix()`)
    cov : tuple, optional
        Tuple wit 3 matrices; the covariance matrix and its inverse and the
        sqrt of the inverse
    verbosity : int, optional
        Level of verbosity
    reference : ufloat, optional
        Ufloat with a reerence value

    Returns
    -------
    tuple
        A tuple containing the result and an info dictionary
    result : tuple[ufloat]
        A tuple containing the result as ufloat
    info_dict : dict
        Information about the calculation, i.e. chi-squared values, ...
    """
    ob = f'Observable (Z_v): average, lmfit, full Z_v determination with\n' \
         f'one-parameter fit to a constant, {mode} covariance matrix, '\
         f'd.o.f. = {len(fit_range)-1}'
    if verbosity >= 2:
        print(ms.blue(ob))

    data1 = data[:, :, 0]  # G1
    data2 = data[:, :, 1]  # G2
    ratio_mean = np.nanmean(data1, axis=0)/np.nanmean(data2, axis=0)

    if cov is None:
        _, _, sqrt_mcovinv = ms.covariance_matrix(
            data1/data2, mode=mode, sqrt=True, verbosity=verbosity)
    else:
        (_, _, sqrt_mcovinv) = cov

    fit_params = lm.Parameters()
    fit_params.add(
        'Z_v',
        value=0.7 if reference.n == 0.0 else reference.n,
        min=np.finfo(np.float64).eps,
        max=1.0
    )

    def residual(params, data, scovinv, frange, model):
        vals = params.valuesdict()
        return scovinv.dot(data - model(frange, vals['Z_v']))

    res = lm.minimize(
        residual,
        fit_params,
        method='nelder',
        nan_policy='omit',
        kws={
            'data': ratio_mean,  # ydata
            'scovinv': sqrt_mcovinv,  # sqrt of inverse covariance matrix
            'frange': fit_range,  # fit range
            'model': lambda nt, Z_v: Z_v  # constant hypothesis function with
                                          # one parameter
        },
        max_nfev=200
    )

    if verbosity >= 2:
        print(lm.fit_report(res, modelpars=fit_params))
        print(ms.format_chisq(res.redchi.real))

    it = iter(res.params.items())
    _, Z_v = next(it)
    Z_v = ufloat(
        np.abs(Z_v.value.real),
        np.abs(0.0 if Z_v.stderr is None else Z_v.stderr),
        "current renormalization Z_v fit-error"
    )

    info_dict = {
        'ob': ob,
        'redchi': res.redchi.real,
        'lmfit_result': res,
    }

    return (Z_v,), info_dict


def main(args):
    r"""Run the observable.

    The observable averages first, then uses lmfit to determin Z_v with a
    one-parameter fit to a constant, using a covariance matrix with some degree
    of freedom.

    Parameters
    ----------
    args : argparse.ArgumentParser
        An argparse object containing the config

    Returns
    -------
    tuple
        A tuple containing the result dictionary and plot data
    result_dict : dict
        Key-value painrs of results
    plot_data : dict
        Key-value painrs needed for plotting
    """
    if args.verbosity:
        print("Running with the following arguments:")
        print(json.dumps(vars(args), default=str, indent=4))

    # reading all the infiles
    G1, (N0, N1, N2, N3), _, nconfig1 = ms.read_infiles(
        args.infile1,
        verbose=args.verbosity
    )
    G2, (N02, N12, N22, N32), _, nconfig2 = ms.read_infiles(
        args.infile2,
        verbose=args.verbosity
    )

    if (N0 != N02 or N1 != N12 or N2 != N22 or N3 != N32):
        raise ValueError('The dimensions of the lattices do not match.')

    # average all nsrc sources of a given config
    if args.verbosity:
        print("G1.shape = {0} (initial)".format(G1.shape))
        print("G2.shape = {0} (initial)".format(G2.shape))

    # average all nsrc sources of a given config
    G1, Gerr1 = ms.avg_sources(G1, args.nsrc)
    G2, Gerr2 = ms.avg_sources(G2, args.nsrc)
    if args.verbosity:
        print("G1.shape = {0} (after averaging over sources)".format(G1.shape))
        print("G2.shape = {0} (after averaging over sources)".format(G2.shape))

    # fold according to folding variant
    G1, Gerr1, N0, _, _, _, _, fold_axis = ms.fold(G1, args.fold, N0, Gerr=Gerr1)
    G2, Gerr2, N0, _, _, _, _, fold_axis = ms.fold(G2, args.fold, N02, Gerr=Gerr2)
    if args.verbosity:
        print("G1.shape = {0} (after folding)".format(G1.shape))
        print("G2.shape = {0} (after folding)".format(G2.shape))

    # ratio = (G1/G2).real

    if args.vary_range is not None:
        fit_ranges = []
        data_ranges = []
        for i in range(args.vary_range[0], args.vary_range[1]+1):
            lower = i+args.vary_range[2]-1
            upper = np.minimum(int(fold_axis), args.vary_range[1]+1)
            for j in range(lower, upper):
                r = ms.obtain_range([i, j], args.exclude)
                if len(r) >= args.vary_range[2]:
                    fit_ranges.append(r)
                    data = np.zeros(G1[:, r].shape + (2,))
                    data[:, :, 0] = G1[:, r].real
                    data[:, :, 1] = G2[:, r].real
                    data_ranges.append(data)

        def observable_to_vary(data, r):
            return observable_average_ufloat(
                data,
                r,
                mode=args.mode,
                verbosity=args.verbosity,
                reference=args.ref,
            )

        # extracts meff, A, and their reduced chi^2 values in a list
        def aggregate(i, retval, *parameters):
            (Z_v,) = retval[0]
            r = parameters[1]
            redchi = retval[1]['redchi']
            return [i+1, r, Z_v, redchi, np.abs(1-redchi)]

        # prints a table and returns the error of meff and A as ufloats
        def collect(retval, chosen_range):
            retval = np.array(retval, dtype=object)
            sigma_Zv = np.nanstd([m.n for m in retval[:, 2]])

            min_idx = np.argmin(retval[:, -1])
            retval[min_idx] = [ms.green(item) for item in retval[min_idx]]

            table = tabulate.tabulate(
                retval,
                headers=["#", "range", "Z_v", "redchi", "norm"]
            )
            table += f'\nSystematic error coming from ' \
                     f'the choice of the range: {sigma_Zv = }'
            tag = "sys: choice of fit-range"
            return ufloat(0.0, sigma_Zv, tag), table

        Z_v_range_err, table_range = ms.vary_parameter(
            data_ranges,
            fit_ranges,
            f=observable_to_vary,
            vary_param="range",
            aggregate=aggregate,
            collect=lambda r: collect(
                r,
                chosen_range=ms.obtain_range(args.range, args.exclude)
            )
        )
    else:
        Z_v_range_err = ufloat(0.0, 0.0, "sys: choice of fit-range")

    # @var list set of all point in the range, except the excluded ones
    r = ms.obtain_range(args.range, args.exclude)
    data = np.zeros(G1[:, r].shape + (2,))
    data[:, :, 0] = G1[:, r].real
    data[:, :, 1] = G2[:, r].real
    C = ms.covariance_matrix(
        G1[:, r].real/G2[:, r].real,
        mode=args.mode,
        sqrt=True,
        verbosity=args.verbosity
    )

    # @choice: with observable?
    def observable(data):
        return observable_average_ufloat(
            data,
            r,
            mode=args.mode,
            cov=C,
            verbosity=args.verbosity,
            reference=args.ref
        )

    # calling of all the methods
    (Z_v,), info = observable(data)
    Z_v += Z_v_range_err

    result_dict = {
        "nconfig1": int(nconfig1/args.nsrc),
        "nconfig2": int(nconfig2/args.nsrc),
        "Z_v (error prop)": ms.error_contributions_dict(
            Z_v,
            {'chisq': info['redchi']}
        ),
    }

    if args.jackknife:
        (Z_v_jn,), info_jn = ms.jackknife(data, observable)
        Z_v_jn += Z_v_range_err
        chisq_jn = np.mean([infok['redchi'] for infok in info_jn])
        result_dict['Z_v (jackknife)'] = ms.error_contributions_dict(
            Z_v_jn,
            {'chisq': chisq_jn}
        )
    if args.bootstrap != 0:
        (Z_v_bs,), info_bs = ms.bootstrap(data, observable, args.bootstrap)
        Z_v_bs += Z_v_range_err
        chisq_bs = np.mean([infok['redchi'] for infok in info_bs])
        result_dict['Z_v (bootstrap)'] = ms.error_contributions_dict(
            Z_v_bs,
            {'chisq': chisq_bs}
        )

    # print information
    text = ''
    text += f'Ren. const. Z_v (mean)               = {Z_v:.10f} ' \
            f'({ms.format_chisq(info["redchi"])})\n'

    if args.jackknife:
        text += f'Ren. const. Z_v (jackknife)          = {Z_v_jn:.10f} ' \
                f'({ms.format_chisq(chisq_jn)})\n'
    if args.bootstrap != 0:
        text += f'Ren. const. Z_v (bootstrap, K={args.bootstrap:>4})  = ' \
                f'{Z_v_bs:.10f} ({ms.format_chisq(chisq_bs)})\n'

    if args.ref != 0.0:
        text += f'Ren. const. Z_v (reference)          = ' \
                f'({args.ref:.10f}) x 10^-10\n'
        text += "discrepancy (compared to mean)       = {0} sigma ({1})\n"\
            .format(
                ms.format_sigma(
                    np.abs(Z_v[0]-args.ref)/np.sqrt(args.ref.s**2 + Z_v[1]**2),
                    '{0:.1f}'
                ),
                (
                    ms.red('no overlap')
                    if np.abs(Z_v[0]-args.ref) > args.ref.s+Z_v[1]
                    else ms.green('overlap')
                ) + ' with ref.'
            )
        if args.jackknife:
            text += "discrepancy (compared to jackknife)  = {0} sigma ({1})\n"\
                .format(
                    ms.format_sigma(
                        np.abs(Z_v_jn.n-args.ref)/np.sqrt(args.ref.s**2 + Z_v_jn.s**2),
                        '{0:.1f}'
                    ),
                    (
                        ms.red('no overlap')
                        if np.abs(Z_v_jn.n-args.ref) > args.ref.s+Z_v_jn.s
                        else ms.green('overlap')
                    ) + ' with ref.'
                )
        if args.bootstrap != 0:
            text += "discrepancy (compared to bootstrap)  = {0} sigma ({1})\n"\
                .format(
                    ms.format_sigma(
                        np.abs(Z_v_bs.n-args.ref)/np.sqrt(args.ref.s**2 + Z_v_bs.s**2),
                        '{0:.1f}'
                    ),
                    (
                        ms.red('no overlap')
                        if np.abs(Z_v_bs.n-args.ref) > args.ref.s+Z_v_bs.s
                        else ms.green('overlap')
                    ) + ' with ref.'
                )

    title = 'Vector current renormalization constant Z_v for {0}\n{1}'\
        .format(ms.bold(args.name), info['ob'])

    _, _, fprint = ms.get_target(args.outfile, '.txt')

    if args.vary_range is not None:
        fprint("Variation table for fit-range")
        fprint(table_range, end='\n\n')

    fprint(ms.box([text], title=title))
    fprint(ms.error_contributions(Z_v, 'Z_v (mean)'))
    if args.jackknife:
        fprint(ms.error_contributions(Z_v_jn, 'Z_v (jackknife)'))
    if args.bootstrap != 0:
        fprint(ms.error_contributions(
            Z_v_bs,
            f'Z_v (bootstrap, K={args.bootstrap})')
        )

    data1_mean = unumpy.uarray(np.nanmean(G1.real, axis=0),
                               np.nanstd(G1.real, axis=0))
    data2_mean = unumpy.uarray(np.nanmean(G2.real, axis=0),
                               np.nanstd(G2.real, axis=0))
    ratio_mean = data1_mean/data2_mean
    X = np.arange(0, int(N0))

    plot_data = {
        'N0': N0,
        'ratio_mean': ratio_mean,
        'X': X,
        'Z_v': Z_v,
        'ratio_mean': ratio_mean,
    }
    if args.bootstrap != 0:
        plot_data['Z_v_bs'] = Z_v_bs
    if args.jackknife:
        plot_data['Z_v_jn'] = Z_v_jn

    return result_dict, plot_data


def plot(args, plot_data):
    r"""Plot the data.

    Parameters
    ----------
    args : argparse.ArgumentParser
        An argparse object containing the config
    plot_data : dict
        Key-value painrs needed for plotting

    Returns
    -------
    bool
        True
    """
    N0 = plot_data['N0']
    ratio_mean = plot_data['ratio_mean']
    X = plot_data['X']
    Z_v = plot_data['Z_v']
    ratio_mean = plot_data['ratio_mean']
    if args.bootstrap != 0:
        Z_v_bs = plot_data['Z_v_bs']
    if args.jackknife:
        Z_v_jn = plot_data['Z_v_jn']

    r = ms.obtain_range(args.range, args.exclude)
    title = f"Vector current renormalization constant Z_v for {args.name}"

    fig = plt.figure()
    plt.title(ms.clean(title))

    gs = fig.add_gridspec(2, 1, height_ratios=[3, 1])
    ax = fig.add_subplot(gs[0, 0])

    axins = inset_axes(
        ax,
        width="100%",
        height="100%",
        bbox_to_anchor=(0.4, -0.5, 0.6, 0.4),  # (left, bottom, width, height)
        bbox_transform=ax.transAxes,
        loc=upper_left,
        borderpad=0
    )
    axes = [ax, axins]

    eps = 0.1
    for axis in axes:
        axis.axvspan(
            args.range[0]-eps, args.range[-1]+eps,
            alpha=0.1,
            color='green',
            label='fit region'
        )

        axis.errorbar(X, unumpy.nominal_values(ratio_mean),
                      yerr=unumpy.std_devs(ratio_mean),
                      fmt='-',
                      color='red',
                      label='correlator ratio',
                      capsize=6
                      )

        axis.plot([0, N0], [Z_v.n, Z_v.n], '--',
                  color='green',
                  label='constant fit (mean)'
                  )
        axis.fill_between(
            [0, N0],
            Z_v.n - Z_v.s,
            Z_v.n + Z_v.s,
            alpha=0.2,
            label='constant fit (mean)'
        )

        if args.jackknife:
            axis.plot([0, N0], [Z_v_jn.n, Z_v_jn.n], '-.',
                      color='blue',
                      label='constant fit (jackknife)'
                      )
            axis.fill_between(
                [0, N0],
                Z_v_jn.n - Z_v_jn.s,
                Z_v_jn.n + Z_v_jn.s,
                alpha=0.2,
                label='constant fit (jackknife)'
            )

        if args.bootstrap != 0:
            axis.plot([0, N0], [Z_v_bs.n, Z_v_bs.n], '.',
                      color='black',
                      label=f'constant fit (bootstrap K={args.bootstrap})'
                      )
            axis.fill_between(
                [0, N0],
                Z_v_bs.n - Z_v_bs.s,
                Z_v_bs.n + Z_v_bs.s,
                alpha=0.2,
                label=f'constant fit (bootstrap K={args.bootstrap})'
            )

        axis.set_xlabel(r'$n_0$ [lattice units]')

    # sub region of the original image
    x1, x2 = ms.limits(args.range, epsilon=1.1, no_zeros=False)
    y1, y2 = ms.limits(ratio_mean[r], args.ref, Z_v, shift=0.1)
    axins.set_xlim(x1, x2)
    axins.set_ylim(y1, y2)

    # draw a bbox of the region of the inset axes in the parent axes and
    # connecting lines between the bbox and the inset axes area
    mark_inset(
        ax, axins, loc1=upper_left, loc2=upper_right,
        fc="none", ec="0.5"
    )

    h, leg = ms.compactify_legend([ax, axins])
    ax.legend(h, leg)

    target, file, _ = ms.get_target(args.outfile, '.pdf')
    if file:
        plt.savefig(target)
    else:
        plt.draw()
        plt.show()

    return True


if __name__ == '__main__':
    ufloat_fmt_info = 'the number can have different formats, for all possibilities, see https://uncertainties-python-package.readthedocs.io/en/latest/user_guide.html#creating-numbers-with-uncertainties'  # noqa: E501

    parser = ms.JsonArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='Renormalization constant Z_v of the vector currents '
                    'generated by meson.c.',
        plot_function=plot,
        exceptions=['infile1', 'infile2', 'outfile'],
        main_result=['result.Z_v (jackknife).value&.3uS?Z_v (jackknife)'],
    )
    parser.add_argument(
        "-v", "--verbosity", action="count", help="increase output verbosity",
        default=0
    )
    parser.add_argument(
        '-n', '--name', help='name of the contribution', default='full'
    )
    parser.add_argument(
        '--no-plot', help="don't show the plot", dest='noplot', default=False,
        action="store_true"
    )
    parser.add_argument(
        '--nsrc', help='number of sources used', type=int, default=1
    )
    parser.add_argument(
        '--fold', help='See help text of mass.py', type=int, default=0,
        choices=[0, 1, 2, 3, 4]
    )
    parser.add_argument(
        '-e', '--exclude', help='list of indices to exclude from the fits.',
        type=int, nargs='+', default=[]
    )
    parser.add_argument(
        '-m', '--mode',
        help='mode for the covariance matrix. Can be either full:n, diag:n, '
             'or normalized:n, where n denotes the number of lowest '
             'eigenvalues to truncate. n can also be -1, which cut all '
             'negative eigenvalues automatically. (default: diag:0)',
        default='diag:0'
    )
    parser.add_argument(
        '-j', '--jackknife',
        help='calculate the result using the jackknife method', default=False,
        action="store_true"
    )
    parser.add_argument(
        '-b', '--bootstrap',
        help='calculate the result using the bootstrap method. The integer '
             'value gives the number of bootstrap samples to take.',
        type=int, default=0
    )
    parser.add_argument(
        '--ref',
        help=f'a reference value and its error for Z_v ({ufloat_fmt_info}).',
        type=lambda x: ufloat_fromstr(x, "reference value Z_v"),
        default="0.0+/-0.0"
    )
    required = parser.add_argument_group('required named arguments')
    required.add_argument(
        '-r', '--range',
        help='range for the fit [A, B].',
        type=int,
        nargs=2,
        metavar=('A', 'B'),
        required=True
    )
    required.add_argument(
        '--vary-range',
        help='How much should the range be varied in order to obtain the '
             'systematic error that appears from its choice. Expects 3 '
             'numbers: A B C, A and B are the lower and upper end of the '
             'variation (including), and C denotes the minimal number of '
             'points that should be contained in a range.',
        type=int, nargs=3, metavar=('A', 'B', 'C'), default=None
    )
    required.add_argument(
        '-i1', '--in1',
        help='input file(s) containing the extracted data (from meson.c) for '
             'the first current.',
        dest='infile1',
        metavar='FILE',
        required=True,
        nargs='+'
    )
    required.add_argument(
        '-i2', '--in2',
        help='input file(s) containing the extracted data (from meson.c) for '
             'the second current.',
        dest='infile2',
        metavar='FILE',
        required=True,
        nargs='+'
    )

    required.add_argument(
        '-o', '--out',
        help='output file(s) containing the text and plot data.',
        dest='outfile',
        metavar='FILE',
        default=('-', '-'),
        nargs=2
    )

    # to make sure lmfit has this dependency
    if not module_exist('numdifftools'):
        raise ValueError("module numdifftools does not exist, please install.")

    args = parser.parse_args()
    result_dict, plot_data = main(args)
    meta = parser.set_result(result_dict, plot_data)
    _, _, fprint = ms.get_target(args.outfile, '.txt')
    fprint(f"This took {meta['duration']:.2f} seconds.")

    if not args.noplot:
        plot(args, plot_data)
