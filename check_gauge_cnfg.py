#!/usr/bin/env python3
#
# Check an exported gauge configuration for validity
#

import argparse
import numpy as np
import struct # struct.unpack(), struct.pack()
import os # os.path.getsize
import sys # sys.stderr
import json # json.dumps()

int_size, float_size, double_size = 4, 4, 8 # in bytes

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description='Check an exported gauge configuration for validity.\n1) whether the files contain 4 integers that denote the lattice size.\n2) if the file size corresponds to one of 3 possible values (SU(3), U(1) or SU(3)xU(1)).'
    )
    parser.add_argument('-v', '--verbose', help='verbose output (lists valid files as well)', default=False, action="store_true")
    parser.add_argument('-p', '--print', help='Print a list of corrupted/invalid configs', default=False, action="store_true")

    required = parser.add_argument_group('required named arguments')
    required.add_argument('-l', '--lattice',
        help='expected total lattice size given as 4 integers, eg: -l 64 32 32 32 for a 64x32x32x32 lattice (in case of c* boundaries, the files only contain the physical lattice, thus the expected lattice size is the physical one).',
        nargs=4,
        metavar=('N0', 'N1', 'N2', 'N3'),
        type=int,
        required=True,
    )
    required.add_argument('-i', '--in',
        help='gauge configuration file(s) to validate (exported via export_cnfg(), see modules/archive/archive.c)',
        dest='infile',
        metavar='FILE',
        required=True,
        nargs='+'
    )

    args = parser.parse_args()
    if args.verbose:
        print("Running with the following arguments:")
        print(json.dumps(vars(args), indent = 4))


    ifail = 0
    corrupt_configs = []
    for infile in args.infile:
        fail = False
        with open(infile, "rb") as f:
            if args.verbose: print("reading file {0}".format(infile))

            # bit order is always little endian
            # the first 4 integers are the lattice size
            N = np.array(struct.unpack('<4I', f.read(4*int_size))) # 4 unsigned int I, < little endian

            if np.array_equal(N, args.lattice):
                if args.verbose: print("* \033[92mLattice sizes match\033[0m")
            else:
                print("* \033[91mLattice sizes do not match. Found: {0}x{1}x{2}x{3} (expected: {4}x{5}x{6}x{7})\033[0m".format(*N, *args.lattice), file=sys.stderr)
                fail = True

            VOLUME = args.lattice[0]*args.lattice[1]*args.lattice[2]*args.lattice[3]
            size1 = (18*4*VOLUME + 1)*double_size + 4*int_size  # pure SU(3)
            size2 = (4*VOLUME + 1)*double_size + 4*int_size     # pure U(1)
            size3 = (19*4*VOLUME + 2)*double_size + 4*int_size  # SU(3)xU(1)

            file_size = os.path.getsize(infile)

            if file_size == size1:
                cnfg_type = 1
                if args.verbose: print("* \033[92mGauge group is SU(3)\033[0m")
            elif file_size == size2:
                cnfg_type = 2
                if args.verbose: print("* \033[92mGauge group is U(1)\033[0m")
            elif file_size == size3:
                cnfg_type = 3
                if args.verbose: print("* \033[92mGauge group is SU(3)xU(1)\033[0m")
            else:
                cnfg_type = -1
                print("* \033[91mUnknown gauge group (file size is wrong)\033[0m", file=sys.stderr)
                fail = True

        if fail:
            print("--> \033[91mCorrupt or invalid gauge config: {0}\033[0m".format(infile), file=sys.stderr)
            ifail += 1
            corrupt_configs.append(infile)
        else:
            if args.verbose: print("--> \033[92mGauge config seems valid: {0}\033[0m".format(infile))

    if args.print:
        print('\n'.join(corrupt_configs))

    print("\nSummary")
    print("-------")
    print("{0}/{1} invalid or corrupt configs".format(len(corrupt_configs), len(args.infile)))

    exit(0 if len(corrupt_configs) == 0 else 1)
