#!/usr/bin/env python3
#
# Compare correlators.
#

import argparse
import numpy as np # np.asarray, np.zeros(), np.float16, np.float32, np.float64, np.linalg.norm()
import scipy as sc
import scipy.optimize # sc.optimize.curve_fit()
import struct # struct.unpack(), struct.pack()
import json # json.dumps()
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from matplotlib.ticker import MaxNLocator
from matplotlib.legend_handler import HandlerLine2D
import matplotlib as mpl
import hashlib as hl
import warnings
import lmfit as lm
import re
import mass as ms
import sys
import shlex
import difflib
import os

int_size, float_size, double_size = 4, 4, 8 # in bytes

def braces_append(b, first, last, step):
    # if it's only two elements, don't expand, just list them
    if first+step == last:
        b.append(first)
        b.append(last)
    # only one elemnent
    elif first == last:
        b.append(first)
    # if there exists an n>1 such that first + n*step == last
    elif (last-first) % step == 0:
        b.append((first, last, step))
    # in all other cases, just list the elements
    else:
        b.append(first)
        b.append(last)
    return b

def brace_unexpansion(args):
    placeholder = "[[__PLACEHOLDER__]]"

    for first in args:
        try:
            skeleton = first
            for arg in args:
                if arg == first:
                    continue
                s = difflib.SequenceMatcher(a=first, b=arg)
                blocks = s.get_matching_blocks()
                skeleton = ""
                for (a,b,size) in blocks:
                    if size > 0:
                        skeleton += first[a:a+size]
                        if a+size!=len(first):
                            skeleton += placeholder

            regex = skeleton.replace(placeholder, r"([0-9]+)")
            elements = []
            for arg in args:
                if (m := re.search(regex, arg)):
                    for i, match in enumerate(m.groups()):
                        if len(elements) <= i:
                            elements.append([])
                        elements[i].append(match)
                else:
                    error = ValueError(f"No match for regex {regex} in {arg}")
                    raise error
        except ValueError:
            continue

        break

    else:
        raise error

    #elements[1] = np.array(["0","1","2","3","5","7","9"])
    #elements[1] = np.array(["0","1","2","8", "13"])
    path = skeleton
    for el in elements:
        def convert_to_int(x):
            if str(x).isnumeric():
                return int(x)
            else:
                raise ValueError(f"Not all differences are numeric ({x})")
        arr = np.array(list(map(convert_to_int, set(el))))
        arr.sort()
        steps = (arr - np.roll(arr, 1))[1:]
        first = arr[0]
        last = arr[0]
        braces = []
        for j,(step,last_step) in enumerate(zip(steps, np.roll(steps, 1))):
            if j == 0:
                last_step = step
            if step == last_step:
                last += step
            elif first != last:
                braces = braces_append(braces, first, last, last_step)
                first = last+step
                last = first
            else:
                last += step
        braces = braces_append(braces, first, last, last_step)

        def fmt(x):
            if type(x) == tuple:
                if x[2] == 1:
                    return f"{{{x[0]}..{x[1]}}}"
                else:
                    return f"{{{x[0]}..{x[1]}..{x[2]}}}"
            return str(x)

        j = list(map(fmt, braces))
        replacement = ",".join(j)
        if len(j) > 1:
            replacement = "{"+replacement+"}"
        path = path.replace(placeholder, replacement, 1)

    return path

def reduced_argv(arguments, argv):
    for argument in np.atleast_1d(arguments):
        if argument in argv:
            index = argv.index(argument)
            args = []
            for i in range(index+1, len(argv)):
                if argv[i].startswith('-') and argv[i] != '-':
                    break
                else:
                    args.append(argv[i])
                    argv[i] = 0

            argv[index+1] = brace_unexpansion(args)
            argv = list(filter(lambda a: a != 0, argv))
    return argv

def get_cmd(red_args, path_args, argv = sys.argv):
    # make paths absolute
    for arg in path_args:
        if arg in argv:
            index = argv.index(arg)
            for i in range(index+1, len(argv)):
                if argv[i].startswith('-') and argv[i] != '-':
                    break
                else:
                    argv[i] = os.path.abspath(argv[i])

    # reduce the arguments, only quote the non-reduced args
    rargv = reduced_argv(red_args, argv)
    for (i, arg) in enumerate(rargv):
        rargv[i] = arg if rargv[i-1] in red_args else shlex.quote(arg)

    # absolute path for the script itself
    rargv[0] = os.path.abspath(rargv[0])

    return " ".join(rargv)

if __name__ == '__main__':

    pre_parser = argparse.ArgumentParser(add_help=False)
    pre_parser.add_argument('-N', default = 2, type=int)
    pre_args = pre_parser.parse_known_args()[0]
    print(pre_args)
    print(pre_args.N)

    parser = argparse.ArgumentParser(
        description = 'Compare correlators.',
        formatter_class = argparse.RawTextHelpFormatter
    )

    parser.add_argument("-N", help="number of correlators to compare", type=int, default=pre_args.N)
    parser.add_argument("-v", "--verbosity", action="count", help="increase output verbosity", default=0)
    parser.add_argument('-n', '--names', nargs=pre_args.N, help='names of the ensembles/plots (can contain latex)', default=["Unknown ensemble #{0}".format(x) for x in range(pre_args.N)])
    parser.add_argument("-t", "--title", type=str, help="plot title", default="")

    parser.add_argument('--plot-only', nargs='+', help='plot only these correlators', metavar = 'n', dest = 'plot_only', type=int, default=np.arange(0,pre_args.N).tolist())
    parser.add_argument('--no-plot', help="don't plot", default=False, action="store_true", dest = 'no_plot')
    parser.add_argument('--plot-all', help="equivalent to --plot-rel-err --plot-ratio --plot-fold-ratio --plot-meff --plot-meff-rel-err --plot-A", default=False, action="store_true")
    parser.add_argument('--plot-folds', help="plot the folds as well", default=False, action="store_true")
    parser.add_argument('--plot-rel-err', help="plot the relative errors", default=False, action="store_true")
    parser.add_argument('--plot-abs-var', help='plot the variances', default=False, action="store_true")
    parser.add_argument('--plot-abs-err', nargs=pre_args.N, help='plot the absolute errors w.r.t the correlator with index i', type=int, default=[None]*pre_args.N)
    parser.add_argument('--plot-rel-var', nargs=pre_args.N, help='plot the variances w.r.t other variances', type=str, default=[None]*pre_args.N)
    parser.add_argument('--plot-contr', nargs=pre_args.N, help='plot relative contributions w.r.t other correlators', type=str, default=[None]*pre_args.N)
    parser.add_argument('--plot-ratio', help="plot Im/Re ratio as well", default=False, action="store_true")
    parser.add_argument('--plot-fold-ratio', help="plot ratio of the folds as well", default=False, action="store_true")
    parser.add_argument('--plot-meff', nargs='+', type=int, help="plot the log ratio (meff) as well", default=[])
    parser.add_argument('--plot-meff-rel-err', nargs='+', type=int, help="plot the rel. error of meff as well", default=[])
    parser.add_argument('--plot-A', nargs=pre_args.N, help='Plot the amplitude A, given meff for each correlator', type=float, default=None, metavar=('MEFF'))
    parser.add_argument('--plot-all-configs', help="plot all configs separately", default=False, action="store_true")
    parser.add_argument('--nsrc', nargs=pre_args.N, help='number of sources used', type=int, default=[1]*pre_args.N)
    parser.add_argument('--read-src', nargs=pre_args.N, help='number of sources to read per file used', type=int, default=[1]*pre_args.N)
    parser.add_argument('--fold',
        help = '\n'.join((
            'whether to fold around the center to reduce some systematic errors or not. Possible values are:',
            ' * 0: no folding (default)',
            ' * 1: the obvious one, fold at 31.5 if N0=64, basically combine [0] and [63], [1] and [62], ..., [31] and [32]',
            ' * 2: remove first pt, middle counts twice, fold at 32 if N0=64, basically combine [1] and [63], [2] and [62], ..., [32] and [32]',
            ' * 3: remove last and middle pt, Anians method, basically combine [0] and [62], [1] and [61], ..., [30] and [32]',
            ' * 4: remove middle, first counts twice, basically combine [0] and [0], [1] and [63], ..., [31] and [31]'
        )),
        type = int,
        default = [0]*pre_args.N,
        nargs = pre_args.N,
        choices = [0,1,2,3,4]
    )
    parser.add_argument('--limit',
        help = 'limit the number of configs to use, set to -1 to use all, use random(n) to select randomly n configs, or use array slicing à la range(start:end:step)',
        type = str,
        default = [None]*pre_args.N,
        nargs = pre_args.N
    )
    parser.add_argument('-l', '--linear', help='linear plot instead of log', default=False, action="store_true")
    parser.add_argument('-r', '--regime', help='regime to compare correlators in default [0,1000]', type=int, nargs=2, default=[0,1000])
    parser.add_argument('-e', '--expression', help='expression to perform on correlator i, eg: "{} *= -1" multiplies the correlator with -1., brackets {} are replaced by the array containing the correlator, G[i].', type=str, nargs=pre_args.N, default=[None]*pre_args.N)
    parser.add_argument('-c', '--construct', help='construct a new correlator by combining. Eg. "G[0]+G[1]" "name" adds the two correlators given by -i1 and -i2. It expect parameters in the format <expression> <string>', type=str, action='append', nargs=2, metavar=('corr_exp','name'), default=[])

    parser.add_argument('-a', '--additional-plot', help='add another plot by combining. Eg. "G[0]/G[1]" "name" divides the two correlators given by -i1 and -i2. It expects parameters in the format <expression> <string>', type=str, action='append', nargs=3, metavar=('plot_exp','name', 'color'), default=[])

    required = parser.add_argument_group('required named arguments')
    for i in range(pre_args.N):
        required.add_argument('-i{0}'.format(i), '--infile{0}'.format(i),
            help = 'input file(s) for ensemble {0} containing the extracted data (from meson.c). If multiple files are provided, statistical errors will be in the plot.'.format(i),
            dest = 'infile{0}'.format(i),
            metavar = 'FILE',
            required = True,
            nargs = '+'
        )
    
    parser.add_argument('-o', '--outfile', help='Output filename', default='-')

    args = parser.parse_args()
    if args.plot_all:
        args.plot_folds = True
        args.plot_rel_err = True
        args.plot_ratio = True
        args.plot_fold_ratio = True
        args.plot_meff = np.arange(0,pre_args.N).tolist()
        args.plot_meff_rel_err = np.arange(0,pre_args.N).tolist()

    if args.verbosity:
        print("Running with the following arguments:")
        print(json.dumps(vars(args), indent = 4))

    G = []
    G_err = []
    G_var = []
    G_rel_var = []
    G_contr = []
    G_mean = []
    GL = []
    GR = []
    G_err_rel = []
    G_err_abs = []
    ratio = []
    fratio = []
    meff_mean = []
    meff_err = []
    meff_err_rel = []
    A = []
    nconfigs = []
    with open(args.infile0[0], "rb") as f:
        N0 = struct.unpack('<1I', f.read(int_size))[0] # 1 unsigned int I, < little endian
        N1 = struct.unpack('<1I', f.read(int_size))[0]
        N2 = struct.unpack('<1I', f.read(int_size))[0]
        N3 = struct.unpack('<1I', f.read(int_size))[0]
        if args.verbosity: print("lattice size was {0}x{1}x{2}x{3}".format(N0, N1, N2, N3))
        for n in range(pre_args.N):
            infile_len = args.read_src[n]*len(getattr(args, "infile{0}".format(n)))

            G.append(np.zeros((infile_len, N0), dtype=np.complex64))
            G_err.append(np.zeros((infile_len, N0), dtype=np.complex64))
            G_var.append(np.zeros((infile_len, N0), dtype=np.complex64))
            G_rel_var.append(np.zeros((infile_len, N0), dtype=np.complex64))
            G_contr.append(np.zeros((infile_len, N0), dtype=np.complex64))
            G_err_rel.append(np.zeros((infile_len, N0), dtype=np.complex64))
            G_err_abs.append(np.zeros((infile_len, N0), dtype=np.complex64))
            ratio.append(np.zeros((infile_len, N0), dtype=np.complex64))
            fratio.append(np.zeros((infile_len, N0), dtype=np.complex64))
            A.append(np.zeros((infile_len, N0), dtype=np.complex64))
            meff_mean.append(np.zeros(N0 if args.fold[n] == 0 else int(N0/2), dtype=np.complex64))
            meff_err.append(np.zeros(N0 if args.fold[n] == 0 else int(N0/2), dtype=np.complex64))
            meff_err_rel.append(np.zeros((infile_len, N0), dtype=np.complex64))
            G_mean.append(np.zeros(N0 if args.fold[n] == 0 else int(N0/2), dtype=np.complex64))
            GL.append(np.zeros(N0 if args.fold[n] == 0 else int(N0/2), dtype=np.complex64))
            GR.append(np.zeros(N0 if args.fold[n] == 0 else int(N0/2), dtype=np.complex64))
        #G_mean = np.zeros((pre_args.N, N0), dtype=np.complex64)

    for n in range(pre_args.N):
        i = 0
        for infile in getattr(args, "infile{0}".format(n)):
            with open(infile, "rb") as f:
                if args.verbosity: print(f"reading file {infile} ({args.read_src[n]} entries)")

                N0 = struct.unpack('<1I', f.read(int_size))[0] # 1 unsigned int I, < little endian
                N1 = struct.unpack('<1I', f.read(int_size))[0]
                N2 = struct.unpack('<1I', f.read(int_size))[0]
                N3 = struct.unpack('<1I', f.read(int_size))[0]

                datam = struct.unpack('<{0}d'.format(2*N0*args.read_src[n]), f.read(double_size*2*N0*args.read_src[n])) # 2*N0 doubles

                for j in range(args.read_src[n]):
                    G[n][i*args.read_src[n] + j].real = np.array(datam[j*2*N0:2*N0*(j+1):2])  # only even indices, real part
                    G[n][i*args.read_src[n] + j].imag = np.array(datam[j*2*N0+1:2*N0*(j+1)+1:2])  # only odd indices, imag part

                #G[n][i].imag = np.array(datam[1::2]) # only odd  indices, imag part
                i += 1

    for n in range(pre_args.N):
        if args.expression[n] is not None and args.expression[n] != "":
            #exp = args.expression[n].format("G[{}]".format(n))
            exp = args.expression[n].replace("{}", "G[{}]".format(n))
            if args.verbosity: print(f"evaluating {exp}")
            exec(exp)

    if args.verbosity:
        for n in range(pre_args.N):
            print("G[{0}].shape = {1} (initial)".format(n, G[n].shape))

    # average all nsrc sources of a given config
    for n in range(pre_args.N):
        if args.nsrc[n] != 1:
            G[n], G_err[n], _ = ms.avg_sources(G[n], args.nsrc[n], flatten = False)
        if args.verbosity: print("G[{0}].shape = {1} (after averaging over sources)".format(n, G[n].shape))

    # fold according to folding variant
    for n in range(pre_args.N):
        G[n], G_err[n], _, GL[n], GR[n], _, _, _ = ms.fold(G[n], args.fold[n], N0, Gerr = G_err[n])
        if args.verbosity: print("G[{0}].shape = {1} (after folding)".format(n, G[n].shape))

    for n in range(pre_args.N):
        if args.limit[n] is not None:
            random = lambda N: np.random.choice(G[n].shape[0], N, replace=False)
            # print(f"evaluating \"G[n] = G[n][{args.limit[n]}]\"")
            G[n] = G[n][eval(args.limit[n])]
            G_err[n] = G_err[n][eval(args.limit[n])]
        if args.verbosity: print("G[{0}].shape = {1} (after limiting)".format(n, G[n].shape))

    for n in range(pre_args.N):
        nconfigs.append(G[n].shape[0])

    for i, (corr_exp, name) in enumerate(args.construct):
        if args.verbosity: print(f"evaluating G[{pre_args.N}] = {corr_exp} (name {name})")
        corrs = np.array([int(e) for e in re.compile('G\[([0-9]+)\]').findall(corr_exp)])
        G.append(eval(corr_exp))
        G_err.append([])
        G_var.append([])
        GL.append(np.array([]))
        GR.append(np.array([]))
        G_mean.append([])
        G_err_rel.append([])
        G_err_abs.append([])
        G_rel_var.append([])
        G_contr.append([])
        ratio.append([])
        fratio.append([])
        meff_mean.append([])
        meff_err.append([])
        meff_err_rel.append([])
        pre_args.N += 1
        args.names.append(name)
        if not np.all(np.array(args.plot_abs_err) == None):
            args.plot_abs_err.append(args.plot_abs_err[corrs[0]])

        nc = np.array([G[e].shape[0] for e in corrs])
        if np.all(nc == nc[0]):
            nconfigs.append(nc[0])
        else:
            nconfigs.append(str(set(nc)))

        # determine the number of sources for a constructed correlator
        nsources = np.array([args.nsrc[e] for e in corrs])
        if np.all(nsources == nsources[0]):
            args.nsrc.append(nsources[0])
        else:
            args.nsrc.append(str(set(nsources)))

    for n in range(pre_args.N):
        G_mean[n], G_err[n], G_var[n] = ms.avg_sources(G[n], G[n].shape[0], Gerr = None) # average all remaining configs
        if args.verbosity: print("G_mean[{0}].shape = {1} (after final averaging)".format(n, G_mean[n].shape))
        if args.plot_folds or args.plot_fold_ratio:
            try:
                GL[n], _, _ = ms.avg_sources(GL[n], GL[n].shape[0])
                GR[n], _, _ = ms.avg_sources(GR[n], GR[n].shape[0])
            except:
                pass

    G_plot = {}
    for i, (plot_exp, name, fmt) in enumerate(args.additional_plot):
        if args.verbosity: print(f"evaluating G_plot[{name}] = {plot_exp} (name {name})")
        G_plot[name] = eval(plot_exp)

    if args.plot_all_configs:
        fig = plt.figure()
        c=0
        for n in range(pre_args.N):
            for m in range(G[n].shape[0]):
                plt.plot(np.arange(0, len(G[n][m]), 1), np.real(G[n][m]))
                c+=1
        if not args.linear: plt.yscale("log")
        if args.title: plt.suptitle(args.title)
        plt.title(f"All {c} configs/sources, limited by {args.limit}")
        if args.outfile == '-':
            plt.draw()
            plt.show()
        else:
            reduce_args = [f'{arg}{n}' for n in range(pre_args.N) for arg in ['-i', '--infile']]
            plt.savefig(args.outfile, dpi=fig.dpi, metadata={
                'Keywords': shlex.join(reduced_argv(reduce_args, sys.argv))
            })
        exit()

    if args.plot_rel_err:
        for n in range(pre_args.N):
            G_err_rel[n] = np.abs(G_err[n]/G_mean[n])

    if not np.all(np.array(args.plot_abs_err) == None):
        for n in range(pre_args.N):
            if args.verbosity: print(f"G_err_abs[{n}] = np.abs(G_err[{n}]/G_mean[{args.plot_abs_err[n]}])")
            G_err_abs[n] = np.abs(G_err[n]/G_mean[args.plot_abs_err[n]])

    if not np.all(np.array(args.plot_rel_var) == None):
        for n in range(pre_args.N):
            try:
                exp = re.sub("(\d+)", "G_var[\\1]", args.plot_rel_var[n])
                if args.verbosity: print(f"G_rel_var[{n}] = G_var[{n}]/({exp})")
                G_rel_var[n] = G_var[n]/(eval(exp))
            except:
                print(f"failed: G_rel_var[{n}] = G_var[{n}]/({exp})")

    if not np.all(np.array(args.plot_contr) == None):
        for n in range(pre_args.N):
            try:
                exp = re.sub("(\d+)", "np.abs(G_mean[\\1])", args.plot_contr[n])
                if args.verbosity: print(f"G_contr[{n}] = G_mean[{n}]/({exp})")
                G_contr[n] = np.abs(G_mean[n])/(eval(exp))
            except:
                print(f"failed: G_contr[{n}] = np.abs(G_mean[{n}])/({exp})")

    if args.plot_meff or args.plot_meff_rel_err:
        for n in range(pre_args.N):
            M = np.log((G[n]/np.roll(G[n], -1)).real)
            meff_mean[n] = np.nanmean(M, axis=0) if M.shape[0] > 1 else M[0]
            meff_err[n]  = np.nanstd(M, axis=0) if M.shape[0] > 1 else np.zeros_like(meff_mean[n])
            meff_err_rel[n] = np.abs(meff_err[n]/meff_mean[n])

    if args.plot_ratio:
        for n in range(pre_args.N):
            ratio[n] = np.abs(G_mean[n].imag/G_mean[n].real)

    if args.plot_fold_ratio:
        for n in range(pre_args.N):
            fratio[n] = np.abs(GR[n]/GL[n])

    if args.plot_A is not None:
        for n in range(pre_args.N):
            A[n] = G_mean[n]/np.exp(list(map(lambda x: -args.plot_A[n]*x, range(0, G_mean[n].shape[0]))))

    a, b = args.regime[0], args.regime[1]
    if args.verbosity and pre_args.N > 1:
        rcorrs = 0, 1
        print(f"ratio in regime [{a},{b}], G[{rcorrs[0]}]/G[{rcorrs[1]}]: {np.abs(G_mean[rcorrs[0]][a:b]/G_mean[rcorrs[1]][a:b])}")
        for n in range(pre_args.N):
            print(f"G[{n}] sum in regime [{a},{b}]: {np.sum(G_mean[n][a:b])}")

    if args.verbosity:
        for n in range(pre_args.N):
            for m in range(pre_args.N):
                avg_ratio = np.abs(G_mean[n][a:b]/G_mean[m][a:b])
                print(f"avg ratio in regime [{a},{b}], G[{n}]/G[{m}]: {np.mean(avg_ratio)} ± {np.nanstd(avg_ratio)}")

    if args.verbosity:
        for n in range(pre_args.N):
            print(f"max relative error in regime [{a},{b}] of correlator G[{n}]: {np.abs(G_err[n][a:b]/G_mean[n][a:b]).max():.6e}")

    if args.verbosity:
        for n in range(pre_args.N):
            for m in range(n):
                print(f"difference in regime [{a},{b}] of correlators max(abs(G[{n}]-G[{m}])): {np.abs(G_mean[n][a:b]-G_mean[m][a:b]).max():.6e}")

    ####################################################################################################
    ####################################### Plotting starts here #######################################
    ####################################################################################################

    plots = 0
    if not args.no_plot:
        plots += 1
        corr = 0
    if args.plot_rel_err:
        rel = plots
        plots += 1
    if not np.all(np.array(args.plot_abs_err) == None):
        absolute = plots
        plots += 1
    if not np.all(np.array(args.plot_rel_var) == None):
        relvar = plots
        plots += 1
    if not np.all(np.array(args.plot_contr) == None):
        contr = plots
        plots += 1
    if args.plot_abs_var:
        absvar = plots
        plots += 1
    if args.plot_ratio:
        rat = plots
        plots += 1
    if args.plot_meff:
        lrat = plots
        plots += 1
    if args.plot_meff_rel_err:
        mrel = plots
        plots += 1
    if args.plot_fold_ratio:
        frat = plots
        plots += 1
    if args.plot_A is not None:
        pltA = plots
        plots += 1
    if G_plot:
        addp = plots
        plots += 1

    if plots in [4]:
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
        axes = [ax1, ax2, ax3, ax4]
    elif plots in [5, 6]:
        fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(2, 3)
        axes = [ax1, ax2, ax3, ax4, ax5, ax6]
    elif plots in [3]:
        fig = plt.figure()
        ax1 = fig.add_subplot(2, 2, (1,3))
        ax2 = fig.add_subplot(2, 2, 2)
        ax3 = fig.add_subplot(2, 2, 4)
        axes = [ax1, ax2, ax3]
    elif plots == 1:
        fig, axes = plt.subplots(1, 1)
        axes = [axes]
    elif plots > 0:
        fig, axes = plt.subplots(1, plots)

    if plots > 0:
        plt_kwargs_titles = {'fontsize': 20}
        plt_kwargs_labels = {'fontsize': 20}
        plt_kwargs_legend = {'fontsize': 12}
        plt_kwargs_ticks = {'fontsize': 16}

        fig.set_size_inches(18.5, 10.5, forward=True)
        fig.suptitle(fr"Compare {len(args.plot_only)} correlators" if args.title is None else args.title, **plt_kwargs_titles)
        print(f"{pre_args.N = }, {args.plot_only = }")

        for n in range(pre_args.N):
            if not n in args.plot_only:
                continue

            print(f"plotting {args.names[n]}, {n = }")
            X = np.arange(0, len(G_mean[n]), 1)
            ls = ['-','--','-.',':'][n%4]

            if args.plot_rel_err:
                print(f" * plotting rel error {args.names[n]}, {n = }")
                axes[rel].plot(X, G_err_rel[n].real.flatten(),
                    linestyle = ls,
                    linewidth = 2,
                    label = f'G[{n}] rel. error of correlator for {args.names[n]}, {nconfigs[n]} config(s), {args.nsrc[n]} source(s)',
                )

            if 0 <= n < len(args.plot_abs_err) and args.plot_abs_err[n] != None:
                print(f" * plotting abs error {args.names[n]}, {n = }")
                axes[absolute].plot(X, G_err_abs[n].real.flatten(),
                    linestyle = ls,
                    linewidth = 2,
                    label = f'G_err[{n}]/G[{args.plot_abs_err[n]}]',
                )

            if 0 <= n < len(args.plot_rel_var) and args.plot_rel_var[n] != None:
                print(f" * plotting rel variances {args.names[n]}, {n = }")
                axes[relvar].plot(X, G_rel_var[n].real.flatten(),
                    linestyle = ls,
                    linewidth = 2,
                    label = f'G_var[{n}]/??',
                )

            if 0 <= n < len(args.plot_contr) and args.plot_contr[n] != None:
                print(f" * plotting contributions {args.names[n]}, {n = }")
                axes[contr].plot(X, G_contr[n].real.flatten(),
                    linestyle = ls,
                    linewidth = 2,
                    label = f'G_var[{n}]/??',
                )

            if args.plot_abs_var:
                print(f" * plotting abs variances {args.names[n]}, {n = }")
                axes[absvar].plot(X, G_var[n].real.flatten(),
                    linestyle = ls,
                    linewidth = 2,
                    label = f'G_var[{n}]',
                )

            if args.plot_ratio:
                axes[rat].plot(X, ratio[n].flatten(),
                    linestyle = ls,
                    linewidth = 2,
                    label = f'G[{n}] Im/Re ratio of correlator for {args.names[n]}, {nconfigs[n]} config(s), {args.nsrc[n]} source(s)',
                )

            if args.plot_meff:
                axes[lrat].errorbar(X[0:-1], meff_mean[n].real[0:-1],
                    #xerr = 0, # if Y has nans, it fails when this is uncommented !?!
                    yerr = meff_err[n].flatten()[0:-1],
                    linestyle = ls,
                    linewidth = 2,
                    label = f'G[{n}] meff, log(G(t)/G(t+1) of correlator for {args.names[n]}, {nconfigs[n]} config(s), {args.nsrc[n]} source(s)',
                    capsize = 3,
                    alpha = 0.5 if n in args.plot_meff else 0.0
                )

            if args.plot_meff_rel_err:
                axes[mrel].plot(X[0:-1], meff_err_rel[n].real.flatten()[0:-1],
                    linestyle = ls,
                    linewidth = 2,
                    label = f'G[{n}] meff, rel. error of correlator for {args.names[n]}, {nconfigs[n]} config(s), {args.nsrc[n]} source(s)',
                    alpha = 0.5 if n in args.plot_meff_rel_err else 0.0
                )

            if args.plot_A is not None and args.plot_A[n] != 0.0:
                axes[pltA].plot(X, A[n].real.flatten(),
                    linestyle = ls,
                    linewidth = 2,
                    label = f'G[{n}] A of correlator for {args.names[n]}, {nconfigs[n]} config(s), {args.nsrc[n]} source(s), $m_{{eff}}$={args.plot_A[n]}',
                )

            if args.plot_fold_ratio:
                axes[frat].plot(X, fratio[n].flatten(),
                    linestyle = ls,
                    linewidth = 2,
                    label = f'G[{n}] GR/GL fold-ratio of correlator for {args.names[n]}, {nconfigs[n]} config(s), {args.nsrc[n]} source(s)',
                )

            if not args.no_plot:
                axes[corr].errorbar(X, G_mean[n].real,
                    xerr = 0,
                    yerr = G_err[n].flatten(),
                    linestyle = ls,
                    linewidth = 2,
                    label = f'G[{n}], {args.names[n]}, {nconfigs[n]} conf, {args.nsrc[n]} src',
                    capsize = 3,
                    alpha = 0.5
                )

            if args.plot_folds:
                try:
                    axes[0].plot(X, GL[n].real,
                        label = f'GL[{n}] mean of correlator for {args.names[n]}, {nconfigs[n]} config(s), {args.nsrc[n]} source(s)'
                    )
                    print(f" * plotting fold left {args.names[n]}, {n = }")
                    print(f" * plotting fold right {args.names[n]}, {n = }")
                    axes[0].plot(X, GR[n].real,
                        label = f'GR[{n}] mean of correlator for {args.names[n]}, {nconfigs[n]} config(s), {args.nsrc[n]} source(s)'
                    )
                except:
                    print(ms.red(f" * ERROR: cannot plot folds of {args.names[n]}, {n = }"))
                    pass

        for i, (plot_exp, name, fmt) in enumerate(args.additional_plot):
            ls = ['-','--','-.',':'][(n+i)%4]
            axes[addp].plot(X, np.real(G_plot[name]), fmt, label = f'{name}')

        if args.additional_plot:
            axes[addp].set_title(r'Additional plot', **plt_kwargs_titles)
            axes[addp].legend()
            ms.tablulated_legend(axes[addp], **plt_kwargs_legend)
            #axes[addp].set_yscale("log")

        if not args.no_plot:
            axes[corr].set_title(r'Correlator(s)', **plt_kwargs_titles)
            axes[corr].set_xlabel(r'$x_0$', **plt_kwargs_labels)
            axes[corr].set_ylabel(r'$G(x_0)$', **plt_kwargs_labels)
            if not args.linear: axes[corr].set_yscale("log")

        if args.plot_rel_err:
            axes[rel].axhline(y = 1.0, color = 'black', linestyle = '-')
            axes[rel].set_xlabel(r'$x_0$', **plt_kwargs_labels)
            axes[rel].set_ylabel(r'$\Delta G_i / G_i$', **plt_kwargs_labels)
            axes[rel].set_title(r'Relative error(s) on G', **plt_kwargs_titles)
            axes[rel].set_yscale("log")

        if not np.all(np.array(args.plot_abs_err) == None):
            axes[absolute].axhline(y = 1.0, color = 'black', linestyle = '-')
            axes[absolute].set_xlabel(r'$x_0$', **plt_kwargs_labels)
            axes[absolute].set_ylabel(r'$\Delta G_i / G_j$', **plt_kwargs_labels)
            axes[absolute].set_title(r'Absolute error(s) on G', **plt_kwargs_titles)
            axes[absolute].set_yscale("log")
            ms.tablulated_legend(axes[absolute], **plt_kwargs_legend)

        if not np.all(np.array(args.plot_rel_var) == None):
            axes[relvar].axhline(y = 1.0, color = 'black', linestyle = '-')
            axes[relvar].set_xlabel(r'$x_0$', **plt_kwargs_labels)
            axes[relvar].set_ylabel(r'todo', **plt_kwargs_labels)
            axes[relvar].set_title(r'Relative variance(s) on G', **plt_kwargs_titles)
            #axes[relvar].set_yscale("log")
            ms.tablulated_legend(axes[relvar], **plt_kwargs_legend)

        if not np.all(np.array(args.plot_contr) == None):
            axes[contr].axhline(y = 1.0, color = 'black', linestyle = '-')
            axes[contr].set_xlabel(r'$x_0$', **plt_kwargs_labels)
            axes[contr].set_ylabel(r'todo', **plt_kwargs_labels)
            axes[contr].set_title(r'Contribution(s) on G', **plt_kwargs_titles)
            #axes[contr].set_yscale("log")
            ms.tablulated_legend(axes[contr], **plt_kwargs_legend)

        if args.plot_abs_var:
            axes[absvar].axhline(y = 1.0, color = 'black', linestyle = '-')
            axes[absvar].set_xlabel(r'$x_0$', **plt_kwargs_labels)
            axes[absvar].set_ylabel(r'todo', **plt_kwargs_labels)
            axes[absvar].set_title(r'Absolute variance(s) on G', **plt_kwargs_titles)
            axes[absvar].set_yscale("log")
            ms.tablulated_legend(axes[absvar], **plt_kwargs_legend)

        if args.plot_ratio:
            #axes[rat].axhline(y = np.finfo(float).eps, color = 'black', linestyle = '-')
            #axes[rat].axhline(y = -np.finfo(float).eps, color = 'black', linestyle = '-')
            axes[rat].set_xlabel(r'$x_0$', **plt_kwargs_labels)
            axes[rat].set_ylabel(r'$\Im(G) / \Re(G)$', **plt_kwargs_labels)
            axes[rat].set_title(r'Ratios: imag/real', **plt_kwargs_titles)
            axes[rat].set_yscale("symlog")

        if args.plot_fold_ratio:
            axes[frat].axhline(y = 1, color = 'black', linestyle = '-')
            axes[frat].set_xlabel(r'$x_0$', **plt_kwargs_labels)
            axes[frat].set_ylabel(r'$G_R / G_L$', **plt_kwargs_labels)
            axes[frat].set_title(r'Ratios: right/left folds', **plt_kwargs_titles)
            axes[frat].set_yscale("symlog")

        if args.plot_meff:
            axes[lrat].set_xlabel(r'$x_0$', **plt_kwargs_labels)
            axes[lrat].set_ylabel(r'$m_{eff} = log(G(t)/G(t+1))$', **plt_kwargs_labels)
            axes[lrat].set_title(r'meff, log-ratio', **plt_kwargs_titles)
            axes[lrat].set_yscale("linear")

        if args.plot_meff_rel_err:
            axes[mrel].axhline(y = 1.0, color = 'black', linestyle = '-')
            axes[mrel].set_xlabel(r'$x_0$', **plt_kwargs_labels)
            axes[mrel].set_ylabel(r'$\Delta m_{eff} / m_{eff}$', **plt_kwargs_labels)
            axes[mrel].set_title(r'Relative error(s) on $m_{eff}$', **plt_kwargs_titles)
            axes[mrel].set_yscale("log")

        if args.plot_A:
            axes[pltA].set_xlabel(r'$x_0$', **plt_kwargs_labels)
            axes[pltA].set_ylabel(r'amplitude $A$', **plt_kwargs_labels)
            axes[pltA].set_title(r'Amplitude $A$: $G(x_0)/\exp(-m_{eff} x_0)$', **plt_kwargs_titles)
            axes[pltA].set_yscale("linear")

        for ax in axes:
            plt.setp(ax.get_xticklabels(), **plt_kwargs_ticks)
            plt.setp(ax.get_yticklabels(), **plt_kwargs_ticks)

        ms.tablulated_legend(axes[0], **plt_kwargs_legend)
        fig.tight_layout()

        if args.outfile == '-':
            plt.draw()
            plt.show()
        else:
            if args.verbosity: print(f"Storing {args.outfile}")
            reduce_args = [f'{arg}{n}' for n in range(pre_args.N) for arg in ['-i', '--infile']]
            paths = ["-o", "--outfile"] + reduce_args

            try:
                keywords = get_cmd(reduce_args, paths)
            except ValueError:
                keywords = shlex.join(sys.argv)

            plt.savefig(args.outfile, dpi=fig.dpi, metadata={
                #'Keywords': ' '.join(sys.argv)
                'Keywords': keywords,
                'Creator': os.getcwd(),
            })
