#!/usr/bin/env python3
#
# Create a benchmark plot out of slurm.out files
#
# For the CSCS proposal generate the plots as follows:
#
# ./benchmark_plot.py -v --labels multicore --title "A400a00b324" -i /scratch/snx3000/groman/cscs_proposal/QCD-32-1/slurm-QCD-32-1_*_p32-*.out
# ./benchmark_plot.py -v --labels multicore --title "B400a00b324" -i /scratch/snx3000/groman/cscs_proposal/QCD-48-1/slurm-QCD-48-1_*_p32_mc*.out -g mc
# ./benchmark_plot.py -v --labels hybrid --title "B400a00b324" -i /scratch/snx3000/groman/cscs_proposal/QCD-48-1/slurm-QCD-48-1_*_p8_hybrid*.out -g hybrid
# ./benchmark_plot.py -v --labels hybrid --title "A400a00b324" -i /scratch/snx3000/groman/cscs_proposal/QCD-32-1/slurm-QCD-32-1_*_p8_hybrid*.out -g hybrid
#

import argparse
import re
import time
import matplotlib.pyplot as plt
import matplotlib.ticker
import numpy as np
import json
import tabulate
import glob
import os

# takes %H:%M:%S and returns seconds
def to_seconds(string):
    sec = 0
    for fraction in string.split(':'):
        sec = sec*60 + int(fraction, 10)
    return sec

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create a benchmark plot out of slurm.out files.')

    parser.add_argument('-v', '--verbose', help='verbose output', default=False, action="store_true")
    parser.add_argument('-g', '--groups', help='keywords to search in the job name on how to group slurm files', nargs='+', default=[])
    parser.add_argument('--labels', help="labels for the groups given by --groups (must be #groups +1 elements; the last item is for the group of remaining items, that don't fit in any group)", nargs='+', default=None)
    parser.add_argument('--title', help="plot title", default='Unknown')
    parser.add_argument('--table-fmt', help="format for the table, for possible options see https://bitbucket.org/astanin/python-tabulate/src/master/ under 'Table format', use 'hidden' to omit the table", default='grid')
    parser.add_argument('-s', help="calculate speed-up wrt to which time", default='wall_time', choices=['inv_time', 'wall_time'])
    parser.add_argument('--fname', help="output filename", default=None)

    required = parser.add_argument_group('required named arguments')
    required.add_argument('-i', '--in',
        help='list of slurm output files to parse',
        dest='infile',
        metavar='FILE',
        required=True,
        nargs='+'
    )

    args = parser.parse_args()
    if args.labels == None and len(args.groups) > 0:
        raise argparse.ArgumentTypeError('You have to give labels for the groups with --labels')

    if args.verbose:
        print("Running with the following arguments:")
        print(json.dumps(vars(args), indent = 4))

    # initialize the dictionaries
    wall_time, inv_time, tot_proc, nodes, speedup, ppn, jnames, jids, peff = {}, {}, {}, {}, {}, {}, {}, {}, {}
    for group in args.groups + [None]:
        for d in [wall_time, inv_time, tot_proc, nodes, speedup, ppn, jnames, jids, peff]:
            d[group] = []

    for infile in args.infile:
        with open(infile) as f:
            lines = f.readlines()
            jinfo = -100
            for i, line in enumerate(lines):
                if line.startswith('Batch Job Summary Report'):

                    # extract name and 37991070 from:
                    # Batch Job Summary Report (version 21.01.1) for Job "name" (37991070) on daint
                    m = re.search(r'Job \"([^\"]+)\" \(([0-9]+)\)', line)
                    if m:
                        jname = m.group(1)
                        jid = int(m.group(2))

                        # extract 16 and 32 from:
                        # name_n16_p32
                        m = re.search(r'_n([0-9]+)_p([0-9]+)', line)
                        if m:
                            jnodes = int(m.group(1))
                            jproc = int(m.group(2))
                        else:
                            print(f"Not able to extract #nodes and #proc/node from job name {jname}")
                            exit(1)

                        jgroup = None
                        for group in args.groups:
                            if group in jname:
                                jgroup = group

                    else:
                        print(f"Not able to extract job name and id from file {infile}")
                        exit(1)

                if line.startswith('Job information (1/2)'): jinfo = i

                if i == jinfo + 4:
                    # extract 00:54:39 from:
                    # 2022-04-26T17:54:30 2022-04-26T17:54:30 2022-04-26T18:13:22 2022-04-26T19:08:01   00:54:39 1-00:00:00
                    m = re.search(r'\s([0-9,\:]+)\s', line)
                    if m:
                        jelapsed = to_seconds(m.groups()[0])
                        wall_time[jgroup].append(jelapsed)
                        ppn[jgroup].append(jproc)
                        tot_proc[jgroup].append(jnodes*jproc)
                        nodes[jgroup].append(jnodes)
                        jnames[jgroup].append(jname)
                        jids[jgroup].append(jid)
                        #if args.verbose: print(f"extracted from file {infile}:\tgroup = {jgroup}\telapsed = {jelapsed}\tp = {jproc}\tn = {jnodes}")
                        logfile_pattern = f"{os.path.dirname(infile)}/{jgroup or ''}/n{jnodes}_p{jproc}/*.log"
                        if len(glob.glob(logfile_pattern)) > 0:
                            logfile = glob.glob(logfile_pattern)[0]
                        else:
                            print(f"No log file found for job name {jname}, glob {logfile_pattern}")
                            exit(1)
                        #if args.verbose: print(f"opening log file {logfile}")
                        with open(logfile) as flog:
                            log_lines = flog.readlines()
                            for log_line in log_lines:
                                # extract 950 and 8.01e+02 from:
                                # Configuration no 950 fully processed in 8.01e+02 sec
                                m = re.search(r'Configuration no ([0-9]+) fully processed in ([0-9,e,\+,\-\.]+) sec', log_line)
                                if m:
                                    jinversion = float(m.group(2))
                                    inv_time[jgroup].append(jinversion)
                                    #if args.verbose: print(f"extracted from file {logfile}:\tinversion_time = {jinversion}")
                                    if args.verbose:
                                        print(f"extracted information from the 2 files:\n\t{infile}\n\t{logfile}:\n\t\tgroup = {jgroup}\n\t\telapsed = {jelapsed}\n\t\tproc = {jproc}\n\t\tnodes = {jnodes}\n\t\tinv_time = {jinversion}\n")
                                    break
                            else:
                                print(f"Not able to extract inversion time time from file {logfile}")
                                exit(1)
                        break

                    else:
                        print(f"Not able to extract job time from file {infile}")
                        exit(1)

    for i, group in enumerate(args.groups + [None]):
        order = np.argsort(tot_proc[group]) # get the indices in sorted order
        # sort all the dicts according to the same order
        for d in [wall_time, inv_time, tot_proc, nodes, ppn, jnames, jids]:
            d[group] = [d[group][i] for i in order]

        # the run with the fewest processes has a speedup of 1.0, this is the baseline t0 and p0
        exec(f'time = {args.s}')
        t0 = time[group][0] if 0 < len(time[group]) else 1.0
        p0 = tot_proc[group][0] if 0 < len(tot_proc[group]) else 1
        speedup[group] = [t0/t for t in time[group]] # calculate speedup wrt args.s
        peff[group] = [100*p0*s/p for s,p in zip(speedup[group], tot_proc[group])] # calculate PE wrt speedup in %

    flat_tot_proc = []
    for sublist in tot_proc:
        for item in tot_proc[sublist]:
            flat_tot_proc.append(item)

    ideal_scaling_x = [min(flat_tot_proc), max(flat_tot_proc)]
    ideal_scaling_y = [1, max(flat_tot_proc)/min(flat_tot_proc)]

    # render the table
    if args.table_fmt != 'hidden':
        for i, group in enumerate(args.groups + [None]):
            if len(speedup[group]) > 0:
                io_percent = 100*(1-np.array(inv_time[group])/np.array(wall_time[group]))
                nodes_hours = (np.array(wall_time[group])/3600)*np.array(nodes[group])
                table = zip(
                    nodes[group],
                    tot_proc[group],
                    ppn[group],
                    wall_time[group],
                    io_percent,
                    nodes_hours,
                    speedup[group],
                    peff[group],
                    jnames[group],
                    jids[group]
                )
                print(f"\nTable for {args.title}, {args.labels[i]}:")
                print(tabulate.tabulate(table,
                    headers = [
                        '# nodes',
                        'Total # proceses',
                        '# processes/node',
                        'Wall-time [s]',
                        'I/O time [%]',
                        'Node hours [h]',
                        'Speed-up*',
                        'Parallel efficiency** [%]',
                        'Slurm job name',
                        'Slurm jod id'
                    ],
                    tablefmt = args.table_fmt,
                    floatfmt = (None, None, None, None, ".1f", ".1e", ".1f", ".1f")
                ))
                print(f"*Speed-up is calculated wrt {args.s}")
                print(f"**Parallel efficiency is calculated as p_0*t_0/(p_n*t_n), where p_i is the number of processes and t_i the time it took on p_i processes")

    ####################################################################################################
    ####################################### Plotting starts here #######################################
    ####################################################################################################

    mx, mn = 0, 9999999
    for group in args.groups + [None]:
        if len(tot_proc[group]) != 0:
            mx = max(tot_proc[group]) if max(tot_proc[group]) > mx else mx
            mn = min(tot_proc[group]) if min(tot_proc[group]) < mn else mn

    log_max = int(np.log2(mx))
    log_min = int(np.log2(mn))

    xticks = [int(2**x) for x in np.arange(log_min, log_max+1)]
    xticks.append(2**(log_max+1.4))

    # labels for the ticks
    xticklabels = [str(t) for t in xticks[:-1]]
    for n in range(len(xticks[:-1])):
        for i, group in enumerate(args.groups + [None]):
            if len(speedup[group]) > 0:
                if n < len(nodes[group]):
                    xticklabels[n] += '\n' + str(nodes[group][n])
                else:
                    xticklabels[n] += '\nN/A'

    xticklabels.append("# processes")

    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.2, left=0.1, top=0.9, right=0.9)
    ax.set_title(args.title)

    for i, group in enumerate(args.groups + [None]):
        if len(speedup[group]) > 0:
            xticklabels[-1] += "\n# nodes".format(args.labels[i])

    ax.plot(ideal_scaling_x, ideal_scaling_y, '-', label = 'Ideal scaling')
    ideal_scaling_y[-1] *= 0.5
    ideal_scaling_y[0] *= 0.5
    ax.plot(ideal_scaling_x, ideal_scaling_y, '--', label = '50% of Ideal scaling')
    for i, group in enumerate(args.groups + [None]):
        if len(speedup[group]) > 0:
            ax.plot(tot_proc[group], speedup[group], 'o-', label = args.labels[i])

    ax.set_yscale('log', base=10)
    ax.set_xscale('log', base=2)
    ax.set_ylabel("Speed-up")
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticklabels)

    plt.legend()
    if args.fname is not None:
        plt.savefig(args.fname)
    else:
        plt.draw()
        plt.show()
