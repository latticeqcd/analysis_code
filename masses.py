#!/usr/bin/env python3
#
# Check an exported gauge configuration for validity
#

import argparse
from uncertainties import ufloat, ufloat_fromstr
import json # json.dumps()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Calc quark masses')

    delta = ufloat_fromstr("0.0097(3)")/2.0
    m_prime = ufloat_fromstr("-0.27928(4)")

    print(f"{m_prime = }")
    print(f"{delta = }")

    m_ud = m_prime - delta/3.0
    k_ud = 0.5/(m_ud+4.0)

    m_s = m_prime + 2.0*delta/3.0
    k_s = 0.5/(m_s+4.0)

    print(f"{m_ud = }")
    print(f"{k_ud = }")
    print(f"{m_s = }")
    print(f"{k_s = }")

