#!/usr/bin/env python3
#
# Caclulate and plot the gauge noise.
#

import argparse
import numpy as np
import scipy as sc
import os
from itertools import tee, islice, chain, product
from glob import glob
from braceexpand import braceexpand
import struct
import re
from tqdm import tqdm
from termcolor import colored
import mass as ms
from uncertainties import ufloat, unumpy
import itertools
import matplotlib.pyplot as plt
import json
import shlex
import sys
import logging

logger = logging.getLogger("utils")

def braced_glob(path):
    lst = []
    for x in braceexpand(path):
        lst.extend(glob(x))
    return lst

def previous_and_next(some_iterable):
    prevs, items, nexts = tee(some_iterable, 3)
    prevs = chain([None], prevs)
    nexts = chain(islice(nexts, 1, None), [None])
    return zip(prevs, items, nexts)

def infile_list(infiles):
    lst = []
    for _, element, nxt in previous_and_next(infiles):
        if os.path.isfile(element):
            lst.append(element)
        if os.path.isdir(element):
            if nxt is not None:
                l = braced_glob(f"{element}/{nxt}")
                if (len(l) == 0):
                    logger.error(colored(f"No files matching {element}/{nxt}", 'red'))
                    exit()
                logger.debug(f"adding {len(l)} files from directory {element} using glob {nxt}")
                lst.extend(l)
            else:
                raise argparse.ArgumentTypeError(f"{nxt} is not a shell glob")
    return sorted(lst)

def group_by_configs(infiles, regex=r'\/([^\/]+)n(?=([0-9]+)_)'):
    dic = {}
    for infile in infiles:
        if (m := re.search(regex, infile)):
            run_name = m.group(1)
            cnfg = int(m.group(2))
            if cnfg not in dic:
                dic[cnfg] = []
            dic[cnfg].append(infile)

    logger.debug(f"added configs {dic.keys()} to dict")
    lens = np.array([len(v) for v in dic.values()])
    if not np.all(lens == lens[0]):
        logger.error(colored('Error: not all configs have the same amount of sources', 'red'))
        exit()

    return dict(sorted(dic.items())), lens[0], len(dic.keys()), run_name

def read_coords(infile):
    int_size = 4 # 4 bytes
    with open(infile, "rb") as f:
        N0 = struct.unpack('<1I', f.read(int_size))[0] # 1 unsigned int I, < little endian
        N1 = struct.unpack('<1I', f.read(int_size))[0]
        N2 = struct.unpack('<1I', f.read(int_size))[0]
        N3 = struct.unpack('<1I', f.read(int_size))[0]
        return N0, N1, N2, N3

def read_dat(infile):
    int_size = 4 # 4 bytes
    double_size = 8 # 8 bytes IEEE binary64 float
    with open(infile, "rb") as f:
        N0 = struct.unpack('<1I', f.read(int_size))[0] # 1 unsigned int I, < little endian
        N1 = struct.unpack('<1I', f.read(int_size))[0]
        N2 = struct.unpack('<1I', f.read(int_size))[0]
        N3 = struct.unpack('<1I', f.read(int_size))[0]
        data = struct.unpack('<{0}d'.format(2*N0), f.read(double_size*2*N0)) # 2*N0 doubles
        G = np.zeros(N0, dtype=np.complex128)
        G.real = np.array(data[0:2*N0:2]) # only even indices, real part
        G.imag = np.array(data[1:2*N0+1:2])  # only odd indices, imag part
        return G

def write_dat(G, N, outfile):
    int_size = 4 # 4 bytes
    double_size = 8 # 8 bytes IEEE binary64 float
    with open(outfile, "wb") as f:
        f.write(struct.pack('<1I', N[0]))
        f.write(struct.pack('<1I', N[1]))
        f.write(struct.pack('<1I', N[2]))
        f.write(struct.pack('<1I', N[3]))
        f.write(struct.pack('<{0}d'.format(2*N[0]), *G.view(np.float64)))

def plot(Y, X = None, **kwargs):
    opts = {
        'xerr': 0,
        'yerr': unumpy.std_devs(Y),
        'capsize': 3,
        'alpha': 0.5,
        'linewidth': 1
    }
    opts = dict(list(opts.items()) + list(kwargs.items()))
    if X is None:
        X = np.arange(Y.shape[0]) + plot.shift*(-1.0)**plot.counter
    plot.shift += 0.025
    plot.counter += 1
    return plt.errorbar(X, unumpy.nominal_values(np.real(Y)), **opts)
plot.shift = 0.0
plot.counter = 0

# Fold the deepest dimension of a numpy array
def fold(G):
    N0 = G.shape[-1]
    r1 = np.arange(0, int(N0/2))
    r2 = np.flip(np.concatenate((np.arange(int(N0/2)+1, N0), [0])))
    return np.mean([G[...,r1], G[...,r2]], axis=0)

ufloat_ = lambda tup: ufloat(*tup) # use ufloat(nominal_value, std_dev), not ufloat(tuple)

def collapse(x):
    return x.item() if len(np.atleast_1d(x)) == 1 else x

def mean(arr, **kwargs):
    #return unumpy.uarray(np.mean(arr, **kwargs), sc.stats.sem(arr, **kwargs))
    return collapse(np.array(list(map(ufloat_, zip(np.atleast_1d(np.mean(arr, **kwargs)), np.atleast_1d(sc.stats.sem(arr, **kwargs)))))))

def mean_ax0(d):
    return np.mean(d, axis=0)

# def gauge_noise(G):
#     return mean(G[0]*G[1], axis=0) - mean(G[0], axis=0)*mean(G[1], axis=0)

def gn(d):
    return np.mean(d**2, axis=0) - np.mean(d, axis=0)**2

def gauge_variance(G):
    N0, nsrc = G.shape[-1], G.shape[1]
    G = np.real(G)
    length = int((nsrc*(nsrc-1))/2)
    #sigma = unumpy.uarray(np.zeros(N0, dtype=np.float64), np.zeros(N0, dtype=np.float64))
    sigma = np.zeros(N0, dtype=np.float64)
    for i,j in itertools.combinations(range(nsrc), 2):
        #sigma += gauge_noise((G[:,i], G[:,j]))
        sigma += (np.mean(G[:,i]*G[:,j], axis=0) - np.mean(G[:,i], axis=0)*np.mean(G[:,j], axis=0))
    return sigma/length

def gauge_noise_second_part(G):
    nsrc = G.shape[1]
    second_part = np.zeros(G.shape[-1], dtype=np.float64)
    for i in range(nsrc):
        second_part += np.mean(G[:,i]**2, axis=0)
    second_part /= nsrc
    return second_part

def var(d):
    return np.var(d, axis=0)

def correlator_variance(G):
    nsrc, sigma = G.shape[1], 0.0
    G = np.real(G)
    for i,j in product(range(nsrc), range(nsrc)):
        sigma += (np.mean(G[:,i]*G[:,j], axis=0) - np.mean(G[:,i], axis=0)*np.mean(G[:,j], axis=0))
    return sigma/(nsrc*nsrc)

#
# Apply the jackknife resampling method to a data set along an axis.
#
#from tqdm.contrib.concurrent import process_map
def jackknife_old(data, fn, axis=0, bar=range):
    data = np.array(data)
    f = lambda x: unumpy.nominal_values(fn(x))
    theta_hat = f(data)
    n = data.shape[axis]

    #all_data = np.array([np.delete(data, k, axis=axis) for k in range(n)])
    #d = {
    #     "max_workers": 32,
    #     "desc": f"jackknife of {fn.__name__}"
    # }
    #r = np.array(list(map(unumpy.nominal_values, process_map(fn, all_data, **d))))
    #with tqdm(total=n*23653) as bar:
    # with multiprocessing.Pool(10) as p:
    #     r = np.array(list(map(unumpy.nominal_values, p.map(fn, all_data))))

    r = np.array([f(np.delete(data, k, axis=axis)) for k in bar(n)])
    j_est = np.mean(r, axis=0)
    bias = (n-1)*(theta_hat - j_est)
    variance = ((n-1)/n)*np.sum(np.fromiter((r[k] - j_est - bias)**2 for k in range(n)))
    error = np.sqrt(variance)
    if np.atleast_1d(j_est).shape == (1,):
        return ufloat(j_est, error)
    else:
        return np.array(list(map(ufloat_, zip(j_est, error))))

#
# Apply the jackknife resampling method to a data set along an axis.
#
# :param      data:  Input data
# :type       data:  ndarray
# :param      fn:    Function to estimate the mean and variance of
# :type       fn:    Callable
# :param      axis:  The axis in data over which to jackknife
# :type       axis:  int
# :param      bar:   A potential progress bar
# :type       bar:   Callable that returns an iterable
#
# :returns:   Result as a numpy array of ufloats
# :rtype:     uarray (numpy array of dtype=ufloats)
#
def jackknife(data, fn, axis=0, bar=range):
    n = data.shape[axis]
    if n == 1:
        m = np.mean(data, axis=axis)
        return unumpy.uarray(m, np.zeros_like(m))
    r = np.array([unumpy.nominal_values(fn(np.delete(data, k, axis=axis))) for k in bar(n)])
    j_est = np.mean(r, axis=0)
    bias = (n-1)*(unumpy.nominal_values(fn(data)) - j_est)
    variance = ((n-1)/n)*sum((r[k] - j_est)**2 for k in range(n))
    error = np.sqrt(variance)
    return unumpy.uarray(j_est, error)

def iter(it, **kwargs):
    return tqdm(it, **kwargs) if logger.getEffectiveLevel() == logging.DEBUG else it

def bar(**kwargs):
    if logger.getEffectiveLevel() == logging.DEBUG:
        return lambda n: tqdm(range(n), **kwargs)
    return range

def set_ticks(plt, ticks, labels):
    ax = plt.gca()
    for tick in ax.xaxis.get_minor_ticks():
        tick.label1.set_visible(False)
    plt.xticks(ticks=ticks, labels=labels)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description = 'Calculate the gauge-noise from correlators.')
    parser.add_argument("-v", "--verbosity", action="count", help="increase output verbosity", default=0)
    parser.add_argument('--fold', help="fold the correlator", default=False, action="store_true")
    parser.add_argument('-r', '--resampling', help="resampling method to use", default="jackknife", type=str, choices=['jackknife', 'std'],)
    parser.add_argument('-o', '--outfile', help='Output filename', default=None)
    parser.add_argument('--nsrc', help='Limit the number of sources for the correlator, if not given, the program tries to determine itself', nargs="+", type=str, default=None)
    parser.add_argument('--nconfigs', help='Limit the number of configs for the correlator, if not given, the program tries to determine itself', type=int, default=None)
    parser.add_argument('-t', '--title', help='Plot title', default=None)
    parser.add_argument('--plot-appx', help="plot the variance; sigma^2_appx", default=False, action="store_true")
    parser.add_argument('--plot-vol', help="plot sigma^2_vol", default=False, action="store_true")
    parser.add_argument('--plot-corr', help="plot the correlator", default=False, action="store_true")
    parser.add_argument('--plot-nsrc', nargs="+", help="plot variance at time t versus number of sources", type=int, default=None)

    required = parser.add_argument_group('required named arguments')
    required.add_argument('-i', '--infile',
        help='input file(s) containing the extracted data (from meson.c).',
        metavar='FILE',
        required=True,
        nargs='+'
    )

    args = parser.parse_args()
    logger.setLevel(logging.DEBUG if args.verbosity else logging.INFO)
    logger.addHandler(logging.StreamHandler()) # add handlers to logger

    logger.debug("Running with the following arguments:")
    logger.debug(json.dumps(vars(args), indent=4))

    if args.resampling == 'std':
        method = lambda data, fn, **kwargs: fn(data)
    elif args.resampling == 'jackknife':
        method = jackknife

    # #srun ./time2 -bc 3 -cs 3 -gg 3
    # ntask = [1024, 2048, 4096, 8192]
    # ms_roman = [2*17.93664, 2*9.191232, 2*5.959008, 2*3.070224]
    # ms_isabel = [36.23, 18.61, 10.74, 5.92]
    # sp = [1.00, 1.95, 3.01, 5.84]
    # ms_ideal = [2*17.93664, 2*17.93664/2, 2*17.93664/4, 2*17.93664/8]
    # fig = plt.figure()
    # ax=plt.subplot2grid((1,1),(0,0))
    # s2=r'''\begin{tabular}{ c | c } \# cores & speedup \\\hline 1024 & 1.00 \\\hline 2048 & 1.95 \\\hline 4096 & 3.01 \\\hline 8192 & 5.84 \end{tabular}'''
    # plt.rc('text', usetex=True)
    # ax.text(0.1, 0.2, s2, transform=ax.transAxes, size=10)
    # plt.title(f"96x48x48x48 lattice bc=periodic, cs=3, gauge=SU(3)xU(1)");
    # plt.plot(ntask, ms_roman, "bo-", label="measured (Roman)")
    # plt.plot(ntask, ms_isabel, "ko-", label="measured (Isabel)")
    # plt.plot(ntask, ms_ideal, "r-", label="ideal scaling")
    # plt.xscale('log')
    # plt.yscale('log')
    # plt.xlabel("number of cores")
    # plt.ylabel("time to solution [ms]")
    # plt.legend()
    # plt.savefig(args.outfile)
    # exit()

    # data = np.arange(10)
    # print(data)
    # print(f"mean = {mean(data):.16f}")
    # print(f"jn = {jackknife(data, mean):.16f}")
    # exit()

    def slice(N, nmx, zero="0"):
        if N is None:
            N = [str(nmx)]
        if zero in N:
            N[N.index(zero)] = str(nmx)
        R = []
        for n in list(set(N)):
            if n.isnumeric():
                R.append(int(n))
            elif ":" in n:
                R.extend(list(range(*tuple(map(int, n.split(":"))))))
        return sorted(list(set(R)))

    infiles = infile_list(args.infile)
    N0, _, _, _ = read_coords(infiles[0])
    N0h = int(N0/2)
    infiles, nsrc, nconfigs, run_name = group_by_configs(infiles)
    args.nsrc = slice(args.nsrc, nsrc)
    args.nconfigs = nconfigs if args.nconfigs is None else args.nconfigs
    G_orig = np.zeros((args.nconfigs, nsrc, N0), dtype=np.complex128)
    G = {}

    for n, (cfg, files) in iter(enumerate(infiles.items()), desc="reading dat-files", total=len(infiles)):
        if n < args.nconfigs:
            for m, file in enumerate(files):
                if m < nsrc:
                    G_orig[n,m] = read_dat(file)

    logger.debug(f"{G_orig.shape = } = (nconfigs, nsrc, N0) (initial)")

    if args.fold:
        G_orig = fold(G_orig)
    logger.debug(f"{G_orig.shape = } = (nconfigs, nsrc, N0/2) (after folding)")

    plot_nsrc = sorted(list(set(filter(lambda x: 0<x<=nsrc, args.nsrc))))
    for i,n in enumerate(plot_nsrc):
        G[n] = G_orig[:,0:n,:]
        logger.debug(f"{G[n].shape = } = (nconfigs, nsrc, N0/2) (inital)")

    if args.plot_vol or args.plot_nsrc is not None:
        sigma_vol = {}
        for i,n in enumerate(plot_nsrc):
            if n > 1:
                sigma_vol[n] = method(unumpy.nominal_values(np.real(G[n])), gauge_variance, axis=0, bar=bar(desc=f"calculating sigma_vol ({n} src)"))
                logger.debug(f"{sigma_vol[n].shape = } = (N0/2) (after averaging over configs)")

    #sigma_vol_old = method(G_orig, gauge_noise, axis=0, bar=bar(desc="calculating gauge noise"))
    #sigma_vol_old = gauge_noise(G_orig)
    #logger.debug(f"{sigma_vol_old.shape = } = (N0/2) (after averaging over configs)")

    #second_part = method(G_orig.real, gauge_noise_second_part, axis=0)

    if args.plot_appx or args.plot_nsrc is not None:
        sigma_appx = {}
        for i,n in enumerate(plot_nsrc):
            sigma_appx[n] = method(unumpy.nominal_values(np.real(G[n])), correlator_variance, axis=0, bar=bar(desc=f"calculating sigma_appx ({n} src)"))

    #G_orig = np.mean(G_orig, axis=1) # average over sources
    #logger.debug(f"{G_orig.shape = } = (nconfigs, N0/2) (after averaging over sources)")
    for i,n in enumerate(plot_nsrc):
        G[n] = np.mean(G[n], axis=1) # average over sources 
        logger.debug(f"{G[n].shape = } = (nconfigs, N0/2) (after averaging over sources)")

    #G_orig = mean(G_orig, axis=0)
    #G_var = np.var(G_orig, axis=0)
    #sigma_appx_old = method(G_orig, var)
    # if args.plot_appx:
    #     sigma_appx = {}
    #     for i,n in enumerate(plot_nsrc):
    #         sigma_appx[n] = method(G[n], var)
    #sigma_appx2_old = method(G_orig.real, gn)

    #G_orig = method(G_orig, mean_ax0)
    #logger.debug(f"{G_orig.shape = } (after averaging over configs)")
    if args.plot_corr:
        for i,n in enumerate(plot_nsrc):
            G[n] = method(unumpy.nominal_values(np.real(G[n])), mean_ax0)
            logger.debug(f"{G[n].shape = } (after averaging over configs)")
    #sigma_vol2 = G_orig**2 + sigma_appx_old - second_part

    fig = plt.figure()
    plt.suptitle(f"Lattice {run_name}" if args.title is None else args.title)
    plt.title(f"{args.nconfigs} configs, estimator {args.resampling}");

    if args.plot_corr:
        for n in plot_nsrc:
            plot(G[n], label=fr"$\langle G(t) \rangle$ ({n} src)")

    if args.plot_vol:
        for n in plot_nsrc:
            if n > 1:
                plot(sigma_vol[n], label=fr"$\sigma^2_{{vol}}$ ({n} src)")

    if args.plot_appx:
        for n in plot_nsrc:
            plot(sigma_appx[n], label=fr"$\sigma^2_{{appx}}$ ({n} src)")

    # if args.plot_appx and args.plot_vol:
    #     for n in plot_nsrc:
    #         if n > 1:
    #             plot(sigma_appx[n]-sigma_vol[n], label=fr"$\sigma^2_{{appx}} - \sigma^2_{{vol}}$ ({n} src)")

    if args.plot_nsrc is not None:
        import scipy.optimize
        for t in args.plot_nsrc:
            X = plot_nsrc.copy()
            if 1 in X:
                X.remove(1)
            X = np.array(X)
            # Y = np.array([sigma_appx[n][t]-sigma_vol[n][t] for n in X])
            # label = fr"$\sigma^2_{{appx}} - \sigma^2_{{vol}}$ at {t=}"
            # Y = np.array([sigma_appx[n][t] for n in X])
            # label = fr"$\sigma^2_{{appx}}$ at {t=}"
            Y = np.array([sigma_vol[n][t] for n in X])
            label = fr"$\sigma^2_{{vol}}$ at {t=}"
            Y /= np.mean(unumpy.nominal_values(Y))
            #Y /= Y[0].n
            model = lambda x, A, B: abs(A)/x + abs(B)
            model_inv = lambda y, A, B: (y - abs(B))/abs(A)
            popt, pcov = scipy.optimize.curve_fit(model, X, unumpy.nominal_values(Y),
                maxfev=9999,
                sigma=unumpy.std_devs(Y))
            variances = unumpy.std_devs(Y)**2
            dof = len(Y) - 2
            chi_red = np.sum((unumpy.nominal_values(Y) - model(X, *popt))**2/variances)/dof
            Xc = np.arange(np.min(X), np.max(X)+1)
            A, B = abs(popt)
            p = plot(Y, X, label=label)
            color = p[0].get_color()
            plt.plot(Xc, model(Xc, *popt), "-",
                color=color,
                label=fr"fit: $A x^{{-1}}+B$ ({A=:.1e}, {B=:.1e}) at {t=}, $\chi_r^2=${chi_red:.1e}")
        plt.xlabel(fr"$N_{{src}}$")
        plt.yscale('log')
        plt.xscale('log')
        set_ticks(plt, X, X)

    #plot(G_orig, label=fr"$\langle G(t) \rangle$")
    #plot(second_part, label=fr"$\frac{{1}}{{n_{{src}}}} \sum_i \langle G_i(t)^2 \rangle$")
    #plot(G_orig**2, label=fr"$\langle G(t) \rangle^2$")
    #plot(sigma_appx_old, label=fr"$\sigma^2_{{appx}} = Var(G(t))$ (using np.var)")
    #plot(sigma_appx2_old, label=fr"$\sigma^2_{{appx,2}} = Var(G(t)) = \langle G(t)^2 \rangle - \langle G(t) \rangle^2$ (using own implementation)")
    #plot(sigma_vol_old, label=fr"$\sigma^2_{{vol}} = \frac{{2}}{{n_{{src}}(n_{{src}}-1)}}\sum_{{i<j}}^{{n_{{src}}}} \langle G_i(t) G_j(t) \rangle - \langle G_i(t) \rangle \langle G_j(t) \rangle$")
    #plot(sigma_vol2, label=fr"$\sigma^2_{{vol,2}} = \sigma^2_{{appx}} + \langle G(t) \rangle^2 - \frac{{1}}{{n_{{src}}}} \sum_i^{{n_{{src}}}} \langle G_i(t)^2 \rangle$")
    #plot(-sigma_vol2, label=fr"$-\sigma^2_{{vol,2}}$")

    if args.plot_appx or args.plot_vol or args.plot_corr:
        plt.yscale('log')
    plt.legend(fontsize=6)

    if args.outfile is None:
        logger.debug(f"Showing plot")
        plt.draw()
        plt.show()
    else:
        logger.debug(f"Storing {args.outfile}")
        plt.savefig(args.outfile, dpi=fig.dpi, metadata={
            'Keywords': shlex.join(sys.argv),
            'Creator': os.getcwd(),
        })
