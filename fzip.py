#!/usr/bin/env python3
#
# Compress an exported quark or gauge field
#

import os
import sys
import argparse
import numpy as np
import struct # struct.unpack(), struct.pack()
from tqdm import tqdm
import numpy.linalg
import logging

int_size = 4
float_size = np.finfo(np.single).bits//8
double_size = np.finfo(np.double).bits//8

# su3_vector = np.dtype([('c1', np.complex64), ('c2', np.complex64), ('c3', np.complex64)])
# spinor = np.dtype([('c1', su3_vector), ('c2', su3_vector), ('c3', su3_vector), ('c4', su3_vector)])

# def safe_mod(x, y):
#     if x>=0:
#         return x%y
#     else:
#         return (y-(abs(x)%y))%y

# def index_geom(N, x0, x1, x2, x3, cbs, cbix, cbn):
#     xb1=x1%cbs[1]
#     xb2=x2%cbs[2]
#     xb3=x3%cbs[3]

#     xn1=int(x1/cbs[1])
#     xn2=int(x2/cbs[2])
#     xn3=int(x3/cbs[3])

#     ib=cbix[xb3+cbs[3]*xb2+cbs[2]*cbs[3]*xb1+cbs[1]*cbs[2]*cbs[3]*x0]
#     in1=xn3+cbn[3]*xn2+cbn[3]*cbn[2]*xn1

#     if (x0+x1+x2+x3)%2!=0:
#         ib+=int((N[0]*N[1]*N[2]*N[3])/2)

#     return ib+int((cbs[0]*cbs[1]*cbs[2]*cbs[3]*in1)/2)


# import itertools
# def setup_ipt(N):
#     VOLUME = np.product(N)
#     ipt = np.zeros(VOLUME, dtype=int)
#     cbs = np.zeros(4, dtype=int)
#     cpr = np.zeros(4, dtype=int)
#     cbn = np.zeros(4, dtype=int)

#     cbs[0]=N[0]
#     cbn[0]=1

#     for mu in range(1, 4):
#         if (N[mu]%4)==0:
#             cbs[mu]=4
#         elif (N[mu]%3)==0:
#             cbs[mu]=3
#         elif (N[mu]%2)==0:
#             cbs[mu]=2
#         else:
#             cbs[mu]=1
#         cbn[mu]=int(N[mu]/cbs[mu])

#     cbix = np.zeros(cbs[0]*cbs[1]*cbs[2]*cbs[3], dtype=int)

#     ig = iu = 0
#     for x0,x1,x2,x3 in itertools.product(*(range(n) for n in cbs)):
#         ib = x3 + cbs[3]*x2 + cbs[2]*cbs[3]*x1 + cbs[1]*cbs[2]*cbs[3]*x0
#         if (x0+x1+x2+x3)%2==0:
#             cbix[ib] = ig
#             ig += 1
#         else:
#             cbix[ib] = iu
#             iu += 1

#     for x0,x1,x2,x3 in itertools.product(*(range(n) for n in N)):
#         ix = index_geom(N,x0,x1,x2,x3,cbs,cbix,cbn)
#         iy = x3 + N[3]*x2 + N[2]*N[3]*x1 + N[1]*N[2]*N[3]*x0
#         ipt[iy] = ix

#     return ipt


# def import_sfld_complicated(infile):
#     with open(infile, "rb") as f:
#         # bit order is always little endian
#         # the first 4 integers are the lattice size
#         # 4 unsigned int I, < little endian
#         N = np.array(struct.unpack('<4I', f.read(4*int_size)))
#         #N[0] = 1
#         VOLUME = np.product(N)
#         ipt = setup_ipt(N)
#         sd = np.zeros((VOLUME,4,3), dtype=np.complex128)
#         sd2 = np.zeros((N[0],N[1],N[2],N[3],4,3), dtype=np.complex128)

#         # 1 double, < little endian
#         norm = struct.unpack('<1d', f.read(1*double_size))[0]

#         x = [0,0,0,0]
#         fmt = f"<{24*N[3]}d"
#         size = double_size*24*VOLUME
#         bar = tqdm(enumerate(struct.iter_unpack(fmt, f.read(size))),
#                    total=N[0]*N[1]*N[2], desc="import_sfld")
#         for ix, data in bar:
#             sbuf = np.array(data)
#             x[0]=int(ix/(N[1]*N[2]))
#             x[1]=int((ix/N[2])%N[1])
#             x[2]=ix%N[2]
#             iy=x[2]+N[2]*x[1]+N[1]*N[2]*x[0]
#             iy*=N[3];
#             for y3 in range(0, N[3]):
#                 iz=ipt[iy+y3];
#                 #iz = iy+y3
#                 x3 = y3*N[3]
#                 sd[iz,0,0] = complex(sbuf[x3+0], sbuf[x3+1])
#                 sd[iz,0,1] = complex(sbuf[x3+2], sbuf[x3+3])
#                 sd[iz,0,2] = complex(sbuf[x3+4], sbuf[x3+5])
#                 sd[iz,1,0] = complex(sbuf[x3+6], sbuf[x3+7])
#                 sd[iz,1,1] = complex(sbuf[x3+8], sbuf[x3+9])
#                 sd[iz,1,2] = complex(sbuf[x3+10], sbuf[x3+11])
#                 sd[iz,2,0] = complex(sbuf[x3+12], sbuf[x3+13])
#                 sd[iz,2,1] = complex(sbuf[x3+14], sbuf[x3+15])
#                 sd[iz,2,2] = complex(sbuf[x3+16], sbuf[x3+17])
#                 sd[iz,3,0] = complex(sbuf[x3+18], sbuf[x3+19])
#                 sd[iz,3,1] = complex(sbuf[x3+20], sbuf[x3+21])
#                 sd[iz,3,2] = complex(sbuf[x3+22], sbuf[x3+23])

#                 sd2[x[0],x[1],x[2],y3,0,0] = complex(sbuf[x3+0], sbuf[x3+1])
#                 sd2[x[0],x[1],x[2],y3,0,1] = complex(sbuf[x3+2], sbuf[x3+3])
#                 sd2[x[0],x[1],x[2],y3,0,2] = complex(sbuf[x3+4], sbuf[x3+5])
#                 sd2[x[0],x[1],x[2],y3,1,0] = complex(sbuf[x3+6], sbuf[x3+7])
#                 sd2[x[0],x[1],x[2],y3,1,1] = complex(sbuf[x3+8], sbuf[x3+9])
#                 sd2[x[0],x[1],x[2],y3,1,2] = complex(sbuf[x3+10], sbuf[x3+11])
#                 sd2[x[0],x[1],x[2],y3,2,0] = complex(sbuf[x3+12], sbuf[x3+13])
#                 sd2[x[0],x[1],x[2],y3,2,1] = complex(sbuf[x3+14], sbuf[x3+15])
#                 sd2[x[0],x[1],x[2],y3,2,2] = complex(sbuf[x3+16], sbuf[x3+17])
#                 sd2[x[0],x[1],x[2],y3,3,0] = complex(sbuf[x3+18], sbuf[x3+19])
#                 sd2[x[0],x[1],x[2],y3,3,1] = complex(sbuf[x3+20], sbuf[x3+21])
#                 sd2[x[0],x[1],x[2],y3,3,2] = complex(sbuf[x3+22], sbuf[x3+23])

#     return N, norm, sd, sd2

# def import_sfld(infile):
#     with open(infile, "rb") as f:
#         # bit order is always little endian
#         # the first 4 integers are the lattice size
#         # 4 unsigned int I, < little endian
#         N = np.array(struct.unpack('<4I', f.read(4*int_size)))
#         VOLUME = np.product(N)
#         sd = np.zeros(24*VOLUME, dtype=np.float64)

#         # 1 double, < little endian
#         norm = struct.unpack('<1d', f.read(1*double_size))[0]

#         # 24 VOLUME doubles, < little endian
#         sd = np.array(struct.unpack(f"<{24*VOLUME}d", f.read(double_size*24*VOLUME)))

#     return N, norm, sd

# def import_cnfg(infile):
#     with open(infile, "rb") as f:
#         # bit order is always little endian
#         # the first 4 integers are the lattice size
#         # 4 unsigned int I, < little endian
#         N = np.array(struct.unpack('<4I', f.read(4*int_size)))
#         N[0] = 1
#         VOLUME = np.product(N)

#         size1 = (18*4*VOLUME + 1)*double_size + 4*int_size  # pure SU(3)
#         size2 = (4*VOLUME + 1)*double_size + 4*int_size     # pure U(1)
#         size3 = (19*4*VOLUME + 2)*double_size + 4*int_size  # SU(3)xU(1)
#         file_size = os.path.getsize(infile)

#         # if file_size != size1:
#         #     logger.warning("Only SU(3) gauge fields are currently supported.")
#         #     sys.exit(1)

#         data = np.zeros(18*4*VOLUME, dtype=np.float64)

#         # 1 double, < little endian
#         plaq = struct.unpack('<1d', f.read(1*double_size))[0]

#         # 18*4*VOLUME doubles, < little endian
#         data = np.array(struct.unpack(f"<{18*4*VOLUME}d", f.read(double_size*18*4*VOLUME)))

#     return N, plaq, data


def reduce_su3(u):
    """
    Take a 3x3 complex SU(3)-matrix and calculate 8 real values out of it.
    
    see:
    https://www.sciencedirect.com/science/article/pii/0010465586901116?ref=cra_js_challenge&fr=RR-1
    
    :type       ud:   Array of shape (3, 3) with complex numbers
    :param      ud:   1darray
    
    :returns:   Array of shape (8) with real numbers
    :rtype:     1darray
    """
    a1, a2, a3 = u[0,0], u[0,1], u[0,2]
    b1 = u[1,0]
    c1 = u[2,0]
    theta1 = numpy.angle(a1)
    theta2 = numpy.angle(c1)
    # we store: a2 a3, b1 and theta1, theta2
    return np.array([a2.real, a2.imag, a3.real, a3.imag, b1.real, b1.imag, theta1, theta2])


def compress_su3(ud):
    sd = np.zeros((ud.shape[0], 8), dtype=np.float64)
    for i, u in enumerate(tqdm(ud, desc="compressing SU(3)")):
        sd[i] = reduce_su3(u)

    return sd

def reconstruct_su3(s):
    """
    Take an array of 8 real values and calculate and array of 3x3 complex SU(3)
    matrices out of if.
    
    :param      sd:   Array of shape (8) with real numbers
    :type       sd:   1darray
    
    :returns:   Array of shape (3, 3) with complex numbers
    :rtype:     2darray
    """
    a2 = complex(s[0], s[1])
    a3 = complex(s[2], s[3])
    b1 = complex(s[4], s[5])
    theta1, theta2 = s[6], s[7]

    # reconstruct a1, c1
    a1 = complex(np.sqrt(1 - np.abs(a2)**2 - np.abs(a3)**2)*np.cos(theta1),
                 np.sqrt(1 - np.abs(a2)**2 - np.abs(a3)**2)*np.sin(theta1))
    c1 = complex(np.sqrt(1 - np.abs(a1)**2 - np.abs(b1)**2)*np.cos(theta2),
                 np.sqrt(1 - np.abs(a1)**2 - np.abs(b1)**2)*np.sin(theta2))

    N = np.sqrt(np.abs(a2)**2 + np.abs(a3)**2)
    p1 = np.conj(c1)/N
    p2 = b1/N

    m1 = np.matrix([
        [ 1, 0,            0           ],
        [ 0, p1,           p2          ],
        [ 0, -np.conj(p2), np.conj(p1) ]
    ])

    m2 = np.matrix([
        [ a1, a2,                a3                ],
        [ 0,  -np.conj(a3)/N,    np.conj(a2)/N     ],
        [ N,  -np.conj(a1)*a2/N, -np.conj(a1)*a3/N ]
    ])

    return m1.dot(m2)


def uncompress_su3(sd):
    size = sd.shape[0]
    ud = np.zeros((size, 3, 3), dtype=np.complex128)
    for i, s in enumerate(tqdm(sd, desc="uncompressing SU(3)")):
        ud[i] = reconstruct_su3(s)

    return ud

def next_power_of_2(x):
    return int(2 if x <= 2 else (int(x) - 1).bit_length())

def exponent_to_int(ba):
    """
    Takes N exponent bits and converts them to an unsigned 32-bit integer
    ba is a bitarray containing N=len(ba) bits

    example:
    ba = 01111110001 # 1009 in decimal
    bias = 1023
    bs = 00000000000000000000001111110001
    integer = bs.read(np.int32, 1)[0] # 1009
    integer-bias = -14

    :param      ba:   list of booleans representing the bitarray
    :type       ba:   list

    :returns:   integer of the bit representation of ba - bias
    :rtype:     signed int
    """
    exp_bits = len(ba)
    bs = BitStream()
    bias = 2**(exp_bits-1)-1
    bs.write((32-exp_bits)*[False], bool) # zero padding
    bs.write(ba)

    return bs.read(np.int32, 1)-bias

def uint32_to_uintN(i32, N):
    """
    Cut off the leading 32-N bits

    :param      i32:  32 bit integer
    :type       i32:  int
    :param      N:    how many bits to keep (N>=0)
    :type       N:    int

    :returns:   list of booleans representing the remaining bits
    :rtype:     list
    """
    bs = BitStream()
    bs.write(i32)
    bs.read(bool, 32-N)
    return bs.read(bool, N)


def uintN_to_exponent(stream, min_exp=0):
    """
    Reconstruct the exponent

    example:
    min_exp = -20
    bias = 1023
    N = 5
    stream = 00111 (6 in decimal)
    bs = 00000000000000000000000000000111
    integer = bs.read(np.uint32, 1)[0] = 6
    integer += min_exp # -14
    integer += bias    # 1009 (00000000000000000000001111110001 in binary)
    bs.write(np.int32(integer))
    bs.read(32-11) # 000000000000000000000
    return bs.read(11) # 01111110001

    :param      stream:   input stream of bits
    :type       stream:   BitStream
    :param      min_exp:  The minimum exponent
    :type       min_exp:  int

    :returns:   list of booleans representing the exponent
    :rtype:     list
    """
    bias = 2**(11-1)-1 # 1023
    N = len(stream)
    bs = BitStream()

    bs.write([False]*(32-N))
    bs.write(stream)
    integer = np.int32(bs.read(np.int32, 1)[0] + min_exp + bias)
    bs.write(np.int32(integer))
    bs.read(32-11)
    return bs.read(11)

def mantissa_bits(tol):
    return int(np.min([int(np.ceil(np.log2(1/tol)-1)), 52]))


import dill
from bitstream import BitStream

def compress(inarray, tol, compress_exponent, compress_mantissa):
    """
    Compress an array.
    
    :param      inarray:            The input array to compress
    :type       inarray:            numpy.ndarray
    :param      tol:                Tolerance up to which the mantissa should be
                                    compressed (only respected if
                                    compress_mantissa is true)
    :type       tol:                bool
    :param      compress_exponent:  Whether to compress the exponent bits or not
    :type       compress_exponent:  bool
    :param      compress_mantissa:  Whether to compress the mantissa bits or not
    :type       compress_mantissa:  bool
    
    :returns:   Dictionary holding the bitstream and information needed for
                uncompressing
    :rtype:     dict

    The return dictionary looks as follows:

    {
        "shape": tuple, # the original shape of inarray, i.e. just inarray.shape
        "nman": int, # number of remaining mantissa bits after compression
        "nexp": int, # number of remaining exponent bits after compression
        "min_exp": int, # the minimal exponent appearing in inarray
        "post_process": callable, # a post-processing function (inverse of the pre-processing function)
        "post_process_desc": str, # description of the pre-/post-processing
        "data": BitStream # compressed data as a stream of bits
    }

    """
    #nman = int(np.min([int(np.ceil(np.log2(1/tol)-1)), 52])) if compress_mantissa else 52
    nman = mantissa_bits(tol) if compress_mantissa else 52
    arr = np.array(inarray.flatten(), dtype=np.float64)
    size = np.prod(inarray.shape)
    orig = BitStream()
    new = BitStream()

    nzeros = (np.abs(arr) < tol/10).sum()
    logger.info((f"number of elements with magnitude smaller than {tol/10}:"
                 f" {nzeros}, (fraction: {nzeros/size})"))

    logger.info(f"mantissa bits: 52 -> {nman}")
    logger.info(f"original number range: {np.min(arr)} - {np.max(arr)}")

    # scale = np.max(np.abs(arr))
    # pre_process = lambda x: x/scale
    # post_process = lambda x: x*scale # must be the inverse of pre_process
    # post_process_desc = f"Rescale by a factor {scale}"

    pre_process = lambda x: x
    post_process = lambda x: x # must be the inverse of pre_process
    post_process_desc = f"noop"

    arr = pre_process(arr)
    logger.info(f"new number range: {np.min(arr)} - {np.max(arr)} ({post_process_desc})")

    if compress_exponent:
        exp = np.zeros(size, dtype=np.int32)
        orig.write(arr)
        orig.write([False], bool) # add one more bit, such that the 53 in the last loop does not fail
        orig.read(bool, 1)
        for i in tqdm(range(size), desc="compressing exponent"):
            exp[i] = exponent_to_int(orig.read(bool, 11))
            orig.read(bool, 53)

        min_exp = np.min(exp)
        rexp = np.max(exp) - min_exp +1 # +1 to be sure that we have enough exponent bits
        nexp = next_power_of_2(rexp)
        logger.info(f"exponent range: {min_exp} - {np.max(exp)}, range: {rexp}")
        logger.info(f"exponent bits: 11 -> {nexp}")
        exp -= min_exp
        logger.info(f"{exp = }, {np.min(exp)} - {np.max(exp)}")
    else:
        min_exp = None
        nexp = 11

    orig.write(arr)
    if compress_exponent:
        for i in tqdm(range(size), desc="compressing mantissa" if compress_mantissa else "compressing"):
            new.write(orig.read(bool, 1))
            orig.read(bool, 11) # skip the exponent; we already have it in exp[i]
            new.write(uint32_to_uintN(exp[i], nexp))
            new.write(orig.read(bool, nman))
            orig.read(bool, 52-nman)
    else:
        for i in tqdm(range(size), desc="compressing mantissa" if compress_mantissa else "compressing"):
            new.write(orig.read(bool, nman+12))
            orig.read(bool, 52-nman)

    # some trailing 0s to be byte-aligned
    ntrailing = 8 - (size*(1+nman+nexp) % 8)
    nbytes = int((size*(1+nman+nexp) + ntrailing)/8)
    new.write([False]*ntrailing)

    return {
        "shape": inarray.shape,
        "nman": nman,
        "nexp": nexp,
        "min_exp": min_exp,
        "post_process": post_process,
        "post_process_desc": post_process_desc,
        "data": new.read(type=bytes, n=nbytes)
    }


def uncompress(d):
    """
    Uncompress an array.
    
    :param      d:    Dictionary returned by compress()
    :type       d:    dict
    
    :returns:   Array of the same shape as inarray, that was given to compress()
    :rtype:     numpy.ndarray
    """
    shape = d.get("shape")
    nman = d.get("nman")
    nexp = d.get("nexp")
    min_exp = d.get("min_exp")
    post_process = d.get("post_process")
    post_process_desc = d.get("post_process_desc")
    compressed_exponent = d.get("args").get("compress_exponent")
    compressed_mantissa = d.get("args").get("compress_mantissa")
    data = d.get("data")

    size = np.prod(shape)
    stream = BitStream()
    reconstructed = BitStream()
    stream.write(data)

    if compressed_exponent and compressed_mantissa:
        desc = "uncompressing exponent/mantissa"
    elif compressed_exponent and not compressed_mantissa:
        desc = "uncompressing exponent"
    elif not compressed_exponent and compressed_mantissa:
        desc = "uncompressing mantissa"
    else:
        desc = "uncompressing"

    if compressed_exponent:
        for i in tqdm(range(size), desc=desc):
            # sign
            reconstructed.write(stream.read(1))

            # exponent
            reconstructed.write(uintN_to_exponent(stream.read(type=bool, n=nexp), min_exp))

            # mantissa
            reconstructed.write(stream.read(nman))
            reconstructed.write([False]*(52-nman))
    else:
        for i in tqdm(range(size), desc=desc):
            reconstructed.write(stream.read(nman+12))
            reconstructed.write([False]*(52-nman))

    b = np.array(reconstructed.read(float, size))
    b = post_process(b)

    return b.reshape(shape)


def load_field(infile, offset):
    """
    Load a field.
    
    :param      infile:  The input file
    :type       infile:  str
    :param      offset:  The offset (see args.offset)
    :type       offset:  list
    
    :returns:   tuple holding the header and footer as byte-objects and the
                floating point data as numpy array
    :rtype:     tuple(bytes, numpy.ndarray, bytes)
    """
    with open(infile, "rb") as f:
        # header is offset[0] bytes long
        header = f.read(offset[0])

        file_size = os.path.getsize(infile) # bytes
        if offset[1] <= 0:
            size = int((file_size - offset[0] - abs(offset[1]))/8)
            nfooter = abs(offset[1]) # bytes
        else:
            size = int(offset[1]/8)
            nfooter = file_size - offset[0] - offset[1] # bytes

        fld = np.zeros(size, dtype=np.float64)

        # size doubles, < little endian
        fld = np.array(struct.unpack(f"<{size}d", f.read(double_size*size)))

        footer = f.read(nfooter)

    return header, fld, footer


def print_only_help(parser):
    for line in parser.format_help().splitlines():
        if not line.startswith("usage: "):
            print(line)

class HelpAction(argparse._HelpAction):
    def __call__(self, parser, namespace, values, option_string=None):
        # retrieve subparsers from parser
        subparsers_actions = [
            action for action in parser._actions
            if isinstance(action, argparse._SubParsersAction)]

        parser.print_usage()
        for subparsers_action in subparsers_actions:
            for _, subparser in subparsers_action.choices.items():
                subparser.print_usage()

        print_only_help(parser)
        for subparsers_action in subparsers_actions:
            # get all subparsers and print help
            for choice, subparser in subparsers_action.choices.items():
                subparser._positionals.title = f'{choice} positional arguments'
                subparser._optionals.title = f'{choice} optional arguments'
                print("\n" + "-"*os.get_terminal_size().columns)
                print_only_help(subparser)

        parser.exit()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compress/uncompress or compare exported quark or gauge fields in double precision.', add_help=False)
    parser.add_argument('-h', '--help', action=HelpAction, help='show this help message and exit')
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    parser.add_argument('-l', '--log', help="Log file", metavar='FILE', default=None)

    subparser = parser.add_subparsers(dest='command', description='sub-command help')
    zip_parser = subparser.add_parser('zip', help='Compress file(s)', description='subcommand zip: compress file(s)')
    zip_parser = zip_parser.add_argument_group("zip arguments")
    compress_exponent_help = "Compress the exponent bits (this is lossless and the uncompressed file will be bit-identical to the original file)"
    zip_parser.add_argument('-e', '--compress-exponent', help=compress_exponent_help, default=False, action="store_true")
    compress_mantissa_help = "Compress the mantissa bits (this is lossy and the uncompressed field will introduce errors of magnitude given by --tol)"
    zip_parser.add_argument('-m', '--compress-mantissa', help=compress_mantissa_help, default=False, action="store_true")
    compress_su3_help = "Interpret the input as an array of SU(3) matrices and rewrite the SU(3) fields in terms of their 8 generators. This is lossy compression and the uncompressed field will introduce errors of magnitude DBL_EPSILON ~ 1e-15. This will only work with fields, that have SU(3) symmetry."
    zip_parser.add_argument('-s', '--compress-su3', help=compress_su3_help, default=False, action="store_true")
    zip_parser.add_argument('-t', '--tol', help="Tolerance for lossy compression, i.e. |original_field - uncompressed_field|/|original_field| <= tol. (Default 1e-12)", type=float, default=1e-12)
    offset_help = "Offsets in the file, describing the position of the floating point data. The two arguments [A, B] describe the offset A>=0 of the postion of the first byte of the data (A = size of header in bytes), and B the end. If B<0 it is interpreted as offset from the back end of the file (-B = size of footer in bytes), if B>0 it describes the size of the floating point data in bytes (file_size - A - B = size of footer in bytes). Default [0, 0]."
    zip_parser.add_argument('-o', '--offset', help=offset_help, type=int, nargs=2, default=[0,0])
    zip_parser.add_argument('-i', '--in',
        help='quark/gauge field file(s) to compress (exported via export_sfld() or export_cnfg(), see modules/archive/archive.c)',
        dest='infile',
        metavar='FILE',
        required=True,
        nargs='+'
    )

    unzip_parser = subparser.add_parser('unzip', help='Uncompress file(s)', description='subcommand unzip: uncompress file(s)')
    unzip_parser.add_argument('-i', '--in',
        help='quark/gauge field file(s) to uncompress',
        dest='infile',
        metavar='FILE',
        required=True,
        nargs='+'
    )

    compare_parser = subparser.add_parser('compare', help='Compare two files', description='subcommand compare: compare two files')
    compare_parser = compare_parser.add_argument_group("compare arguments")
    compare_parser.add_argument('-t', '--tol', help="Maximal tolerance of the two fields, i.e. |field1 - field2|/|field1| <= tol. (Default 1e-12)", type=float, default=1e-12)
    compare_parser.add_argument('-o', '--offset', help=offset_help, type=int, nargs=2, default=[0,0])
    compare_parser.add_argument('-i', '--in',
        help='two quark or gauge field files to compare',
        dest='infile',
        metavar='FILE',
        required=True,
        nargs=2
    )

    analyze_parser = subparser.add_parser('analyze', help='Analyze file(s)', description='subcommand analyze: analyze file(s)')
    analyze_parser = analyze_parser.add_argument_group("analyze arguments")
    analyze_parser.add_argument('-o', '--offset', help=offset_help, type=int, nargs=2, default=[0,0])
    analyze_parser.add_argument('-s', '--compress-su3', help=compress_su3_help, default=False, action="store_true")
    analyze_parser.add_argument('-e', '--compress-exponent', help=compress_exponent_help, default=False, action="store_true")
    analyze_parser.add_argument('-m', '--compress-mantissa', help=compress_mantissa_help, default=False, action="store_true")
    analyze_parser.add_argument('-t', '--tol', help="Tolerance for lossy compression, i.e. |original_field - uncompressed_field|/|original_field| <= tol. (Default 1e-12)", type=float, default=1e-12)
    analyze_parser.add_argument('-i', '--in',
        help='quark/gauge field file(s) to analyze',
        dest='infile',
        metavar='FILE',
        required=True,
        nargs='+'
    )

    args = parser.parse_args()
    logger = logging.getLogger("root") 
    logger.setLevel(logging.INFO if args.verbose else logging.WARNING)
    logger.addHandler(logging.StreamHandler(sys.stdout)) # create console handler
    if args.log is not None:
        logger.addHandler(logging.FileHandler(args.log)) # create file handler

    # a = np.random.rand(20000)
    # d = compress(a, args.tol, args.compress_exponent, args.compress_mantissa)
    # d['args'] = vars(args)
    # d['N'] = [1,1,1,1]
    # b = uncompress(d)
    # print(f"norm diff = {np.linalg.norm(a-b)/np.linalg.norm(a)}")
    # exit()

    # print(args)
    # exit()

    if args.command == 'zip' and not args.compress_su3 and not args.compress_exponent and not args.compress_mantissa:
        logger.warning("nothing to do ...")
        sys.exit(1)

    if args.command == 'zip':
        for infile in args.infile:
            logger.info(f"reading field {infile}")
            header, fld, footer = load_field(infile, args.offset)

            if args.compress_su3:
                # View the elements as elements of size 128 bits and interpret them
                # as a complex number, where the first and second 64 bits are the
                # real and imaginary parts respectively. Then reshape as an array of
                # 3x3 complex matrices, and reshape back after compressing
                fld = compress_su3(fld.view(np.complex128).reshape((-1, 3, 3)))
                fld = fld.reshape(-1)

            d = compress(fld, args.tol, args.compress_exponent, args.compress_mantissa)
            d['header'] = header
            d['footer'] = footer
            d['args'] = vars(args)

            # fld2 = uncompress(d)
            # print(f"norm diff = {np.linalg.norm(fld-fld2)/np.linalg.norm(fld)}")
            # exit()

            logger.info(f"storing field {infile}.fzip")
            with open(f"{infile}.fzip", "wb") as fh:
                dill.dump(d, fh)
                d['data'] = None
                logger.info(f"compressed {d = }")

    elif args.command == 'unzip':
        for infile in args.infile:
            logger.info(f"reading field {infile}")
            with open(infile, "rb") as fh:
                d = dill.load(fh)
                fld = uncompress(d)

                header = d.get("header")
                footer = d.get("footer")
                compressed_su3 = d.get("args").get("compress_su3")

                if compressed_su3:
                    fld = uncompress_su3(fld.reshape((-1, 8)))
                    fld = fld.reshape(-1).view(np.float64)

                ndoubles = fld.shape[0]
                logger.info(f"reading field {os.path.splitext(infile)[0]}.unc")
                with open(f"{os.path.splitext(infile)[0]}.unc", 'wb') as out:
                    out.write(header)
                    out.write(struct.pack(f"<{ndoubles}d", *fld))
                    out.write(footer)
                    d['data'] = None
                    logger.info(f"uncompressed {d = }")

    elif args.command == 'compare':
        logger.info(f"reading field1 {args.infile[0]}")
        header1, fld1, footer1 = load_field(args.infile[0], args.offset)
        logger.info(f"reading field2 {args.infile[1]}")
        header2, fld2, footer2 = load_field(args.infile[1], args.offset)
        r = np.linalg.norm(fld1-fld2)/np.linalg.norm(fld1)
        logger.info(f"|field1 - field2|/|field1| = {r} ({args.tol = })")
        sys.exit(0 if r <= args.tol else 1)

    elif args.command == 'analyze':
        for infile in args.infile:
            logger.info(f"reading field {infile}")
            header, fld, footer = load_field(infile, args.offset)

            if args.compress_su3:
                fld = compress_su3(fld.view(np.complex128).reshape((-1, 3, 3)))
                fld = fld.reshape(-1)

            N = len(fld)
            # The nullbyte is needed, because else tabulate formats it as
            # number/float or whatever, even if casted to a string !?
            fmt = lambda x: [str(x[0]), f"{x[1]}\0", x[2] if len(x)>2 else f"{x[1]/N:.2f}\0"]

            from scipy.stats import entropy
            tests = [
                ("number of total elements", N),
                ("number of non-zero elements", np.count_nonzero(fld)),
                ("number of zero elements", np.count_nonzero(fld==0)),
                ("number of negative elements", np.count_nonzero(fld<0)),
                ("number of positive elements", np.count_nonzero(fld>0)),
                ("number of elements <1e-16 in magnitude", np.count_nonzero(abs(fld)<1e-16)),
                ("number of elements <1e-15 in magnitude", np.count_nonzero(abs(fld)<1e-15)),
                ("number of elements <1e-14 in magnitude", np.count_nonzero(abs(fld)<1e-14)),
                ("number of elements <1e-13 in magnitude", np.count_nonzero(abs(fld)<1e-13)),
                ("number of elements <1e-12 in magnitude", np.count_nonzero(abs(fld)<1e-12)),
                ("number of elements <1e-11 in magnitude", np.count_nonzero(abs(fld)<1e-11)),
                ("number of elements <1e-10 in magnitude", np.count_nonzero(abs(fld)<1e-10)),
                ("number of elements >1e-2 in magnitude", np.count_nonzero(abs(fld)>1e-2)),
                ("number of elements >1e-1 in magnitude", np.count_nonzero(abs(fld)>1e-1)),
                ("number of elements >1 in magnitude", np.count_nonzero(abs(fld)>1)),
                ("unique numbers", len(numpy.unique(fld))),
                ("highest number", f"{np.max(fld):.2e}", None),
                ("smallest number", f"{np.min(fld):.2e}", None),
                ("highest number in magnitude", f"{np.max(abs(fld)):.2e}", None),
                ("smallest number in magnitude", f"{np.min(abs(fld)):.2e}", None),
                ("norm", f"{np.linalg.norm(fld):.2e}", None),
            ]

            _, exponents = np.frexp(fld)
            exponents.sort()
            tests.append(("minimal exponent (base 2)", np.min(exponents), None))
            tests.append(("maximal exponent (base 2)", np.max(exponents), None))
            tests.append(("exponent range (base 2)", np.max(exponents) - np.min(exponents)+1, None))

            values, counts = np.unique(exponents, return_counts=True)
            tests.append(("distinct exponents (base 2)", len(values), None))
            # n = 4
            # for idx in np.argpartition(counts, -n)[-n:]:
            #     tests.append((f"numbers with exponent {values[idx]}", counts[idx]))
                
            for v,c in zip(values, counts):
                tests.append((f"numbers with exponent {v} (base 2)", c))

            nexp = next_power_of_2(np.max(exponents) - np.min(exponents)+1) if args.compress_exponent else 11
            nman = mantissa_bits(args.tol) if args.compress_mantissa else 52
            tests.append(("number of exponent bits", 11, f"{1.0:.2f}"))
            tests.append(("number of mantissa bits", 52, f"{1.0:.2f}"))
            tests.append(("number of compressed exponent bits", nexp, f"{nexp/11:.2f}"))
            tests.append(("number of compressed mantissa bits", nman, f"{nman/52:.2f}"))
            tests.append(("number of compressed bits per double", 1+nexp+nman, f"{(1+nexp+nman)/64:.2f}"))
            tests.append(("original file size (bytes)", os.path.getsize(infile), f"{1.0:.2f}"))
            fs = int(args.offset[0]+fld.shape[0]*(1+nexp+nman)/8)
            tests.append(("approx. compressed file size (bytes)", fs, f"{fs/os.path.getsize(infile):.2f}"))

            from tabulate import tabulate
            logger.warning(tabulate(map(fmt, tests),
                headers=['key', 'value', 'fraction'], floatfmt=[]))
