#!/usr/bin/env python3
"""Calculate g-2 from correlators produced by meson.c."""

import argparse
import numpy as np
import scipy as sc
import scipy.integrate  # sc.integrate.quad()
import json  # json.dumps()
import matplotlib.pyplot as plt
import mass as ms
import warnings
import re  # findall()
from tqdm import tqdm
import uncertainties as uc
from uncertainties import ufloat, ufloat_fromstr, unumpy, UFloat
from uncertainties.umath import exp, sin, log
import tabulate
import inspect
from functools import cache
from collections.abc import Iterable


def new_print(*args, **kwargs):
    r"""Define a new print function.

    This function as a replacement for the original print function tries first
    to write using `tqdm.write`, else uses legacy `print`.

    Other Parameters
    ----------------
    args : dict
        Positional arguments to be passed to print()
    kwargs : dict
        Keyword arguments to be passed to print()

    """
    try:
        tqdm.write(*args, **kwargs)
    except NameError:
        old_print(*args, ** kwargs)


# globaly replace print with new_print
old_print = print
inspect.builtins.print = new_print

#
# @const fine structure constant
#
ALPHA = 0.00729735257

#
# @const 1 = h_bar*c = 197.327*MeV*fm
# @see Particle Data Group, Physical Constants
# https://pdg.lbl.gov/2021/web/viewer.html?file=../reviews/rpp2021-rev-phys-constants.pdf (10.12.2021) # noqa: E501
#
HBAR_C = 197.3269804

#
# @const value of the muon mass in MeV
#
M_MU = 105.6583745


def float_expr(number):
    r"""Convert a fraction of two integers to a float.

    Parameters
    ----------
    number : str
        Number as a string "a/b"

    Returns
    -------
    tuple
        A tuple containing the fraction as float and string
    float : float
        The float
    string : str
        The string

    """
    if '/' in number:
        n, d = number.split('/')
        n, d = np.float64(n), np.float64(d)
        return n/d, str(number)
    else:
        return float(number), str(number)


def min_ufloat(arr):
    r"""Return the min of a ufloat array."""
    return np.min((unumpy.nominal_values(arr)-unumpy.std_devs(arr)))


def max_ufloat(arr):
    r"""Return the max of a ufloat array."""
    return np.max((unumpy.nominal_values(arr)+unumpy.std_devs(arr)))


def aggregate(i, retval, *parameters, prefactor):
    r"""Extract a_mu, meff, A, model_part, lattice_part, redchi in a list.

    @param ufloat prefactor: Z_v and a part to multiply
    """
    a_mu, meff, A, model_part, lattice_part = retval[0]
    r = parameters[0]
    redchi_m = retval[1]['redchi_m']
    redchi_A = retval[1]['redchi_A']
    return np.array(
        [
            i+1,
            r,
            prefactor*a_mu,
            meff,
            A,
            prefactor*model_part,
            prefactor*lattice_part,
            redchi_m,
            redchi_A,
            np.sqrt((redchi_m-1)**2 + (redchi_A-1)**2)
        ],
        dtype=object
    )


# Return a table and the errors of a_mu, meff, A, model_part, lattice_part as ufloats
# @param string vary_param: the name of the parameter that is varied
def collect(retval, vary_param, return_agg=False):
    retval = np.array(retval, dtype=object)
    sigma_a_mu = np.nanstd([m.n for m in retval[:, 2]])
    sigma_meff = np.nanstd([m.n for m in retval[:, 3]])
    sigma_A = np.nanstd([m.n for m in retval[:, 4]])
    sigma_model_part = np.nanstd([m.n for m in retval[:, 5]])
    sigma_lattice_part = np.nanstd([m.n for m in retval[:, 6]])

    min_idx = np.argmin(retval[:, -1])
    retval[min_idx, -1] = ms.green(retval[min_idx, -1])

    text = tabulate.tabulate(retval, headers=[
        "#", "vary_param", "a_mu",  "meff", "A", "model_part", "lattice_part",
        "chi_m", "chi_A", "norm"
    ])
    text += f'\nSystematic error coming from {vary_param}: {sigma_a_mu = }, ' \
            f'{sigma_meff = }, {sigma_A = }, {sigma_model_part = }, ' \
            f'{sigma_lattice_part = }'
    tag = f"sys: {vary_param}"
    if return_agg:
        return (
            ufloat(0.0, sigma_a_mu, tag),
            ufloat(0.0, sigma_meff, tag),
            ufloat(0.0, sigma_A, tag),
            ufloat(0.0, sigma_model_part, tag),
            ufloat(0.0, sigma_lattice_part, tag),
            text, retval[:, [1, 2]]
        )
    else:
        return (
            ufloat(0.0, sigma_a_mu, tag),
            ufloat(0.0, sigma_meff, tag),
            ufloat(0.0, sigma_A, tag),
            ufloat(0.0, sigma_model_part, tag),
            ufloat(0.0, sigma_lattice_part, tag),
            text
        )
    

def substitute_arctan(f, a, b):
    sf = lambda y: (np.tan(y)**2 + 1)*f(np.tan(y))
    return sf, np.arctan(a), np.arctan(b)


def fill_plot(args, plot_data, fig, axes, nplot=0):
    N0 = plot_data['N0']
    mass = plot_data['mass']
    G = plot_data['G']
    Gerr = plot_data['Gerr']
    A = plot_data['A']
    spline_interpolation_y = plot_data['spline_interpolation_y']
    kernel_data = plot_data['kernel']
    cutoffs_vs_amu = plot_data['cutoffs_vs_amu']

    formats = ['-']
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
    axes[0].set_title(r'Integrand for g-2')
    axes[1].set_title(r'$a_{\mu} vs. $x_{cut}$')
    axes[1].set_title(r'Relative error of the integrand')

    if args.type == 'locloc':
        tpower = 2
    elif args.type == 'psloc':
        tpower = 1
    elif args.type == 'psps':
        tpower = 0

    m_mu = M_MU/HBAR_C # in fm^-1
    prefactor_q = args.charge_squared[0]*(ALPHA/np.pi)**2*(1/args.a.n**2)
    prefactor_aZ = (args.z**tpower)

    if args.yunit == 'mu':
        prefactor_plot = args.charge_squared[0]/(args.a**3*m_mu)
    else:
        prefactor_plot = prefactor_q*prefactor_aZ

    end = int(1.5*N0) if args.fold == 0 else 2*N0-1
    color = colors[nplot % len(colors)]
    shift = ((-1)**nplot)*np.ceil(nplot/2)/10

    plot_fit = lambda x0: prefactor_plot*A*exp(-mass*x0)*kernel_data[x0]

    G = np.delete(G, args.exclude, axis = 0)

    # average all remaining configs
    G_mean = np.array([ufloat(n,s) for n,s in zip(*ms.avg_sources(G, G.shape[0], Gerr = None))])

    # plot the limiting lines
    for ax in axes:
        ax.axvline(args.xcut, 0, 1, label = rf'$x_{{cut}} = {args.xcut}$')
    axes[0].axhline(0, 0, 1, color="black")

    # full product dimless
    mn = np.minimum(len(G_mean), len(kernel_data))
    x = np.arange(0, mn)
    y = prefactor_plot*np.array([G_mean[x0]*kernel_data[x0] for x0 in x])
    y1 = 1.1*min_ufloat(y)
    y2 = 1.1*max_ufloat(y[0:args.xcut+2])
    axes[0].set_ylim([y1, y2])
    axes[0].set_xlim([0, end])
    axes[0].errorbar(x, unumpy.nominal_values(y),
        yerr = unumpy.std_devs(y),
        fmt = ':',
        color = color,
        label = f'lattice part {args.name}',
        capsize = 3
    )

    # rel. error of full product dimless
    axes[2].set_xlim([0, end])
    y = unumpy.std_devs(y)/unumpy.nominal_values(y)
    axes[2].plot(x, y, f',-{color}')

    # model part (dimless)
    x = np.arange(args.xcut-2, len(kernel_data))
    y = np.array([plot_fit(x0) for x0 in x])
    axes[0].errorbar(x, unumpy.nominal_values(y),
        yerr = unumpy.std_devs(y),
        fmt = '--',
        color = color,
        label = f'model part {args.name}',
        capsize = 3
    )

    # rel. error of model part (dimless)
    axes[2].plot(x, unumpy.std_devs(y)/unumpy.nominal_values(y), f',-{color}',
        label = f'rel. error of lattice / model part {args.name}',
    )

    # cubic spline interpolation
    x = np.linspace(0, mn, 1000)
    y = prefactor_plot*spline_interpolation_y
    axes[0].plot(x, unumpy.nominal_values(y), '-',
        color = color,
        label = 'cubic spline interpolation of lattice data'
    )

    if args.xunit == 'fm':
        import matplotlib.ticker as ticker
        ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x*args.a.n))
        #ax = fig.axes[0]
        for ax in axes:
            ax.xaxis.set_major_formatter(ticks_x)
            ax.set_xticks(np.arange(0, end*args.a.n, 0.5)/args.a.n)
        axes[0].set_xlabel(r'$x_0$ [fm]')
        axes[1].set_xlabel(r'$x_{cut}$ [fm]')
        axes[2].set_xlabel(r'$x_0$ [fm]')
    else:
        axes[0].set_xlabel(r'$x_0$ [lattice units]')
        axes[1].set_xlabel(r'$x_{cut}$ [lattice units]')
        axes[2].set_xlabel(r'$x_0$ [lattice units]')

    axes[1].set_ylabel(r'$a_{\mu}$')

    # a_mu vs cutoff
    if len(cutoffs_vs_amu) > 0:
        x = cutoffs_vs_amu[:,0]
        y = cutoffs_vs_amu[:,1]
        axes[1].errorbar(x+shift, unumpy.nominal_values(y),
            yerr = unumpy.std_devs(y),
            fmt = '-',
            color = color,
            label = r'{}-contribution to $a_{{\mu}}^{{HVP}}$'.format(args.name),
            capsize = 3
        )

    axes[2].set_yscale("linear")


def plot_multi(Args, Plot_data):
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
    axes = [ax1, ax2, ax3]
    fig.suptitle(f"TODO-contribution to the HVP of myon g-2, TODO-current\n")

    for i, (args, plot_data) in enumerate(zip(Args, Plot_data)):
        fill_plot(args, plot_data, fig, axes, nplot=i)

    for ax in axes:
        ax.legend()

    plt.draw()
    plt.show()


def plot(args, plot_data):
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
    axes = [ax1, ax2, ax3]
    fig.suptitle(f"{args.name}-contribution to the HVP of myon g-2, {args.type}-current\n")

    fill_plot(args, plot_data, fig, axes)

    for ax in axes:
        ax.legend()

    target, file, _ = ms.get_target(args.outfile, '.pdf')
    if file:
        plt.savefig(target)
    else:
        plt.draw()
        plt.show()


def main(args):
    if args.verbosity:
        print("Running with the following arguments:")
        print(json.dumps(vars(args), default=str, indent = 4))

    if args.type == 'locloc':
        tpower = 2
    elif args.type == 'psloc':
        tpower = 1
    elif args.type == 'psps':
        tpower = 0

    # @choice which unit system
    #args.a = args.a/HBAR_C             # in MeV^-1
    m_mu = M_MU/HBAR_C                  # in fm^-1
    #m_mu = args.a*M_MU/HBAR_C          # in lattice units
    #args.a = 1.0                       # in lattice units
    #args.a = args.a*M_MU/HBAR_C        # in units of mu
    #m_mu = 1.0                         # in units of mu
    prefactor_q = args.charge_squared[0]*(ALPHA/np.pi)**2*(1/args.a.n**2)
    prefactor_aZ = (args.z**tpower)

    def Z(s_hat):
        return -(s_hat - np.sqrt(s_hat**2 + 4*s_hat))/(2*s_hat)

    def f(s):
        s_hat = s/m_mu**2
        Zs_hat = Z(s_hat)
        return (1/m_mu**2)*s_hat*Zs_hat**3*(1 - s_hat*Zs_hat)/(1 + s_hat*Zs_hat**2)

    # for alpha @see https://hpcpworkspace.slack.com/archives/C030U2U035F/p1648536672695529
    def kernel_integrand(t, w):
        wsq = w**2;
        if isinstance(w, Iterable):
            sine = [sin(0.5*x*t) for x in np.atleast_1d(w)]
        else:
            sine = sin(0.5*w*t)
        alpha = -1.0
        return f(wsq)*(w*np.power(t,2) - 4*np.power(sine,2)/w + (2*(alpha+1))/w)

    # for alpha @see https://hpcpworkspace.slack.com/archives/C030U2U035F/p1648536672695529
    def kernel_integrand_numpy(t, w):
        wsq = w**2;
        sine = np.sin(0.5*w*t);
        alpha = -1.0
        return f(wsq)*(w*t**2 - 4*sine**2/w + (2*(alpha+1))/w);

    def kernel_orig_ufloat(t):
        I = lambda t, y: (np.tan(y)**2 + 1)*kernel_integrand(t, np.tan(y))
        return quadrature_ufloat(t, I, 0.0, 0.5*np.pi,
            maxiter = 1000
        )
        # The old way:
        # return quad_ufloat(t, kernel_integrand, 0.0, np.inf,
        #     epsabs = args.epsabs,
        #     epsrel = args.epsrel,
        #     limit = args.limit
        # )

    # transform the kernel integral using y = arctan(x), such that the
    # interval is finite.
    @cache
    def kernel_orig_regular(t):
        #I = lambda y: (np.tan(y)**2 + 1)*kernel_integrand_numpy(t, np.tan(y))
        I, a, b = substitute_arctan(lambda y: kernel_integrand_numpy(t,y), 0.0, np.inf)
        result, abserr = sc.integrate.quadrature(I, a, b,
            maxiter = 1000
        )
        # The old way:
        # I = lambda w: kernel_integrand(t, w)
        # result, abserr = sc.integrate.quad(I, 0.0, np.inf,
        #     epsabs = args.epsabs,
        #     epsrel = args.epsrel,
        #     limit = 100
        # )
        if 0.01*np.abs(result) < abserr:
            relerr = 100*abserr/np.abs(result)
            warnings.warn(ms.yellow(f"kernel_orig_regular(): Kernel integrand has a large error ({relerr:.1f}% >= 1%)."))
        return result

    #
    # The kernel function for the convolution integral of the final calculation
    # of muon g-2.
    #
    # @param double t the time slice
    # @return double kernel function evaluted at the time slice t for muon mass m_mu
    # @see document
    #
    def kernel_orig(t):
        #vkernel_orig_regular = np.vectorize(kernel_orig_regular)
        if isinstance(t, UFloat):
            return 8*np.pi**2*kernel_orig_ufloat(t)
        if isinstance(t, Iterable):
            return [8*np.pi**2*kernel_orig_regular(tx) for tx in t]
        else:
            return 8*np.pi**2*kernel_orig_regular(t)

        #return 8*np.pi**2*(kernel_orig_ufloat(t) if isinstance(t, UFloat) else vkernel_orig_regular(t))

    #
    # Calls scipy.integrate.quad, with the given args and kwargs
    #
    # @param ufloat x: argument containing an error
    # @param callable integrand: integrand that takes x as first positional argument
    # @param mixed args: positonal arguments for scipy.integrate.quad
    # @param mixed kwargs: keyword arguments for scipy.integrate.quad
    # @return double: the result including errors
    #
    # @notice: unfortunately the error from the integration [[abserr]] gets lost
    #
    @uc.wrap
    def quad_ufloat(x, integrand, *args, **kwargs):
        I = lambda *largs, **lkwargs: integrand(x, *largs, **lkwargs)
        result, abserr = sc.integrate.quad(I, *args, **kwargs)
        if 0.01*np.abs(result) < abserr:
            relerr = 100*abserr/np.abs(result)
            warnings.warn(ms.yellow(f"quad_ufloat(): Kernel integrand has a large error ({relerr:.1f}% >= 1%)."))
        return result

    #
    # Calls scipy.integrate.quadrature, with the given args and kwargs
    #
    @uc.wrap
    def quadrature_ufloat(x, integrand, *args, **kwargs):
        I = lambda *largs, **lkwargs: integrand(x, *largs, **lkwargs)
        result, abserr = sc.integrate.quadrature(I, *args, **kwargs)
        #print(f"{result = }, {abserr = }")
        if 0.01*np.abs(result) < abserr:
            relerr = 100*abserr/np.abs(result)
            warnings.warn(ms.yellow(f"quadrature_ufloat(): Kernel integrand has a large error ({relerr:.1f}% >= 1%)."))
        return result

    # @choice which kernel
    kernel = lambda t: kernel_orig(t)

    # @check
    # sanity check; integral of f(s) from 0 to inf multipled by alpha/pi should be
    # alpha/2pi; the 1-loop em-contribution to a_mu
    # result, _ = sc.integrate.quad(f, 0.0, np.inf)
    # print((ALPHA/np.pi)*result)
    # print(ALPHA/(2*np.pi))

    # reading all the infiles
    G, (N0, N1, N2, N3), Gname, nconfig = ms.read_infiles(args.infile, verbose = args.verbosity)

    # average all nsrc sources of a given config
    if args.verbosity: print("G.shape = {0} (initial)".format(G.shape))

    # average all nsrc sources of a given config
    G, Gerr = ms.avg_sources(G, args.nsrc)
    Gname = Gname.reshape(-1,args.nsrc)[:,0]
    Gname = np.array([int(re.findall(r'n(\d+)_', name)[0]) for name in Gname])
    if args.verbosity: print("G.shape = {0} (after averaging over sources)".format(G.shape))

    # fold according to folding variant
    G, Gerr, N0, _, _, _, _, fold_axis = ms.fold(G, args.fold, N0, Gerr = Gerr)
    if args.verbosity: print("G.shape = {0} (after folding)".format(G.shape))

    exclude = []
    for i in args.exclude:
        exclude.append(np.where(Gname == i))
    args.exclude = list(np.array(exclude).flatten())

    #
    # This observable averages the data first, then fits the logarithm of the
    # data to a cubic spline and integrates it up to a given cutoff. The
    # model part is determined by a two-parameter fit in a given fit-range.
    # This model is integrated to the cutoff to infinity. The function retuns
    # the results before multiplying with Z_v and the lattice constant a.
    #
    def observable_average_first_ufloat(data, fit_range, xcut, a,
        mode = 'full',
        cov = None,
        verbosity = 0,
        reference = ufloat(0.0, 0.0),
        estimate_errors = False
    ):
        ob = f'Observable (g-2): time-momentum integral, lmfit, mass/A determination with two-parameter exp-fit, {mode} covariance matrix, d.o.f. = {len(fit_range)-1}'
        if verbosity >= 2: print(ms.blue(ob))

        ################################################
        #                  model-part                  #
        ################################################

        # Call the meff/A observable from mass.py to perform the 2-parameter
        # fit for the model-part
        (meff, A), info_dict_mass = ms.observable_chi2_2par_DD(data[:,fit_range], fit_range,
            mode = mode,
            cov = cov,
            verbosity = verbosity,
            reference = reference,
            fit = 'exp',
        )

        # model part (no variation with quad)
        # fit = lambda x0: np.exp(-meff.n*x0)*kernel(a.n*x0)
        # I, Ierr = sc.integrate.quad(fit, xcut, np.inf,
        #     epsabs = args.epsabs,
        #     epsrel = args.epsrel,
        #     limit = args.limit
        # )
        # model part (no variation with quadrature and substitution)
        AA = [0.0014440, 0.0408880]
        m_0 = [0.2664, 0.877157]
        sfit, bound_a, bound_b = substitute_arctan(lambda x0: (AA[0]*np.exp(-m_0[0]*x0))*kernel(a.n*x0), xcut, np.inf)

        #sfit, bound_a, bound_b = substitute_arctan(lambda x0: np.exp(-meff.n*x0)*kernel(a.n*x0), xcut, np.inf)
        I, Ierr = sc.integrate.quadrature(sfit, bound_a, bound_b,
            tol = args.epsabs,
            rtol = args.epsrel,
            maxiter = args.limit
        )
        if 0.01*np.abs(I) < Ierr:
            relerr = 100*Ierr/np.abs(I)
            warnings.warn(ms.yellow(f"model-part: Model integrand has a large error ({relerr:.1f}% >= 1%)."))

        I = ufloat(I, Ierr, "sys: model-part: integration: error from scipy.integrate.quad()")

        if (estimate_errors):
            # model part (variation of a)
            fit = lambda a, x0: np.exp(-meff.n*x0)*kernel(a*x0)
            Ia = quad_ufloat(a, fit, xcut, np.inf,
                epsabs = args.epsabs,
                epsrel = args.epsrel,
                limit = args.limit
            )

            # model part (variation of meff)
            fit = lambda meff, x0: np.exp(-meff*x0)*kernel(a.n*x0)
            Imeff = quad_ufloat(meff, fit, xcut, np.inf,
                epsabs = args.epsabs,
                epsrel = args.epsrel,
                limit = args.limit
            )

            I += ufloat(0.0, Imeff.s, "sys: model-part: integration: error in meff")
            I += ufloat(0.0, Ia.s, "sys: model-part: integration: error in lattice constant a")

        # This is the correct model part of the integration with  with
        # propagated errors from a, meff and A, also including the
        # integration error. The error in a and meff is propagated by using
        # the wrapped quad_ufloat method. The integral error is determined
        # with a regular scipy.integrate.quad call. All these 3 error are
        # added in quadrature
        #model_part = A*I
        model_part = I

        ################################################
        #                 lattice-part                 #
        ################################################

        mean, err = np.nanmean(data, axis = 0), np.nanstd(data, axis = 0)
        data_mean = np.array([ufloat(n, s, f"stat: lattice-part: lattice data averaging of point x0 = {i}") for i,(n,s) in enumerate(zip(mean, err))])

        # @todo: cubic spline fit with errors? Error comes from the data and a
        valid, = np.where(data_mean[:xcut] > 0)
        Y = np.array([log(x) for x in data_mean[valid]])
        tck, fp, ier, msg = sc.interpolate.splrep(valid, unumpy.nominal_values(Y),
            w=1/unumpy.std_devs(Y),
            xb=0,
            xe=xcut,
            k=3,
            full_output=1
        )
        spline_interpolation = lambda x: np.exp(sc.interpolate.splev(x, tck))*kernel(a.n*x)

        # fallback to summing the lattice points if spline interpolation fails
        if not ier <= 0:
            lattice_part = np.sum([data_mean[x0]*kernel(a*x0) for x0 in range(0, xcut+1)])
            warnings.warn(ms.yellow(f"scipy.interpolate.splrep() failed with {ier = }, {msg = }"))
        else:
            I, Ierr = sc.integrate.quad(spline_interpolation, 0, xcut,
                epsabs = args.epsabs,
                epsrel = args.epsrel,
                limit = args.limit
            )
            lattice_part = ufloat(I, Ierr, "stat: lattice_part: spline integration error (probably underestimated)")
            #if estimate_errors:
                #lattice_part_sum = np.sum([data_mean[x0]*kernel(a*x0) for x0 in range(0, xcut+1)])
                #lattice_part = ufloat(lattice_part.n, lattice_part_sum.s, "stat: lattice_part: error prop from summing")

        ################################################
        #                  Finalizing                  #
        ################################################

        # No multiplication with the current renormalization constant Z_v and
        # lattice constant a yet, this is done outside the observable,
        # because else we loose the error coming from them when using
        # bootstrap/jackknife.
        lattice_part *= prefactor_q*10**10
        model_part *= prefactor_q*10**10
        a_mu = lattice_part + model_part

        info_dict = {
            'ob': ob,
            'redchi_A': info_dict_mass['redchi_A'],
            'redchi_m': info_dict_mass['redchi_m'],
            'spline_interpolation': spline_interpolation,
            'lmfit_result': info_dict_mass['lmfit_result'],
        }
        return (a_mu, meff, A, model_part, lattice_part), info_dict

    # variation w.r.t. fit-range
    # setup all data and ranges for the range-variation
    # and obtain the systematic error due to the choice of range
    if args.vary_range is not None:
        fit_ranges = []
        for i in range(args.vary_range[0], args.vary_range[1]+1):
            lower = i+args.vary_range[2]-1
            upper = np.minimum(int(fold_axis), args.vary_range[1]+1)
            for j in range(lower, upper):
                r = ms.obtain_range([i,j], [])
                fit_ranges.append(r)

        observable_to_vary = lambda r: observable_average_first_ufloat(G.real,
            fit_range = r,
            xcut = args.xcut,
            a = args.a,
            mode = args.mode,
            verbosity = args.verbosity,
            reference = args.mass_ref,
        )

        a_mu_range_err, mass_range_err, A_range_err, model_part_range_err, lattice_part_range_err, table_range = ms.vary_parameter(fit_ranges,
            f=observable_to_vary,
            vary_param="fit-range",
            aggregate=lambda *pargs, **kwargs: aggregate(*pargs, **kwargs, prefactor = prefactor_aZ),
            collect=lambda r: collect(r, vary_param = "choice of the fit-range")
        )
    else:
        a_mu_range_err         = ufloat(0.0, 0.0, "sys: choice of fit-range")
        mass_range_err         = ufloat(0.0, 0.0, "sys: choice of fit-range")
        A_range_err            = ufloat(0.0, 0.0, "sys: choice of fit-range")
        model_part_range_err   = ufloat(0.0, 0.0, "sys: choice of fit-range")
        lattice_part_range_err = ufloat(0.0, 0.0, "sys: choice of fit-range")
        table_range = ''

    # vary w.r.t cutoff args.xcut
    cutoffs_vs_amu = []
    if args.plot_xcut is not None:
        r = ms.obtain_range(args.range, [])
        cutoffs = list(range(args.plot_xcut[0], args.plot_xcut[1]+1))
        C = ms.covariance_matrix(G.real[:,r], mode = args.mode, sqrt = True, verbosity = args.verbosity)
        observable_to_vary = lambda xcut: observable_average_first_ufloat(G.real,
            fit_range = r,
            xcut = xcut,
            a = args.a,
            mode = args.mode,
            cov = C,
            verbosity = args.verbosity,
            reference = args.mass_ref
        )

        _, _, _, _, _, _, cutoffs_vs_amu = ms.vary_parameter(cutoffs,
            f=observable_to_vary,
            vary_param="cutoff",
            aggregate=lambda *pargs, **kwargs: aggregate(*pargs, **kwargs, prefactor=prefactor_aZ),
            collect=lambda r: collect(r, vary_param="choice of the cutoff", return_agg=True)
        )

    if args.vary_xcut is not None:
        r = ms.obtain_range(args.range, [])
        cutoffs = list(range(args.vary_xcut[0], args.vary_xcut[1]+1))
        C = ms.covariance_matrix(G.real[:,r], mode = args.mode, sqrt = True, verbosity = args.verbosity)
        observable_to_vary = lambda xcut: observable_average_first_ufloat(G.real,
            fit_range = r,
            xcut = xcut,
            a = args.a,
            mode = args.mode,
            cov = C,
            verbosity = args.verbosity,
            reference = args.mass_ref
        )

        # No error due to xcut for the model- and lattice-part, because
        # obviously the two values change significantly with the cutoff, but
        # their sum (a_mu) should not.
        a_mu_xcut_err, mass_xcut_err, A_xcut_err, _, _, table_xcut = ms.vary_parameter(cutoffs,
            f=observable_to_vary,
            vary_param="cutoff",
            aggregate=lambda *pargs, **kwargs: aggregate(*pargs, **kwargs, prefactor=prefactor_aZ),
            collect=lambda r: collect(r, vary_param="choice of the cutoff")
        )
    else:
        a_mu_xcut_err         = ufloat(0.0, 0.0, "sys: choice of the cutoff xcut")
        mass_xcut_err         = ufloat(0.0, 0.0, "sys: choice of the cutoff xcut")
        A_xcut_err            = ufloat(0.0, 0.0, "sys: choice of the cutoff xcut")
        table_xcut = ''

    # vary w.r.t. lattice constant args.a
    if args.vary_a is not None and args.a.s != 0.0:
        r = ms.obtain_range(args.range, [])
        spacings = [ufloat(x, 0.0) for x in np.random.normal(args.a.n, args.a.s, args.vary_a)]
        C = ms.covariance_matrix(G.real[:,r], mode = args.mode, sqrt = True, verbosity = args.verbosity)
        observable_to_vary = lambda a: observable_average_first_ufloat(G.real,
            fit_range = r,
            xcut = args.xcut,
            a = a,
            mode = args.mode,
            cov = C,
            verbosity = args.verbosity,
            reference = args.mass_ref
        )

        import time
        s = time.perf_counter()
        a_mu_a_err, mass_a_err, A_a_err, model_part_a_err, lattice_part_a_err, table_a = ms.vary_parameter(spacings,
            f=observable_to_vary,
            vary_param="lattice constant a",
            aggregate=lambda *pargs, **kwargs: aggregate(*pargs, **kwargs, prefactor = prefactor_aZ),
            collect=lambda r: collect(r, vary_param = "stat: variation of a")
        )
        elapsed = time.perf_counter() - s
        print(f"{__file__} executed in {elapsed:0.2f} seconds.")
    else:
        a_mu_a_err         = ufloat(0.0, 0.0, "stat: variation of a")
        mass_a_err         = ufloat(0.0, 0.0, "stat: variation of a")
        A_a_err            = ufloat(0.0, 0.0, "stat: variation of a")
        model_part_a_err   = ufloat(0.0, 0.0, "stat: variation of a")
        lattice_part_a_err = ufloat(0.0, 0.0, "stat: variation of a")
        table_a = ''


    # setup for the acutal observable
    data = G.real
    r = ms.obtain_range(args.range, [])
    C = ms.covariance_matrix(data[:,r], mode = args.mode, sqrt = True, verbosity = args.verbosity)
    observable = lambda data, estimate_errors=False: observable_average_first_ufloat(data,
        fit_range = r,
        xcut = args.xcut,
        a = args.a,
        mode = args.mode,
        cov = C,
        verbosity = args.verbosity,
        reference = args.mass_ref,
        estimate_errors = estimate_errors
    )

    result_dict = {
        #"a": ms.error_contributions_dict(args.a),
        #"Z_v": ms.error_contributions_dict(args.z),
        "nconfig": int(nconfig/args.nsrc)
    }

    (a_mu, mass, A, model_part, lattice_part), info = observable(data, estimate_errors=True)
    a_mu *= prefactor_aZ
    model_part *= prefactor_aZ
    lattice_part *= prefactor_aZ
    a_mu         += a_mu_range_err + a_mu_xcut_err + a_mu_a_err
    mass         += mass_range_err + mass_xcut_err + mass_a_err
    A            += A_range_err + A_xcut_err + A_a_err
    model_part   += model_part_range_err + model_part_a_err
    lattice_part += lattice_part_range_err + lattice_part_a_err
    spline_interpolation = info['spline_interpolation']
    result_dict['meff (error prop)'] = ms.error_contributions_dict(mass, {'chisq': info['redchi_m']})
    result_dict['A (error prop)'] = ms.error_contributions_dict(A, {'chisq': info['redchi_A']})
    result_dict['model-part (error prop)'] = ms.error_contributions_dict(model_part)
    result_dict['lattice-part (error prop)'] = ms.error_contributions_dict(lattice_part)
    result_dict['a_mu (error prop)'] = ms.error_contributions_dict(a_mu)

    if args.jackknife:
        (a_mu_jn, mass_jn, A_jn, model_part_jn, lattice_part_jn), info_jn = ms.jackknife(data, observable)
        a_mu_jn *= prefactor_aZ
        model_part_jn *= prefactor_aZ
        lattice_part_jn *= prefactor_aZ
        a_mu_jn         += a_mu_range_err + a_mu_xcut_err + a_mu_a_err
        mass_jn         += mass_range_err + mass_xcut_err + mass_a_err
        A_jn            += A_range_err + A_xcut_err + A_a_err
        model_part_jn   += model_part_range_err + model_part_a_err
        lattice_part_jn += lattice_part_range_err + lattice_part_a_err
        chisq_jn_m = np.mean([ik['redchi_m'] for ik in info_jn])
        chisq_jn_A = np.mean([ik['redchi_A'] for ik in info_jn])
        result_dict['meff (jackknife)'] = ms.error_contributions_dict(mass_jn, {'chisq': chisq_jn_m})
        result_dict['A (jackknife)'] = ms.error_contributions_dict(A_jn, {'chisq': chisq_jn_A})
        result_dict['model-part (jackknife)'] = ms.error_contributions_dict(model_part_jn)
        result_dict['lattice-part (jackknife)'] = ms.error_contributions_dict(lattice_part_jn)
        result_dict['a_mu (jackknife)'] = ms.error_contributions_dict(a_mu_jn)

    if args.bootstrap != 0:
        (a_mu_bs, mass_bs, A_bs, model_part_bs, lattice_part_bs), info_bs = ms.bootstrap(data, observable, args.bootstrap)
        a_mu_bs *= prefactor_aZ
        model_part_bs *= prefactor_aZ
        lattice_part_bs *= prefactor_aZ
        a_mu_bs         += a_mu_range_err + a_mu_xcut_err + a_mu_a_err
        mass_bs         += mass_range_err + mass_xcut_err + mass_a_err
        A_bs            += A_range_err + A_xcut_err + A_a_err
        model_part_bs   += model_part_range_err + model_part_a_err
        lattice_part_bs += lattice_part_range_err + lattice_part_a_err
        chisq_bs_m = np.mean([ik['redchi_m'] for ik in info_bs])
        chisq_bs_A = np.mean([ik['redchi_A'] for ik in info_bs])
        result_dict['meff (bootstrap)'] = ms.error_contributions_dict(mass_bs, {'chisq': chisq_bs_m})
        result_dict['A (bootstrap)'] = ms.error_contributions_dict(A_bs, {'chisq': chisq_bs_A})
        result_dict['model-part (bootstrap)'] = ms.error_contributions_dict(model_part_bs)
        result_dict['lattice-part (bootstrap)'] = ms.error_contributions_dict(lattice_part_bs)
        result_dict['a_mu (bootstrap)'] = ms.error_contributions_dict(a_mu_bs)

    # print information
    prologue = ''
    prologue += f"charge q^2                           = {args.charge_squared[1]}\n"
    prologue += f"renormalization constant Z_v         = {args.z:.10f}\n"
    prologue += f"lattice constant a                   = {args.a:.10f} (variation: {args.vary_a})\n"
    prologue += f"cutoff x_cut                         = {args.xcut} (variation: {args.vary_xcut})\n"
    prologue += f"mass fit-range                       = {args.range} (variation: {args.vary_range})\n"
    prologue += f"current type                         = {args.type}\n"
    prologue += f"number of configs                    = {int(nconfig/args.nsrc)}\n"
    prologue += f"number of sources                    = {args.nsrc}\n"
    prologue += f"mass covariance-matrix               = {args.mode}\n"
    text = ''
    text += f"mass m (error prop)                  = {mass:.10f} ({ms.format_chisq(info['redchi_m'])})\n"
    if args.jackknife:
        text += f"mass m (jackknife)                   = {mass_jn:.10f} ({ms.format_chisq(chisq_jn_m)})\n"
    if args.bootstrap != 0: 
        text += f"mass m (bootstrap, K={args.bootstrap:>4})           = {mass_bs:.10f} ({ms.format_chisq(chisq_bs_m)})\n"

    text += f"amplitude A (error prop)             = {A:.10f} ({ms.format_chisq(info['redchi_A'])})\n"
    if args.jackknife:
        text += f"amplitude A (jackknife)              = {A_jn:.10f} ({ms.format_chisq(chisq_jn_A)})\n"
    if args.bootstrap != 0: 
        text += f"amplitude A (bootstrap, K={args.bootstrap:>4})      = {A_bs:.10f} ({ms.format_chisq(chisq_bs_A)})\n"

    text += f"a_mu^HVP_lattice (error prop)        = ({lattice_part:.10f}) x 10^-10\n"
    if args.jackknife:
        text += f"a_mu^HVP_lattice (jackknife)         = ({lattice_part_jn:.10f}) x 10^-10\n"
    if args.bootstrap != 0: 
        text += f"a_mu^HVP_lattice (bootstrap, K={args.bootstrap:>4}) = ({lattice_part_bs:.10f}) x 10^-10\n"

    text += f"a_mu^HVP_model (error prop)          = ({model_part:.10f}) x 10^-10\n"
    if args.jackknife:
        text += f"a_mu^HVP_model (jackknife)           = ({model_part_jn:.10f}) x 10^-10\n"
    if args.bootstrap != 0: 
        text += f"a_mu^HVP_model (bootstrap, K={args.bootstrap:>4})   = ({model_part_bs:.10f}) x 10^-10\n"

    text += f"{ms.bold('a_mu^HVP')} (error prop)                = ({a_mu:.10f}) x 10^-10\n"
    if args.jackknife:
        text += f"{ms.bold('a_mu^HVP')} (jackknife)                 = ({a_mu_jn:.10f}) x 10^-10\n"
    if args.bootstrap != 0: 
        text += f"{ms.bold('a_mu^HVP')} (bootstrap, K={args.bootstrap:>4})         = ({a_mu_bs:.10f}) x 10^-10\n"

    if args.ref != 0.0:
        text += f"a_mu^HVP (reference)                 = ({args.ref:.10f}) x 10^-10\n"
        text += "discrepancy (compared to error prop) = {0} sigma ({1})\n".format(
            ms.format_sigma(np.abs(a_mu[0]-args.ref)/np.sqrt(args.ref.s**2 + a_mu[1]**2), '{0:.1f}'),
            (ms.red('no overlap') if np.abs(a_mu[0]-args.ref) > args.ref.s+a_mu[1] else ms.green('overlap')) + ' with ref.')
        if args.jackknife:
            text += "discrepancy (compared to jackknife)  = {0} sigma ({1})\n".format(
                ms.format_sigma(np.abs(a_mu_jn.n-args.ref)/np.sqrt(args.ref.s**2 + a_mu_jn.s**2), '{0:.1f}'),
                (ms.red('no overlap') if np.abs(a_mu_jn.n-args.ref) > args.ref.s+a_mu_jn.s else ms.green('overlap')) + ' with ref.')
        if args.bootstrap != 0:
            text += "discrepancy (compared to bootstrap)  = {0} sigma ({1})\n".format(
                ms.format_sigma(np.abs(a_mu_bs.n-args.ref)/np.sqrt(args.ref.s**2 + a_mu_bs.s**2), '{0:.1f}'),
                (ms.red('no overlap') if np.abs(a_mu_bs.n-args.ref) > args.ref.s+a_mu_bs.s else ms.green('overlap')) + ' with ref.')

    title = f"{ms.bold(args.name.capitalize())}-contribution to the HVP of myon g-2\n"
    
    _, _, fprint = ms.get_target(args.outfile, '.txt')
    if args.vary_range is not None:
        fprint("Variation table for fit-range")
        fprint(table_range, end='\n\n')

    if args.vary_xcut is not None:
        fprint("Variation table for cutoff")
        fprint(table_xcut, end='\n\n')

    if args.vary_a is not None and args.a.s != 0.0:
        fprint("Variation table for the lattice constant a")
        fprint(table_a, end='\n\n')

    fprint(ms.box([prologue, text], title = title), end='\n\n')
    fprint(ms.error_contributions(a_mu, 'a_mu (error prop)'))
    if args.jackknife:
        fprint(ms.error_contributions(a_mu_jn, 'a_mu (jackknife)'))
    if args.bootstrap != 0:
        fprint(ms.error_contributions(a_mu_bs, f'a_mu (bootstrap, K={args.bootstrap})'))

    G = np.delete(G, args.exclude, axis = 0)
    G_mean = np.array([ufloat(n,s) for n,s in zip(*ms.avg_sources(G, G.shape[0], Gerr = Gerr))])
    kernel_data = np.array([kernel(args.a*x0) for x0 in np.arange(0, 2*int(N0))])
    mn = np.minimum(len(G_mean), len(kernel_data))
    plot_data = {
        'N0': N0,
        'mass': mass,
        'G': G,
        'Gerr': Gerr,
        'A': A,
        'spline_interpolation_y': np.array([spline_interpolation(x0) for x0 in np.linspace(0, mn, 1000)]),
        'kernel': kernel_data,
        'cutoffs_vs_amu': cutoffs_vs_amu
    }

    return result_dict, plot_data


def check(json):
    vary_xcut = json['args']['vary_xcut']
    if vary_xcut[1] - vary_xcut[0] > 7:
        return False, "xcut variation too high"
    return True, None


if __name__ == '__main__':
    ufloat_format_info = "the number can have different formats, for all possibilities, see https://uncertainties-python-package.readthedocs.io/en/latest/user_guide.html#creating-numbers-with-uncertainties"

    parser = ms.JsonArgumentParser(
        formatter_class = argparse.ArgumentDefaultsHelpFormatter,
        description = 'Calculate g-2 from correlators produced by meson.c.',
        plot_function = plot,
        plot_multiple_function = plot_multi,
        check_function = check,
        exceptions = ['infile', 'outfile', 'a', 'z'],
        default_actions = {
            'args.charge_squared': lambda x: x[1],
        },
        main_result = [
            'result.a_mu (jackknife).value&.3uS?a_mu (jackknife)'
        ],
    )
    parser.add_argument("-v", "--verbosity", action="count", help="increase output verbosity", default=0)
    parser.add_argument('-a', help = f'The value of the lattice constant in fm and its error ({ufloat_format_info}).', type=lambda x: ufloat_fromstr(x, "lattice constant a"), default = "0.1+/-0.0")
    parser.add_argument('--vary-a', help = f'How to vary the whole observable w.r.t. the lattice constant a. Accepts one integer that specifies how many random samples of a should be drawn from a Gaussian distribution with center a and width sigma_a. Set to 0 to disable variation of a.', type=int, default=None)
    parser.add_argument('--xcut', help='x0 coordinate where to cut', type=int, default=31)
    parser.add_argument('--vary-xcut', help='How much should xcut be varied in order to obtain the systematic error that appears from its choice. Expects 2 numbers: A <= xcut <= B, the lower and upper end of the variation (including).', type=int, nargs=2, metavar=('A', 'B'), default=None)
    parser.add_argument('--plot-xcut', help='How much should xcut be varied for the plot', type=int, nargs=2, metavar=('A', 'B'), default=None)
    parser.add_argument('-n', '--name', help='name of the contribution', default='full')

    parser.add_argument('--epsabs', help = 'Absolute error tolerance (see scipy.integrate.quad() and scipy.integrate.quadrature()).', type = float, default = 1.49e-8)
    parser.add_argument('--epsrel', help = 'Relative error tolerance (see scipy.integrate.quad() and scipy.integrate.quadrature()).', type = float, default = 1.49e-8)
    parser.add_argument('--limit', help = 'An upper bound on the number of subintervals used in the adaptive algorithm. (see scipy.integrate.quad() and scipy.integrate.quadrature()).', type = int, default = 1000)
    parser.add_argument('--plot', help="show the plot", default=False, action="store_true")
    parser.add_argument('--nsrc', help='number of sources used', type=int, default=1)
    parser.add_argument('--fold', help = 'See help text of mass.py', type = int, default = 0, choices = [0,1,2,3,4])
    parser.add_argument('-e', '--exclude', help='list of configs to exclude.', type=int, nargs='+', default=[])
    parser.add_argument('-m', '--mode', help='mode for the covariance matrix to fit for the amplitude A. Can be either full:n, diag:n,  or normalized:n, where n denotes the number of lowest eigenvalues to truncate. n can also be -1, which cut all negative eigenvalues automatically. (default: diag:0)', default='diag:0')
    parser.add_argument('-j', '--jackknife', help='calculate the result using the jackknife method', default=False, action="store_true")
    parser.add_argument('-b', '--bootstrap', help='calculate the result using the bootstrap method. The integer value gives the number of bootstrap samples to take.', type=int, default=0)
    parser.add_argument('--ref', help=f'a reference value and its error for g-2 ({ufloat_format_info}).',
        type=lambda x: ufloat_fromstr(x, "reference value g-2"),
        default="0.0+/-0.0"
    )
    parser.add_argument('--mass-ref', help=f'a reference value and its error for the mass ({ufloat_format_info}).',
        type=lambda x: ufloat_fromstr(x, "reference value mass"),
        default="0.0+/-0.0"
    )
    parser.add_argument('-t', '--type', help=r'Current type, can be either locloc, psloc, psps',
        choices=['locloc', 'psloc', 'psps'],
        default='locloc'
    )
    parser.add_argument('--xunit', help=r'Units of the x-axis', choices = ['lattice', 'fm'], default = 'lattice')
    parser.add_argument('--yunit', help=r'Units of the y-axis', choices = ['lattice', 'mu'], default = 'phys')
    parser.add_argument('-q', '--charge-squared', help='charge squared or factor in front of the current (already squared)',
        type=float_expr,
        default=(1.0, 1)
    )
    parser.add_argument('-z', help=f'renormalization factor in front of the current ({ufloat_format_info}).',
        type=lambda x: ufloat_fromstr(x, "stat: renormalization constant Z_v"),
        default="1.0+/-0.0"
    )

    required = parser.add_argument_group('required named arguments')
    required.add_argument('-r', '--range', help='range for the fit [A, B].',
        type=int,
        nargs=2,
        metavar=('A', 'B'),
        required=True
    )
    required.add_argument('--vary-range', help='How much should the range be varied in order to obtain the systematic error that appears from its choice. Expects 3 numbers: A B C, A and B are the lower and upper end of the variation (including), and C denotes the minimal number of points that should be contained in a range.', type=int, nargs=3, metavar=('A', 'B', 'C'), default = None)
    required.add_argument('-i', '--in',
        help = 'input file(s) containing the extracted data (from meson.c).',
        dest = 'infile',
        metavar = 'FILE',
        required = True,
        nargs = '+'
    )

    required.add_argument('-o', '--out',
        help = 'output file(s) containing the text and plot data.',
        dest = 'outfile',
        metavar = 'FILE',
        default = ('-', '-'),
        nargs = 2
    )

    args = parser.parse_args()
    result_dict, plot_data = main(args)
    meta = parser.set_result(result_dict, plot_data)
    _, _, fprint = ms.get_target(args.outfile, '.txt', truncate=False)
    fprint(f"This took {meta['duration']:.2f} seconds.")

    if args.plot:
        plot(args, plot_data)
