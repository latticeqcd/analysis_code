#!/usr/bin/env python3
#
# Compare the timings of jobs
#

import argparse
import json
import re
import numpy as np
import mass as ms
import os
from collections.abc import Iterable
from uncertainties import ufloat
import matplotlib.pyplot as plt
import sys, os
import fnmatch

ALPHA = 0.00729735257
NINV = 120

def extract_from_file(file, regex, group, dtype, limit = np.inf):
    items = []
    with open(file, 'rt') as f:
        i = 0
        for line in f.readlines():
            if (m := re.search(regex, line)):
                if isinstance(group, Iterable) and isinstance(dtype, Iterable):
                    items.append([t(m.group(g)) for g,t in zip(group, dtype)])
                else:
                    items.append(dtype(m.group(group)))
            if i >= limit:
                break

    if len(items) == 0:
        return False
    return items if limit > 1 else items[0]

def main(args):
    data = {}

    inf = []
    for infile in args.infile:
        if os.path.isdir(infile):
            infile = f"{infile}/*.log"
        for name in ms.braced_glob(infile, recursive=True):
            inf.append(name)

    for infile in inf:
        time = extract_from_file(infile, f'Configuration no ([0-9]+) fully processed in ([0-9,\.\+e]+) sec', 2, float, limit = 1)
        status = extract_from_file(infile, f'^status = ([0-9]+),([0-9]+)$', [1,2], [int, int], limit = 1)
        #ensemble = extract_from_file(infile, f'^name\s+([^\s]+)$', 1, str, limit = 1)
        ensemble_name = os.path.basename(extract_from_file(infile, f'^cnfg_dir\s+([^\s]+)$', 1, str, limit = 1))
        name = ensemble_name
        log_dir = extract_from_file(infile, f'^log_dir\s+([^\s]+)$', 1, str, limit = 1)
        if (m := re.search('\/('+ensemble_name+'[^\/]*)\/', log_dir)):
           ensemble = m.group(1) 
           name = ensemble
        alpha = extract_from_file(infile, f'^name\s+([^a]+)a([0-9,\.]+)', 2, float, limit = 1)
        if (time is False or status is False or ensemble is False or name is False or alpha is False):
            print(ms.red(f'Failed to read all information from logfile {infile}'))
            print(f"Extracted from {infile}:")
            print(f"  {time = }")
            print(f"  {status = }")
            print(f"  {ensemble = }")
            print(f"  {name = }")
            print(f"  {alpha = }")
            if args.pedantic:
                exit()
            continue

        print(f"Extracted from {infile}:")
        print(f"  {time = }")
        print(f"  {status = }")
        print(f"  {ensemble = }")
        print(f"  {name = }")
        print(f"  {alpha = }")

        if ensemble not in data:
            data[ensemble] = {}
            data[ensemble]['time'] = []
            data[ensemble]['status'] = {}
            data[ensemble]['status']['krylov'] = []
            data[ensemble]['status']['avg_gcr'] = []
            data[ensemble]['n'] = 0


        data[ensemble]['time'].append(time)
        data[ensemble]['name'] = name
        data[ensemble]['n'] += 1
        data[ensemble]['alpha'] = alpha
        data[ensemble]['status']['krylov'].append(status[0])
        data[ensemble]['status']['avg_gcr'].append(status[1])

    for ensemble in data.keys():
        if fnmatch.fnmatch(ensemble, "*QCD_only"):
            data[ensemble]['alpha'] = 0.0
        avg = ufloat(np.nanmean(data[ensemble]['time']), np.nanstd(data[ensemble]['time']))
        data[ensemble]['time'] = avg

        avg = ufloat(np.nanmean(data[ensemble]['status']['krylov']), np.nanstd(data[ensemble]['status']['krylov']))
        data[ensemble]['status']['krylov'] = avg

        avg = ufloat(np.nanmean(data[ensemble]['status']['avg_gcr']), np.nanstd(data[ensemble]['status']['avg_gcr']))
        data[ensemble]['status']['avg_gcr'] = avg

    print(json.dumps(data, default=str, indent = 2))
    return data

def plot(args, plot_data):

    plt.rcParams.update({'font.size': 15})
    fig, axes = plt.subplots(1, 3)
    fig.set_size_inches(18.5, 7.5, forward=True)
    titles = [
        r'Average inversion time (DFL+SAP+GCR)',
        r'Average # Krylov-steps',
        r'Average # GCR-steps per Krylov step'
    ]
    ylabels = [
        r'convergence time [s]',
        r'# Krylov steps',
        r'# GCR steps',
    ]

    for i,ax in enumerate(axes):
        ax.axvline(x = ALPHA, linestyle='--', color='black', label=r'$\alpha_{phys}$')
        ax.set_title(titles[i])
        ax.set_xlabel(r'$\alpha$')
        ax.set_ylabel(ylabels[i])



    def get_nth_key(n, d):
        for i,k in enumerate(d.keys()):
            if i==n:
                return k

    if args.reorder is not None:
        d = {get_nth_key(n, plot_data): plot_data[get_nth_key(n, plot_data)] for n in args.reorder}
        plot_data = d

    for i,(ensemble,value) in enumerate(plot_data.items()):
        if args.reorder is not None:
            i = args.reorder[i]

        if i < len(args.labels):
            label = f"{args.labels[i]}<tab>{value['n']*NINV} inversions"
        else:
            label = f"{value['name']}<tab>{value['n']*NINV} inversions"

        if i < len(args.alphas):
            alpha = args.alphas[i]
        else:
            alpha = value['alpha']

        axes[0].errorbar([alpha], [value['time'].n/NINV],
            yerr = [value['time'].s/NINV],
            fmt = 'x',
            label = label,
            capsize = 6
        )

        axes[1].errorbar([alpha], [value['status']['krylov'].n],
            yerr = [value['status']['krylov'].s],
            fmt = 'x',
            label = label,
            capsize = 6
        )

        axes[2].errorbar([alpha], [value['status']['avg_gcr'].n],
            yerr = [value['status']['avg_gcr'].s],
            fmt = 'x',
            label = label,
            capsize = 6
        )

    # if ax in axes:
    #     pos = ax.get_position()
    #     ax.set_position([pos.x0, pos.y0, pos.width, 0.5*pos.height])

    h, l = axes[0].get_legend_handles_labels()
    ms.tablulated_legend(axes[1], bbox_to_anchor=(0.5, -0.2), loc="upper center")
    #ms.tablulated_legend(fig, h, l, bbox_to_anchor =(0.5,-0.27), loc='lower center')

    # box = ax.get_position()
    # ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    fig.tight_layout()

    if args.outfile == '-':
        plt.draw()
        plt.show()
    else:
        plt.savefig(args.outfile, dpi=fig.dpi, metadata={
            'Keywords': ' '.join(sys.argv)
        })
    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description = 'Compare timings.',
        formatter_class = argparse.RawTextHelpFormatter
    )

    parser.add_argument("-v", "--verbosity", action="count", help="increase output verbosity", default=0)
    required = parser.add_argument_group('required named arguments')
    required.add_argument('-i',
        help = 'input log file(s).',
        dest = "infile",
        metavar = 'FILE',
        required = True,
        nargs = '+'
    )
    parser.add_argument('-o', '--outfile', help='Output filename', default='-')
    parser.add_argument('-p', '--pedantic', help='exit if parsing a file fails', default=False, action="store_true")
    parser.add_argument('-l', '--labels', help='labels for the plot', nargs='+', default=[])
    parser.add_argument('-a', '--alphas', help='alphas for the plot', nargs='+', type=float, default=[])
    parser.add_argument('-r', '--reorder', help='reorder the data points', nargs='+', type=int, default=None)

    args = parser.parse_args()
    if args.verbosity:
        print("Running with the following arguments:")
        print(json.dumps(vars(args), indent = 4))
    
    plot_data = main(args)
    plot(args, plot_data)
