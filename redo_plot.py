#!/usr/bin/env python3
#
# Redo plots.
#

import argparse
from pdfrw import PdfReader
from mass import string_diff
import os
import re

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description = 'Redo plots.')
    parser.add_argument("-f", "--files", nargs="+", help="input file(s)")
    parser.add_argument("-u", "--update", default=False, help="update the plots(s)", action="store_true")
    parser.add_argument("-q", "--quiet", default=False, help="suppress output of the plot script", action="store_true")
    parser.add_argument("-o", "--outarg", nargs="+", default=["-o", "--outfile"], help="Argument(s) holding the output file path")

    args = parser.parse_args()
    print("#"*80 + "\n# pipe the output of this script to bash to redo the plots, i.e.\n# ./redo_plot.py -f ... | bash\n" + "#"*80)

    for file in args.files:
        file = os.path.abspath(os.path.expanduser(file))

        pdf = PdfReader(file)
        cmd = pdf.Info.Keywords.decode()
        cmd_orig = cmd
        for outarg in args.outarg:
            cmd = re.sub(rf"{re.escape(outarg)}\s+.*\.pdf", f"{outarg} {file}", cmd)

        if args.update:
            cmd = re.sub(r"\/[^\ \/]+\}", "/*", cmd)

        if args.quiet:
            cmd += " >/dev/null 2>&1"

        cwd = pdf.Info.Creator.decode()
        print("#"*80 + f"\n# I changed the following:\n# {string_diff(cmd_orig, cmd)}\n" + "#"*80)

        print(f"echo \"Redo plot {file}:\"")
        if os.path.isdir(cwd):
            print(f"cd {cwd}")
            print(f"{cmd} && echo 'Success' || echo 'failed'")
            print(f"cd - >/dev/null")
        else:
            print(f"{cmd}")
