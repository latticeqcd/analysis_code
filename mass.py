#!/usr/bin/env python3
#
# Plot the dat files produced from meson.c and extract the mass
# Needs libraries lmfit, tabulate, numdifftools, uncertainties, tqdm, matplotlib
#

import argparse
import numpy as np # np.asarray, np.zeros(), np.float16, np.float32, np.float64, np.linalg.norm()
import scipy as sc
import scipy.stats
import scipy.optimize # sc.optimize.curve_fit()
import struct # struct.unpack(), struct.pack()
import json # json.dumps()
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from matplotlib.ticker import MaxNLocator
from matplotlib.legend_handler import HandlerLine2D
import warnings
import lmfit as lm
import re
import numdifftools # to make sure lmfit has this dependency
import os
import sys
from tqdm import tqdm
import builtins
from uncertainties import ufloat, ufloat_fromstr, UFloat
from uncertainties.umath import exp, sinh, cosh
import time
import datetime
#import itertools
import tabulate
import inspect
from collections.abc import Iterable
import collections
import glob
import braceexpand
import subprocess
import fnmatch
import socket
import hashlib
import dill as pickle
import base64
import shlex
import tempfile
import difflib
import dateutil.parser

upper_right, upper_left, lower_left, lower_right = 1, 2, 3, 4

# overwrite builtin print() with tqdm.write()
old_print = print
def new_print(*args, **kwargs):
    # if tqdm.write() raises error, use builtin print
    try:
        tqdm.write(*args, **kwargs)
    except:
        old_print(*args, ** kwargs)
# globaly replace print with new_print
inspect.builtins.print = new_print

# returns True iff at least on item in a iterable (of iterables) is NaN.
def contains_nan(iterable):
    if not isinstance(iterable, Iterable):
        return np.isnan(iterable)

    r = False
    for item in iterable:
        r = r or contains_nan(item)
    return r

#
# Apply the jackknife resampling method to a data set.
#
# @param array numpy.ndarray: array of statistical data, eg: can be an array of (x,y)-pairs
# @param callable observable: a function that takes data and returns result_tuple, info_dict
#       @param numpy.ndarray data: data of the same shape as [[data]]
#       @return tuple result_tuple, dict info_dict
#               tuple result_tuple: a tuple of ufloats (value, stderr) determined by the observable
#               dict info_dict: an arbitrary dictionary containing additional information
# @return tuple result_tuple, dict info_dict
#       tuple result_tuple: a tuple of ufloats representing the result(s)
#       list[dict] info_dict: a list of info dictionaries returned by the observable for every sample
#
def jackknife(data, observable, verbose = True):
    n = len(data)
    theta_hat, _ = observable(data)
    theta = np.zeros((n, len(theta_hat)), dtype = type(theta_hat[0]))
    theta_err = np.zeros((n, len(theta_hat)), dtype = type(theta_hat[0]))
    info_dict = []

    if verbose:
        bar = tqdm(range(n))
        bar.set_description("jackknife")
    else:
        bar = range(n)

    for k in bar:
        data_k = np.delete(data, k, axis=0)
        rk, info_dict_k = observable(data_k)
        for i, res in enumerate(rk):
            theta[k,i], theta_err[k,i] = res.n, res.s
        info_dict.append(info_dict_k)

    if contains_nan(theta):
        warnings.warn(yellow("jackknife: some invocations of the observable returned NaN."))

    variances = ((n-1)/n)*np.nansum((theta - [t.n for t in theta_hat])**2, axis = 0)
    # we ignore the error coming from the observable
    #variances += (1/n)*np.sum(theta_err**2, axis = 0)
    theta_tilde = (1/n)*np.nansum(theta, axis = 0)
    # Hoelbling says it's usally better not to correct (stability)
    result = [t.n for t in theta_hat] # - (n-1)*(theta_tilde - [t.n for t in theta_hat])
    result_tuple = tuple([ufloat(value, np.sqrt(variance), "stat: jackknife") for value, variance in zip(result, variances)])
    return result_tuple, info_dict

#
# Apply the bootstrap resampling method to a data set.
#
# @param numpy.ndarray data: array of statistical data, eg: can be an array of (x,y)-pairs
# @param callable observable: @see [[jackknife]]
# @param int K: how many subsets should be created out of data
# @return tuple result_tuple, dict info_dict: @see [[jackknife]]
#
def bootstrap(data, observable, K, verbose = True):
    n = len(data)
    test, _ = observable(data)
    theta = np.zeros((K, len(test)), dtype = type(test[0]))
    theta_err = np.zeros((K, len(test)), dtype = type(test[0]))
    info_dict = []

    if verbose:
        bar = tqdm(range(K))
        bar.set_description("bootstrap")
    else:
        bar = range(K)

    for k in bar:
        idx = np.random.randint(n, size=n)
        rk, info_dict_k = observable(data[idx,:])
        for i, res in enumerate(rk):
            theta[k,i], theta_err[k,i] = res.n, res.s
        info_dict.append(info_dict_k)

    if contains_nan(theta):
        warnings.warn(yellow("bootstrap: some invocations of the observable returned NaN."))

    theta_tilde = (1/K)*np.sum(theta, axis = 0)
    variances =  (1/K)*np.sum((theta - theta_tilde)**2, axis = 0)
    result_tuple = tuple([ufloat(value, np.sqrt(variance), "stat: bootstrap") for value, variance in zip(theta_tilde, variances)])
    return result_tuple, info_dict

#
# Determines the covariance matrix from data
#
# @param 2darray data: the data, shape (a,b), where a = #samples and b = #(data-points per sample)
#   * data[j:,] = correlator values for j-th config for all nt
#   * data[:,i] = correlator values for all configs but at fixed nt=i
#   * data[j,i] = correlator value for j-th config at fixed nt=i
# @param string mode: possible options are
#   * full: the full covariance matrix
#   * diag: only the diagonal part
#   * normalized: returns C_ij/(sigma_i sigma_j) (diag entries are all 1.0)
#   * mode:n: where [[mode]] is one of the above and n the number of smallest eigenvalues to remove
#       * n can be -1 as well; then all negative eigenvalues are automatically removed
# @param bool sqrt: Also determine the sqrt of the inverse covariance matrix
# @param int verbosity: >=2 prints diagnose information
# @return tuple of 2darray covariance matrix and its inverse (and the sqrt of the inverse of [[sqrt]] is True)
# @todo: other modes like, average mode, ... 
# @see: Christian Hoelbling page 32
# @see: https://arxiv.org/pdf/1101.2248.pdf page 9 for more methods
#
# This function behaves just as (1/N)*numpy.cov(data.transpose()), where N is
# the number of configs (data points), if mode=full:0
#
def covariance_matrix(data, mode = 'full', sqrt = False, verbosity = 0):
    dof, N = data.shape[1], data.shape[0]
    if N == 1:
        if sqrt:
            return np.eye(dof), np.eye(dof), np.eye(dof)
        else:
            return np.eye(dof), np.eye(dof)
    cov = np.zeros((dof, dof), dtype = np.float64)
    data_mean = np.mean(data, axis = 0)

    for i in range(dof):
        for j in range(dof):
            cov[i,j] = np.mean((data[:,i] - data_mean[i])*(data[:,j] - data_mean[j]))/(N-1)

    if ":" in mode:
        mode, trunc = mode.split(":")
        trunc = int(trunc)
        if trunc >= cov.shape[0] or trunc < -1: raise ValueError(red('n out of range in --mode <mode>:<n>.'))
    else:
        trunc = 0

    if mode == 'diag':
        cov = np.diag(np.diag(cov)) # all non-diagonal entries =0
    elif mode == 'normalized':
        S = np.diag(1/np.sqrt(np.diag(cov)))
        cov = S @ cov @ S # c_ij /= sigma_i*sigma_j (diagonal is all 1)

    # calculate the inverse & sqrt of inverse
    w, V = np.linalg.eigh(cov)
    if trunc == -1: # cut off all negative eigenvalues
        trunc = w[w<0].size
    D = np.diag(np.pad(1/w[trunc:], (0,len(w[:trunc])))) # truncated diagonal matrix
    inv = V @ D @ V.T

    if sqrt:
        sqrt_inv = V @ np.sqrt(D+0j) @ V.T # allow to make the matrix complex

    if w[w<0].size > 0: warnings.warn(yellow("Covariance matrix contains negative eigenvalues."))

    if verbosity >= 2:
        print("eigenvalues of the {0} covariance matrix (red: truncated, green: used):".format(mode))
        for k in range(0, cov.shape[0]):
            print('{0}: {1}'.format(k+1, green(w[k]) if k >= trunc else red(strike(w[k]))))
        cond = np.linalg.cond(cov)
        print(f"condition number {mode} covariance matrix: {green(cond) if cond <= 1e4 else red(cond)}")
        if sqrt:
            sqrt_norm = np.linalg.norm(inv-sqrt_inv.dot(sqrt_inv))
            print(f"quality of sqrt matrix (wrt inv): {green(sqrt_norm) if sqrt_norm <= 1e-8 else red(sqrt_norm)}")
        inv_norm = np.linalg.norm(inv.dot(cov)-np.eye(dof))
        print(f"quality of inv matrix (wrt original cov): {green(inv_norm) if inv_norm <= 1e-8 else red(inv_norm)}")

    if inv[inv.imag!=0].size > 0:
        warnings.warn(yellow("Inverse covariance matrix contains complex values."))
    else:
        inv = inv.real

    if cov[cov.imag!=0].size > 0:
        warnings.warn(yellow("Covariance matrix contains complex values."))
    else:
        cov = cov.real

    if sqrt:
        if np.isnan(sqrt_inv).any():
            raise ValueError(red('sqrt of inverse covariance matrix contains NaNs.'))

        if sqrt_inv[sqrt_inv.imag!=0].size > 0:
            warnings.warn(yellow("sqrt of inverse covariance matrix contains complex values."))
        else:
            sqrt_inv = sqrt_inv.real

        return cov, inv, sqrt_inv
    else:
        return cov, inv

# @returns the range in terms of a list of indices
def obtain_range(_range, exclude = []):
    r = []
    nh = int(len(_range)/2)
    for ri in range(nh):
        r = np.union1d(r, np.arange(_range[2*ri], _range[2*ri+1]+1))
    r = np.array(np.setdiff1d(r, exclude), dtype=int)
    return r

# average all nsrc sources of a given config
def avg_sources(G, nsrc, Gerr = None, flatten = True):
    if nsrc == 1:
        if flatten: G = G.flatten()
        return G, np.zeros_like(G) if Gerr is None else Gerr, np.zeros_like(G) if Gerr is None else Gerr**2

    N0 = G.shape[-1]
    G = G.reshape(int(G.shape[0]/nsrc), -1, N0)
    Gmean = np.mean(G, axis = 1)

    if Gerr is None or np.all((Gerr == 0)): # standard error of the mean
        Gerr = np.nanstd(G, axis = 1)
        Gvar = np.var(G, axis = 1)
    else: # regular error propagation
        Gerr = Gerr.reshape(int(Gerr.shape[0]/nsrc), -1, N0)
        Gerr = (1/nsrc)*np.sqrt(np.sum(Gerr**2, axis=1))
        Gvar = None

    # remove superfluous dimensions
    if Gmean.shape[0] == 1 and flatten:
        Gmean = Gmean.flatten()
        Gerr = Gerr.flatten()

    return Gmean, Gerr, Gvar

# fold according to fold variant
def fold(G, fold, N0, Gerr = None):
    N0h = int(N0/2)
    fold_axis = 0.5*float(N0)

    # @todo: How to deal with negative values?
    if G[G<0].size > 0: warnings.warn(yellow("Correlator G contains negative values."))


    if fold == 0:
        G1, G2 = G, np.array([])
        r1, r2 = np.arange(0, N0), []
        if Gerr is None or np.all((Gerr == 0)): Gerr = np.zeros_like(G)
    else:
        if fold == 1:
            # fold variant 1 (the obvious one, fold at 31.5 if N0=64)
            r1 = np.arange(0, N0h)
            r2 = np.flip(np.arange(N0h, N0))
            fold_axis = 0.5*float(N0-1)
        elif fold == 2:
            # fold variant 2 (remove first pt, middle counts twice, fold at 32 if N0=64)
            r1 = np.arange(1, N0h+1)
            r2 = np.flip(np.arange(N0h, N0))
            fold_axis = 0.5*float(N0)
        elif fold == 3:
            # fold variant 3 (remove last and middle pt, anians)
            r1 = np.arange(0, N0h)
            r2 = np.flip(np.arange(N0h-1, N0-1))
            fold_axis = 0.5*float(N0)
        elif fold == 4:
            # fold variant 4 (remove middle, first counts twice)
            r1 = np.arange(0, N0h)
            r2 = np.flip(np.concatenate((np.arange(N0h+1, N0), [0])))
            fold_axis = 0.5*float(N0)

        G1 = G[:,r1]
        G2 = G[:,r2]
        G1_err = Gerr[:,r1]
        G2_err = Gerr[:,r2]

        # @choice: when folding take the mean or increase statistics? Always the mean
        #G = np.concatenate((G1, G2), axis=0) # more statistics
        G = np.mean([G1, G2], axis=0) # mean (marina: might remove numerical artifacts? they might be correlated)
        if Gerr is None or np.all((Gerr == 0)): # standard error of the mean
            Gerr = np.nanstd([G1, G2], axis=0)
        else: # regular error propagation
            Gerr = (1/2)*np.sqrt(G1_err**2 + G2_err**2)
        N0 = N0h

    return G, Gerr, N0, G1, G2, r1, r2, fold_axis

# @see https://stackoverflow.com/a/480227
def unique_list(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]

# def compactify_legend(ax1, ax2):
#     h1, l1 = ax1.get_legend_handles_labels()
#     h2, l2 = ax2.get_legend_handles_labels()
#     h = h1 + h2
#     l = l1 + l2

#     labels = []
#     handles = []
#     for label in l:
#         indices = [key for key, value in enumerate(l) if value == label]
#         if len(indices) == 1:
#             handles.append(h[indices[0]])
#             labels.append(l[indices[0]])
#         else:
#             handles.append(tuple([h[i] for i in indices]))
#             labels.append(label)

#     return unique_list(handles), unique_list(labels)

def compactify_legend(axes):
    h, l = [], []
    for ax in axes:
        hi, li = ax.get_legend_handles_labels()
        h += hi
        l += li

    labels = []
    handles = []
    for label in l:
        indices = [key for key, value in enumerate(l) if value == label]
        if len(indices) == 1:
            handles.append(h[indices[0]])
            labels.append(l[indices[0]])
        else:
            handles.append(tuple([h[i] for i in indices]))
            labels.append(label)

    return unique_list(handles), unique_list(labels)

from matplotlib.patches import Rectangle
def tablulated_legend(ax, handles=None, labels=None, **kwargs):
    extra = [Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0)]
    empty = [""]
    if handles is None:
        handles, _ = ax.get_legend_handles_labels()
    if labels is None:
        _, labels = ax.get_legend_handles_labels()

    cols = []
    for i, label in enumerate(labels):
        labels[i] = label.split('<tab>')
        cols.append(len(labels[i]))
    if np.min(cols) != np.max(cols):
        print(yellow("Not all labels have equal columns."))

    cols = np.max(cols)
    if cols == 1:
        return ax.legend(handles, labels, **kwargs)

    rows = len(labels)
    ls = empty*rows
    handles.extend(extra*rows)
    ls.extend(empty*rows)
    for c in range(cols):
        handles.extend(extra*rows)
        for label in labels:
            ls.append(label[c] if c<len(label) else "")

    return ax.legend(handles, ls, ncol=cols+2, handletextpad=-2, **kwargs)

# makes chisq red or green
def format_chisq(chisq):
    string = "χ²/d = {}"
    if 0.5 < chisq < 3:
        return string.format(green('{0:.2f}'.format(chisq)))
    else:
        return string.format(red('{0:.2f}'.format(chisq)))
    # if chisq < 0.5 or chisq > 3:
    #     return string.format(red('{0:.2f}'.format(chisq)))
    # else:
    #     return string.format(green('{0:.2f}'.format(chisq)))

def format_sigma(s, fmt = '{}'):
    if s >= 5:
        return red(fmt.format(s))
    elif s >= 1:
        return yellow(fmt.format(s))
    else:
        return green(fmt.format(s))

def box(text, center = False, title = None, width = None, head = 'regular', tail = 'regular'):
    if isinstance(text, list):
        widths = []
        for txt in text:
            lines = txt.rstrip().split('\n')
            widths.append(np.maximum(0 if width is None else width, len(max(lines, key = len)) + 2) if width is None else width)
        width = np.max(widths)
        r = box(None, center = center, title = title, width = width, tail = None)
        for txt in text:
            r += box(txt, center = center, title = None, width = width, head = 'intermediate', tail = None)
        if tail == 'regular':
            r += '└' + ('─'*width) + '┘'
        elif tail == 'intermediate':
            r += '├' + ('─'*width) + '┤\n'
        return r

    if title is None and text is None:
        lines = []
    elif title is None and text is not None:
        lines = text.rstrip().split('\n')
    elif title is not None and text is None:
        lines = []
    else:
        lines = (title + '\n' + text).rstrip().split('\n')
    width = len(max(lines, key = len)) + 2 if width == None else width
    height = len(lines)
    r = ''

    if head == 'regular':
        r += '┌' + ('─'*width) + '┐\n'
    elif head == 'intermediate':
        r += '├' + ('─'*width) + '┤\n'

    if title != None:
        r += box(title, center = True, width = width, title = None, head = None, tail = 'intermediate')

    for line in lines:
        rlen = len(re.sub(u'\033\[[0-9]+m', '', line)) # removed colors
        pre = int((width-2-rlen)/2) if center else 0
        post = int((width-2-rlen+1)/2) if center else width-2-rlen
        r += '│ ' + (' '*pre) + line + (' '*post) + ' │\n'
    if tail == 'regular':
        r += '└' + ('─'*width) + '┘'
    elif tail == 'intermediate':
        r += '├' + ('─'*width) + '┤\n'
    return r

strike = lambda text: ''.join([u'\u0336{}'.format(c) for c in str(text)])
red = lambda text: '\033[91m{}\033[0m'.format(text)
green = lambda text: '\033[92m{}\033[0m'.format(text)
gray = lambda text: '\033[37m{}\033[0m'.format(text)
white = lambda text: text
yellow = lambda text: '\033[93m{}\033[0m'.format(text)
blue = lambda text: '\033[94m{}\033[0m'.format(text)
bold = lambda text: '\033[1m{}\033[0m'.format(text)
clean = lambda text: re.sub(r"\x1b\[([0-9]+)m", "", text)

# wrapper for np.clip(), but also gives a warning when clipping
def clip(a, *args, **kwargs):
    r = np.clip(a, *args, **kwargs)
    if not np.array_equal(a, r):
        warnings.warn(yellow(f"Clipped some elements to prevent over-/underflows."))
    return r

# lists the error contributions of variable
def error_contributions(variable, name):
    rel = np.abs(100*variable.s/variable.n)
    text = f"Error contributions to {name} = {variable}: (total error: {variable.s}, {rel:.1f}%)\n"
    for (var, error) in variable.error_components().items():
        rel = np.abs(100*error/variable.n)
        text += f" - {var.tag}: {error} ({rel:.1f}%)\n"
    return text

# lists the error contributions of variable as a dict
def error_contributions_dict(variable, misc = None):
    ret = {}
    ret['value'] = variable
    ret['n'] = variable.n
    ret['s'] = variable.s
    if misc is not None:
        ret['misc'] = {}
        for key, value in misc.items():
            ret['misc'][key] = value
    ret['errors'] = {}
    for (var, error) in variable.error_components().items():
        ret['errors'][var.tag] = error
    return ret

#
# Function to vary a general function f.
#
# @param list *pargs: list of list of positional arguments for the function f (all the variances)
# @param callable f: function to vary the inputs of
# @param string vary_param: name of the parameter that is varied (only for info)
# @param callable aggregate: function that is fed with each result
# @param callable collect: function that is fed with a list of aggregated results (return values of aggregate())
# @return mixed: the return value of collect()
#
from concurrent.futures import ThreadPoolExecutor
def vary_parameter(*pargs, f, vary_param, aggregate, collect):
    ret = []
    bar = tqdm(total = len(list(zip(*pargs))), desc = f"varying the {vary_param}")

    executor = ThreadPoolExecutor(max_workers=1)
    futures = [executor.submit(f, *parameters) for parameters in zip(*pargs)]

    for i, parameters in enumerate(zip(*pargs)):
        ret.append(aggregate(i, futures[i].result(), *parameters))
        bar.update()

    executor.shutdown() # blocks
    bar.close()
    return collect(ret)

def vary_parameter_serial(*pargs, f, vary_param, aggregate, collect):
    ret = []
    bar = tqdm(total = len(list(zip(*pargs))), desc = f"varying the {vary_param}")
    for i, parameters in enumerate(zip(*pargs)):
        ret.append(aggregate(i, f(*parameters), *parameters))
        bar.update()
    bar.close()

    return collect(ret)

# expand a brace expansion eg: a{b,c}*d -> [ab*d, ac*d]
def braced_glob(path, **kwargs):
    l = []
    for x in braceexpand.braceexpand(path):
        l.extend(glob.glob(x, **kwargs))
    return l

def brace_match(string, pattern):
    return np.any([fnmatch.fnmatch(string, p) for p in braceexpand.braceexpand(pattern)])

def read_infiles(infiles, verbose):
    int_size, float_size, double_size = 4, 4, 8 # in bytes

    inf = []
    for i, path in enumerate(infiles):
        if os.path.isdir(path):
            path = f"{path}/*.dat"
        for name in braced_glob(path):
            inf.append(name)

    Gname = []
    with open(inf[0], "rb") as f:
        N0 = struct.unpack('<1I', f.read(int_size))[0] # 1 unsigned int I, < little endian
        N1 = struct.unpack('<1I', f.read(int_size))[0]
        N2 = struct.unpack('<1I', f.read(int_size))[0]
        N3 = struct.unpack('<1I', f.read(int_size))[0]
        if verbose: print("lattice size was {0}x{1}x{2}x{3}".format(N0, N1, N2, N3))
        G = np.zeros((len(inf), N0), dtype=np.complex64)

    for i, infile in enumerate(bar := tqdm(inf)):
        with open(infile, "rb") as f:
            if verbose: bar.set_description(f"reading file {os.path.basename(infile)}")

            N0 = struct.unpack('<1I', f.read(int_size))[0] # 1 unsigned int I, < little endian
            N1 = struct.unpack('<1I', f.read(int_size))[0]
            N2 = struct.unpack('<1I', f.read(int_size))[0]
            N3 = struct.unpack('<1I', f.read(int_size))[0]

            datam = struct.unpack('<{0}d'.format(2*N0), f.read(double_size*2*N0)) # 2*N0 doubles

            G[i].real = np.array(datam[::2])  # only even indices, real part
            G[i].imag = np.array(datam[1::2]) # only odd  indices, imag part
            Gname.append(infile)
    return G, (N0, N1, N2, N3), np.array(Gname), len(inf)

# returns the correct model function.
# @param string fit_type: the type of the fit
# @return callable: the model function
# @notice: that with x=0.0 the exponential in front can be disabled (good for the ratio-fits)
def get_curve(fit_type):
    # clip truncates values larger than max and smaller than min, else they whould overflow
    # cosh(±700) ~ 5e-303, exp(±700) ~ e±303
    if fit_type == 'exp':
        return lambda nt, A, meff, x=1.0: A*np.exp(clip(-meff*nt, a_min = -700, a_max = 700))
    elif fit_type == 'sinh':
        return lambda nt, A, meff, x=1.0: A*np.sinh((nt - fold_axis)*meff)
    elif fit_type == 'cosh':
        return lambda nt, A, meff, x=1.0: A*np.cosh(clip(((nt - fold_axis)*meff), a_min = -700, a_max = 700))
    elif fit_type == 'exp_sinh':
        return lambda nt, A, meff, x=1.0: 2*A*np.exp(-x*meff*fold_axis)*np.sinh((nt - fold_axis)*meff)
    elif fit_type == 'exp_cosh':
        return lambda nt, A, meff, x=1.0: 2*A*np.exp(-x*meff*fold_axis)*np.cosh(clip(((nt - fold_axis)*meff), a_min = -700, a_max = 700))
    else:
        return None

#
# Function to determine plot limits based on (many) input objects
#
# @param list *args: list of argument. Can be numbers, numpy arrays, lists, ...
# @param float shift: percentage to shift min and max values
# @param float epsilon: value to add and subtract from min and max
# @param bool no_zeros: zeros should be excluded or not
# @return tuple(min, max): the calculated limits
#
def limits(*args, shift = 0.0, epsilon = 0.0, no_zeros = True):
    numbers = []
    for item in args:
        if (isinstance(item, list)):
            numbers += item
        elif (isinstance(item, (np.ndarray, np.generic))):
            numbers.extend(item)
        else:
            numbers.append(item)

    additional_numbers = []
    #print(numbers)
    for i, number in enumerate(numbers):
        if (isinstance(number, UFloat)):
            if no_zeros and number.n == 0:
                numbers[i] = np.nan
            else:
                if np.isnan(number.s):
                    additional_numbers.append(number.n-epsilon)
                    numbers[i] = number.n+epsilon
                else:
                    additional_numbers.append(number.n+number.s-epsilon)
                    additional_numbers.append(number.n-number.s+epsilon)
                    additional_numbers.append(number.n-number.s-epsilon)
                    numbers[i] = number.n+number.s+epsilon
        else:
            if no_zeros and number == 0:
                numbers[i] = np.nan
            else:
                additional_numbers.append(number-epsilon)
                numbers[i] = number+epsilon

    y1 = np.nanmin(numbers + additional_numbers)
    y2 = np.nanmax(numbers + additional_numbers)
    yshift = shift*(y2 - y1)
    y1 -= yshift
    y2 += yshift

    return y1, y2

#
# Class inheriting argparse.ArgumentParser to give the functionality of
# storing and loading argument from a json file
#
# @property string json_file: path to a file to store/load configs from
#  (defaults to <scriptname>.json)
# @property dict json: contents of the json file self.json_file
#
class JsonArgumentParser(argparse.ArgumentParser):
    # @param: string json_file:
    # @param: callable plot_function:
    def __init__(self,
            json_file=None,
            plot_function=None,
            plot_multiple_function=None,
            check_function=lambda x: (True, None),
            exceptions=[],
            default_actions={},
            main_result=None,
            *args, **kwargs
        ):
        self.start = time.time()
        self.args = None
        self.custom_specifiers = ['open', 'table']
        self.name = os.path.splitext(os.path.basename(sys.argv[0]))[0]
        if json_file is None:
            # name of the script with extension .json
            json_file = self.name + '.json'
        self.json_file = json_file
        self.exceptions = exceptions
        self.plot_function = plot_function
        self.plot_multiple_function = plot_multiple_function
        self.check_function = check_function
        self.exceptions = exceptions
        self.default_actions = default_actions
        self.main_result = main_result
        self.current_hash = hashlib.md5(open(sys.argv[0],'r').read().encode('utf-8')).hexdigest()
        self.fshow = [False, False, False, False]
        self._json = None
        self.footnotes = [
            "[1]: The command was edited after the config was executed. This config should be rerun (see --config-rerun).",
            f"[2]: The results where obtained with a older version of the script {sys.argv[0]}.",
            "[3]: No results available (probably currently running or crashed/aborted before writing results).",
            "[4]: The check-function has failed: {}."
        ]
        self.pre_parser = argparse.ArgumentParser(add_help=False)
        group = self.pre_parser.add_mutually_exclusive_group()
        self.pre_parser.add_argument('--config-store', help=f'store config to {self.json_file}', default=None, metavar="NAME")
        self.pre_parser.add_argument('--config-load', help=f'read config automatically from {self.json_file}', default=None, metavar="NAME")
        self.pre_parser.add_argument('--config-rerun', help=f'shortcut for --config-store and config-load', nargs='+', default=None, metavar="NAME")
        self.pre_parser.add_argument('--config-plot', help=f'Run the plotting again', nargs='+', default=None, metavar="NAME")
        self.pre_parser.add_argument('--config-comment', nargs=2, help=f'add a comment to a config', default=None, metavar=("NAME", "COMMENT"))
        self.pre_parser.add_argument('--config-edit', help=f'add edit the command of a config', default=None, metavar="NAME")
        self.pre_parser.add_argument('--config-list-fmt', help=f'output table format for --config-list', default=None, metavar="FORMAT")
        group.add_argument('--config-remove', help=f'remove a config by name', default=None, metavar="NAME")
        group.add_argument('--config-show', help=f'show full config', default=None, metavar="NAME")
        group.add_argument('--config-list', nargs='*', help=f'list all available configs in {self.json_file}. Arguments can be items to list in the format <name>[=<filter>][%%<action>][&<format>][?<display name>], where <name> is the name of the attribute, <filter> filters the attribute (can contains wildcards, see fnmatch(), i.e. test*, default *), <action> is an action that is applied to the value before formatting, <format> is a format specifier (i.e. .2f, default no formatting), and <display name> is the table header to display (default <name>). The following names are available for <name>: name (the run name), id (numbering), command (the full command that was issued), result.* (the result dictionary given by parser.set_result()), args.* (the parsed arguments object), meta.* (some meta information about the run). Notice that the whole tree of the json result can be accessed by separating the path with a dot (i.e. results.errors.xyz[0]). Examples: "result.meff*jack*.value%%ufloat&.1uS?meff" formats a ufloat number as 0.8044(5)', default=None)
        self.pre_parser.add_argument('--config-help', help='show this help message and exit', action='help')
        argparse.ArgumentParser.__init__(self, *args, **kwargs)

    @staticmethod
    def decode(data):
        return base64.b64encode(pickle.dumps(data)).decode('utf-8')

    @staticmethod
    def encode(data):
        return pickle.loads(base64.b64decode(data))

    @staticmethod
    def join_command(cmd):
        return shlex.join(cmd)

    @staticmethod
    def split_command(cmd):
        return shlex.split(cmd)

    def set_result(self, result, plot_data = None):
        now = datetime.datetime.now().isoformat()
        meta = {
            'duration': time.time() - self.start,
            'mtime': now,
            'cmd_mtime': now,
            'hostname': socket.gethostname(),
            'hash': hashlib.md5(open(sys.argv[0],'r').read().encode('utf-8')).hexdigest()
        }
        if self.pre_args.config_store is not None:
            self._json = None
            data = self.json
            data[self.pre_args.config_store]['result'] = self.decode(result)
            data[self.pre_args.config_store]['args'] = self.decode(self.args)
            data[self.pre_args.config_store]['plot_data'] = self.decode(plot_data)
            old_meta = data[self.pre_args.config_store]['meta'] if 'meta' in data[self.pre_args.config_store] else {}
            meta = {**old_meta, **meta}
            data[self.pre_args.config_store]['meta'] = meta
            with open(self.json_file, 'wt') as f:
                json.dump(data, f, default=repr, indent = 4)
                print(blue(f"Storing current result as {self.pre_args.config_store} in {self.json_file}"))
        return meta

    def apply_action(self, action, key, arg, json):
        if arg is None:
            return arg

        if action is None:
            return self.default_actions[key](arg) if key in self.default_actions else arg
        elif action == 'ufloat':
            if (m := re.search("([0-9,\.]+\+\/\-[0-9,\.]+)", str(arg))):
                arg = m.group(1)
            return ufloat_fromstr(str(arg))
        elif action == 'open':
            print(blue(f"Opening {arg}"))
            subprocess.Popen(["xdg-open", arg])
        elif action == 'table':
            return tabulate.tabulate([[k,v] for k,v in arg.items()], tablefmt="plain") if getattr(arg, "items", None) else arg
        elif (m := re.search("truncate\((\-?[0-9]+)\)", action)):
            max_len = int(m.group(1))
            if max_len == -1:
                return arg
            else:
                return arg[0:max_len] + ('...' if len(arg) > max_len else '')
        elif (m := re.search("percentage_of\(([^\)]+)\)", action)):
            percentage_of = m.group(1)
            number = self.parse_item(0, percentage_of, "", json)[-1]
            return 100*float(arg)/float(number) if number is not None and arg is not None else arg
        elif action == 'timeago':
            import timeago
            return timeago.format(dateutil.parser.isoparse(arg), datetime.datetime.now())
        elif (m := re.search("cut\(([^\)]+)\)", action)):
            params = m.group(1).split(',')
            return arg.split(params[0], )[int(params[1])]
        return arg

    def apply_formatter(self, specifier, key, val, json):
        if specifier is not None and val is not None:
            # prepend with an invisible null-byte, else tabulate formats it as number
            val = str(f'\0{val:^{specifier}}')

        if key == 'name' and self.pre_args.config_list_fmt is None:
            if 'meta' in json:
                # command was edited
                if 'mtime' in json['meta'] and 'cmd_mtime' in json['meta']:
                    mtime = dateutil.parser.isoparse(json['meta']['mtime'])
                    cmd_mtime = dateutil.parser.isoparse(json['meta']['cmd_mtime'])
                    if mtime != cmd_mtime:
                        val += red('¹')
                        self.fshow[0] = True
                # script hash doesn't agree
                if 'hash' in json['meta']:
                    if json['meta']['hash'] != self.current_hash:
                        val += red('²')
                        self.fshow[1] = True

            if 'result' not in json:
                # Currently running
                val += red('³')
                self.fshow[2] = True

            try:
                ccode, cval = self.check_function(json)
            except Exception as e:
                ccode, cval = False, e

            if not ccode:
                val += red('⁴')
                self.fshow[3] = True
                self.footnotes[3] = self.footnotes[3].format(cval)
        return val

    @property
    def json(self):
        if self._json is None:
            if os.path.isfile(self.json_file):
                with open(self.json_file, 'rt') as f:
                    self._json = json.load(f)
        return self._json

    def remove_config_flags(self, argv):
        for arg in ['--config-store', '--config-load', '--config-rerun', '--config-list','--config-help', '--config-plot']:
            if arg in argv:
                index = argv.index(arg)
                argv[index] = 0
                for i in range(index+1, len(argv)):
                    if argv[i].startswith('-') and argv[i] != '-':
                        break
                    else:
                        argv[i] = 0
        return list(filter(lambda a: a != 0, argv))

    def parse_item(self, i, key, name, json):
        if "?" in key:
            key, header = key.split("?", 1)
        else:
            header = key
        if "&" in key:
            key, specifier = key.split("&", 1)
        else: 
            specifier = None
        if "%" in key:
            key, action = key.split("%", 1)
        else:
            action = None
        if "=" in key:
            key, condition = key.split("=", 1)
        else:
            condition = "*"

        if key == 'name':
            el = name
        elif key == 'id':
            el = i
        elif key == 'command':
            el = self.join_command(json['command'] if 'command' in json else json)
        else:
            path = list(filter(None, re.split('\.|\[|\]\.|\]', key)))
            path = [int(p) if p.isdigit() else p for p in path]
            el = json
            el['args'] = vars(argparse.ArgumentParser.parse_args(self, args=el['command'][1:]))
            if 'args' in el and isinstance(el['args'], str):
                el['args'] = vars(self.encode(el['args']))
            if 'result' in el and isinstance(el['result'], str):
                el['result'] = self.encode(el['result'])
            for p in path:
                if "*" in str(p):
                    try:
                        for subkey in el.keys():
                            if fnmatch.fnmatch(str(subkey).lower(),str(p).lower()):
                                header = header.replace(str(p), str(subkey), 1)
                                p = subkey
                                break
                    except:
                        pass
                try:
                    el = el[p]
                except TypeError:
                    try:
                        el = getattr(el, p)
                    except:
                        el = None
                except:
                    el = None
        return key, header, specifier, action, condition, el

    def differing_args(self, max_items = 9999):
        s = []
        for name1, dict1 in self.json.items():
            if 'args' in dict1:
                args1 = vars(argparse.ArgumentParser.parse_args(self, args=dict1['command'][1:]))
                for name2, dict2 in self.json.items():
                    if 'args' in dict2:
                        args2 = vars(argparse.ArgumentParser.parse_args(self, args=dict2['command'][1:]))
                        for key1, value1 in args1.items():
                            for key2, value2 in args2.items():
                                if key1 == key2 and value1 != value2 and f"args.{key1}" not in s:
                                    s.append(f"args.{key1}")
        return [e for e in s if e not in map(lambda x: f"args.{x}", self.exceptions)][0:max_items]

    def parse_args(self, *args, **kwargs):
        self.pre_args, _ = self.pre_parser.parse_known_args()

        if self.pre_args.config_edit is not None:
            data = self.json
            if self.pre_args.config_edit in data:
                cfg = self.json[self.pre_args.config_edit]
                old_command = self.join_command(cfg['command'])
                editor = os.environ.get('EDITOR','vim')
                print(blue(f"Editing {self.pre_args.config_edit} with {editor}:"))

                with tempfile.NamedTemporaryFile(mode='wt', suffix=".tmp") as f:
                    f.write(old_command)
                    f.flush()
                    subprocess.call([editor, f.name])
                    with open(f.name, 'rt') as fc:
                        new_command = fc.read().strip()

                args = argparse.ArgumentParser.parse_args(self, args=self.split_command(new_command)[1:])
                if new_command != old_command:
                    data[self.pre_args.config_edit]['command'] = self.split_command(new_command)
                    if 'meta' not in data[self.pre_args.config_edit]:
                        data[self.pre_args.config_edit]['meta'] = {}
                    data[self.pre_args.config_edit]['meta']['cmd_mtime'] = datetime.datetime.now().isoformat()
                    with open(self.json_file, 'wt') as f:
                        json.dump(data, f, default=str, indent = 4)
                        print(blue(f"Updated command in {self.pre_args.config_edit}:"))
                        print(string_diff(old_command, new_command))
                else:
                    print(blue(f"Nothing to update in {self.pre_args.config_edit}"))


            else:
                raise ValueError(red(f"config {self.pre_args.config_edit} not found in {self.json_file}"))  
            exit()

        if self.pre_args.config_rerun is not None:
            inspect.builtins.print = print
            commands = {}
            for name in self.pre_args.config_rerun:
                for name_in_json in self.json:
                    if fnmatch.fnmatch(name_in_json,name):
                        cmd = self.json[name_in_json]['command']
                        cmd.extend(self.remove_config_flags(sys.argv[1:]))
                        cmd.extend(['--config-store', name_in_json])
                        commands[name_in_json] = cmd

            processes = []
            for i, cmd in enumerate(commands.values()):
                print(blue(f"---> spawning process {i+1}/{len(commands)}: {self.join_command(cmd)}"))
                processes.append(subprocess.Popen(cmd))
                time.sleep(1) # not all at the same time; gives contention when writing to the json-file
            ret = [p.communicate() for p in processes]
            ret = [p.wait() for p in processes]
            print(json.dumps({k: {'returncode': not bool(p.returncode)} for k,p in zip(commands.keys(), processes)},
                default=str,
                indent=4
            ))
            exit()

        if self.pre_args.config_plot is not None:
            if len(self.pre_args.config_plot) == 1:
                if self.plot_function is None:
                    raise ValueError(red(f"plot_function not defined"))
                name = self.pre_args.config_plot[0]
                if name in self.json:
                    cfg = self.json[name]
                    argv = self.remove_config_flags(cfg['command'] + sys.argv[1:])
                    self.args = argparse.ArgumentParser.parse_args(self, args=argv[1:])
                    self.plot_function(self.args, self.encode(cfg['plot_data']))
                else:
                    raise ValueError(red(f"config {name} not found in {self.json_file}"))
            else:
                if self.plot_multiple_function is None:
                    raise ValueError(red(f"plot_multiple_function not defined"))
                args = []
                plot_data = []
                for name in self.pre_args.config_plot:
                    if name in self.json:
                        cfg = self.json[name]
                        argv = self.remove_config_flags(cfg['command'] + sys.argv[1:])
                        args.append(argparse.ArgumentParser.parse_args(self, args=argv[1:]))
                        plot_data.append(self.encode(cfg['plot_data']))
                    else:
                        raise ValueError(red(f"config {name} not found in {self.json_file}"))  
                self.plot_multiple_function(args, plot_data)
            exit()

        # if self.pre_args.config_plot is not None:
        #     items = {}
        #     for name in self.pre_args.config_plot:
        #         for name_in_json in self.json:
        #             if fnmatch.fnmatch(name_in_json,name):
        #                 cfg = self.json[name_in_json]
        #                 argv = self.remove_config_flags(cfg['command'] + sys.argv[1:])
        #                 self.args = argparse.ArgumentParser.parse_args(self, args=argv[1:])
        #                 items[name_in_json] = (self.args, self.encode(cfg['plot_data']))
            
        #     if self.plot_function is None:
        #         raise ValueError("No plot_function provided")
        #     from multiprocessing import Pool
        #     with Pool(len(items)) as p:
        #         print({k: {'returnvalue': v} for k,v in zip(items.keys(), p.starmap(self.plot_function, items.values()))})

        #     exit()

        if self.pre_args.config_show is not None:
            if self.pre_args.config_show in self.json:
                cfg = self.json[self.pre_args.config_show]
                table = [['name', self.pre_args.config_show]]
                table.append(['command', self.join_command(cfg['command']) if 'command' in cfg else None])
                table.append(['result', json.dumps(self.encode(cfg['result']), default=repr, indent = 2) if 'result' in cfg else None])
                print(blue(f"Config {self.pre_args.config_show}:"))
                print(tabulate.tabulate(table, headers=["key", "value"]))
            else:
                raise ValueError(red(f"config {self.pre_args.config_show} not found in {self.json_file}"))  
            exit()

        if self.pre_args.config_remove is not None:
            if self.pre_args.config_remove in self.json:
                data = self.json
                del data[self.pre_args.config_remove]
                with open(self.json_file, 'wt') as f:
                    json.dump(data, f, default=str, indent = 4)
                    print(blue(f"Removing config {self.pre_args.config_remove} from {self.json_file}"))
            else:
                raise ValueError(red(f"config {self.pre_args.config_remove} not found in {self.json_file}"))  
            exit()

        if self.pre_args.config_list is not None:
            max_items = 9999

            if len(self.pre_args.config_list) == 0:
                self.pre_args.config_list = ['id', 'name', 'meta.comment', 'main_result', '+']
                if self.main_result is not None:
                    self.pre_args.config_list[3:3] = self.main_result
                max_items = 10
                print(blue(f"All differing arguments: {self.differing_args()}:"))

            for key, item in enumerate(self.pre_args.config_list):
                if (m := re.search("^\+([0-9]*)$", item)):
                    nitems = 9999 if m.group(1) == '' else int(m.group(1))
                    self.pre_args.config_list[key:key+1] = self.differing_args(nitems)
                elif item == 'main_result' and self.main_result is not None:
                    self.pre_args.config_list[key:key+1] = self.main_result

            self.pre_args.config_list = unique_list(self.pre_args.config_list)
            if ((rem := len(self.pre_args.config_list) - max_items) > 0):
                self.pre_args.config_list = self.pre_args.config_list[0:max_items] + [f"+{rem} items ..."]

            print(blue(f"Stored configs in {self.json_file}:"))
            table = []
            for i, name in enumerate(collections.OrderedDict(sorted(self.json.items()))):
                items = []
                conditions = []
                specifiers = []
                headers = []
                actions = []
                keys = []

                for key in self.pre_args.config_list:
                    key, header, specifier, action, condition, el = self.parse_item(i, key, name, self.json[name])
                    keys.append(key)
                    conditions.append(condition.lower())
                    specifiers.append(specifier)
                    headers.append(header)
                    actions.append(action)
                    items.append(el)

                # match condition
                if np.all([brace_match(str(val).lower(),condition) for val,condition in zip(items, conditions)]):

                    # apply action
                    for i, (key, val, action) in enumerate(zip(keys, items, actions)):
                        items[i] = self.apply_action(action, key, val, self.json[name])

                    # apply format specifier
                    for i, (key, val, specifier) in enumerate(zip(keys, items, specifiers)):
                        items[i] = self.apply_formatter(specifier, key, val, self.json[name])

                    table.append(items)

            # print table
            print(tabulate.tabulate(table,
                headers=headers,
                tablefmt="simple" if self.pre_args.config_list_fmt is None else self.pre_args.config_list_fmt)
            )

            # print footnotes
            print('\n'.join([self.footnotes[k] for k,v in enumerate(self.fshow) if v]))
            exit()

        if self.pre_args.config_comment is not None:
            if self.pre_args.config_comment[0] in self.json:
                data = self.json
                cfg = data[self.pre_args.config_comment[0]]
                meta = {'comment': self.pre_args.config_comment[1]}
                old_meta = cfg['meta'] if 'meta' in cfg else {}
                meta = {**old_meta, **meta}
                data[self.pre_args.config_comment[0]]['meta'] = meta
                with open(self.json_file, 'wt') as f:
                    json.dump(data, f, default=str, indent = 4)
                    print(blue(f"Adding comment to configuration {self.pre_args.config_comment[0]}"))
            else:
                raise ValueError(red(f"config {self.pre_args.config_load} not found in {self.json_file}")) 
            exit()

        if self.pre_args.config_load is not None:
            if self.pre_args.config_load in self.json:
                cfg = self.json[self.pre_args.config_load]
                argv = self.remove_config_flags(cfg['command'] + sys.argv[1:])
                command = self.join_command(argv)
                print(blue(f"Using stored configuration for {self.pre_args.config_load}: {command}"))
                self.args = argparse.ArgumentParser.parse_args(self, args=argv[1:])
                return self.args
            else:
                raise ValueError(red(f"config {self.pre_args.config_load} not found in {self.json_file}"))  

        if self.pre_args.config_store is not None:
            data = self.json
            argv = self.remove_config_flags(sys.argv)

            self.args = argparse.ArgumentParser.parse_args(self, args=argv[1:])
            data[self.pre_args.config_store] = {}
            data[self.pre_args.config_store]['command'] = argv
            with open(self.json_file, 'wt') as f:
                json.dump(data, f, default=str, indent = 4)
                print(blue(f"Storing current config as {self.pre_args.config_store} in {self.json_file}"))
            return self.args

        return argparse.ArgumentParser.parse_args(self, *args, **kwargs)

# @see https://stackoverflow.com/a/64404008/2768341
def string_diff(old, new):
    result = ""
    codes = difflib.SequenceMatcher(a=old, b=new).get_opcodes()
    for code in codes:
        if code[0] == "equal":
            result += gray(old[code[1]:code[2]])
        elif code[0] == "delete":
            result += red(old[code[1]:code[2]])
        elif code[0] == "insert":
            result += green(new[code[3]:code[4]])
        elif code[0] == "replace":
            result += (red(old[code[1]:code[2]]) + green(new[code[3]:code[4]]))
    return result

# truncate a file
def truncate_file(file):
    with open(file, "w") as fh:
        fh.truncate()

def get_target(outlist, endswidth, truncate = True):
    elist = [f for f in outlist if f.endswith(endswidth)]

    def fprint(*args, **kwargs):
        cargs = [clean(n) for n in args]
        print(*args, **kwargs)
        if len(elist) > 0:
            with open(elist[0], "a") as fh:
                print(*cargs, **kwargs, file=fh)

    if len(elist) > 0 and truncate:
        truncate_file(elist[0])
        return elist[0], True, fprint
    return sys.stdout, False, fprint

# Minimize Chi2 functional using a two parameter hypothesis function as in D&D
# @param 2darray data: the raw data points G[:,r] in range r
# @param array fit_range: fit-range
# @param string mode: the method used to calculate the covariance matrix (@see [[covariance_matrix()]])
# @param tuple cov: the return value of covariance_matrix() in order to recycle it
# @param int verbosity: level of verbosity
# @param ufloat reference: reference value of the mass (also the initial value for the minimizer)
# @param string fit: the fit type (exp, cosh, sinh, exp_cosh, exp_sinh)
# @return tuple result_tuple, dict info_dict
def observable_chi2_2par_DD(data, fit_range,
    mode = 'full',
    cov = None,
    verbosity = 0,
    reference = ufloat(0.0, 0.0),
    fit = 'exp',
):
    data = data.real
    dof, N = data.shape[1] -1, data.shape[0]
    ob = f'Observable (mass/A): two-parameter {fit}-fit with {mode} covariance matrix, d.o.f. = {dof} according to D&D'
    data_mean = np.nanmean(data, axis = 0)
    if cov == None:
        mcov, mcovinv, sqrt_mcovinv = covariance_matrix(data, mode = mode, sqrt = True, verbosity = verbosity)
    else:
        (mcov, mcovinv, sqrt_mcovinv) = cov

    if verbosity >= 2: print(blue(ob))

    curve = get_curve(fit)
    f = lambda nt, meff, A: curve(nt, A, meff) # hypothesis function with two parameters

    # as in D&D section 9.2.1
    #chisq = lambda alpha: (data_mean - f(r,*alpha)).dot(mcovinv).dot(data_mean - f(r,*alpha))

    fit_params = lm.Parameters()
    fit_params.add('meff',
        value = 0.1 if reference.n == 0.0 else reference.n,
        min = np.finfo(np.float64).eps,
        max = 10.0
    )
    fit_params.add('A',
        value = 0.001,
        min = np.finfo(np.float64).eps,
        max = 10.0
    )

    def residual(params, data, scovinv, frange, model):
        vals = params.valuesdict()
        return scovinv.dot(data - model(frange, vals['meff'], vals['A']))

    # good ones are: 'Nelder-Mead', 'Powell', 'L-BFGS-B', 'trust-constr'
    methods = [
        #"differential_evolution", # differential evolution
        #"brute", # brute force method
        #"basinhopping", # basinhopping
        #"ampgo", # Adaptive Memory Programming for Global Optimization
        "nelder", # Nelder-Mead
        #"lbfgsb", # L-BFGS-B
        #"powell", # Powell
        #"cg", # Conjugate-Gradient
        #"newton", # Newton-CG
        #"cobyla", # Cobyla
        #"bfgs", # BFGS
        #"tnc", # Truncated Newton
        #"trust-ncg", # Newton-CG trust-region
        #"trust-exact", # nearly exact trust-region
        #"trust-krylov", # Newton GLTR trust-region
        #"trust-constr", # trust-region for constrained optimization
        #"dogleg", # Dog-leg trust-region
        #"slsqp", # Sequential Linear Squares Programming
        #"emcee", # Maximum likelihood via Monte-Carlo Markov Chain
        #"shgo", # Simplicial Homology Global Optimization
        #"dual_annealing" # Dual Annealing optimization
    ]
    for method in methods:

        #try:
        res = lm.minimize(residual, fit_params,
            method = method,
            kws = {
                'data': data_mean, # ydata
                'scovinv': sqrt_mcovinv, # sqrt of inverse covariance matrix
                'frange': fit_range, # fit range
                'model': f # model hypothesis function
            },
            max_nfev = 10000
        )

        if verbosity >= 2:
            print(f"{method = }")
            print(lm.fit_report(res))
            print(format_chisq(res.redchi.real))
        # except Exception as e:
        #     warnings.warn(yellow(f"{method = } failed! Exception {e}"))

    it = iter(res.params.items())
    _, meff = next(it)
    _, A = next(it)
    if meff.stderr is None or A.stderr is None:
        warnings.warn(yellow(r"lmfit was not able to determine errors of the parameter, consider trying -f {exp_cosh, exp_sinh}"))
    meff = ufloat(np.abs(meff.value.real), np.abs(meff.stderr) if meff.stderr is not None else np.nan, "meff fit-error")
    A = ufloat(A.value, np.abs(A.stderr) if A.stderr is not None else np.nan, "amplitude A fit-error")
    if fit in ['exp_cosh', 'exp_sinh']:
        A *= (2*exp(-meff*fold_axis))

    result_tuple = (meff, A)
    info_dict = {
        'ob': ob,
        'redchi_m': res.redchi.real,
        'redchi_A': res.redchi.real,
        'lmfit_result': res
    }
    return result_tuple, info_dict

# Minimize Chi2 functional using two one-parameter hypothesis functions as in D&D
# @param numpy.ndarray data: the raw data points data[:,:,0] are the ratio and data[:,:,1] the raw data in range r
# @param array fit_range: fit-range
# @param string mode: the method used to calculate the covariance matrix (@see [[covariance_matrix()]])
# @param tuple(tuple) cov: tuple of covariance_matrix() for the ratio and raw data in order to recycle it
# @param int verbosity: level of verbosity
# @param ufloat reference: reference value of the mass (also the initial value for the minimizer)
# @param string fit: the fit type (exp, cosh, sinh, exp_cosh, exp_sinh)
# @return tuple result_tuple, dict info_dict
def observable_chi2_1par_DD(data, fit_range,
    mode = 'full',
    cov = None,
    verbosity = 0,
    reference = ufloat(0.0, 0.0),
    fit = 'exp',
):
    data = data.real
    ratio = data[:,:,0] # ratio of correlators; G(t)/G(t+1)
    G = data[:,:,1]     # raw data; G(t)

    dof, N = ratio.shape[1] -1, ratio.shape[0]
    ob = f'Observable (mass/A): one-parameter {fit}-fits with {mode} covariance matrix, d.o.f. = {dof} according to D&D'
    ratio_mean = np.nanmean(ratio, axis = 0)
    G_mean = np.nanmean(G, axis = 0)
    if cov is None:
        _, _, sqrt_mcovinv = covariance_matrix(ratio, mode = mode, sqrt = True, verbosity = verbosity)
    else:
        (_, _, sqrt_mcovinv) = cov[0]

    if verbosity >= 2: print(blue(ob))
    curve = get_curve(fit)

    # hypothesis function with one parameter
    # ratio, x=0 removes the exponential in front
    f = lambda nt, meff: curve(nt, 1.0, meff, x=0.0)/curve(nt+1, 1.0, meff, x=0.0)
    fit_params = lm.Parameters()
    fit_params.add('meff',
        value = 0.1 if reference.n == 0.0 else reference.n,
        min = 0.0,
        max = 100.0
    )

    # The norm squared of this function is exactly the chi-square-functional from the 
    # book D&D, section 9.2.1.
    # @param Parameters params object that holds the named fit-parameter to find
    # @param ndarray data ydata
    # @param 2darray scovinv sqrt of the inverse covariance matrix
    # @param 1darray frange fit range
    # @param callable model hypothesis function taking the fit-parameters and x-values
    # @return 1darray vector of residuals
    def residual(params, data, scovinv, frange, model):
        vals = params.valuesdict()
        return scovinv.dot(data - model(frange, vals['meff']))

    res = lm.minimize(residual, fit_params,
        method = 'lbfgsb',
        nan_policy = 'omit',
        kws = {
            'data': ratio_mean, # ydata
            'scovinv': sqrt_mcovinv, # sqrt of inverse covariance matrix
            'frange': fit_range, # fit range
            'model': f # model hypothesis function
        },
    )
    
    redchi_m = res.redchi.real
    if verbosity >= 2:
        print(lm.fit_report(res, modelpars = fit_params))
        print(format_chisq(redchi_m))

    it = iter(res.params.items())
    _, meff = next(it)
    meff = ufloat(np.abs(meff.value.real), np.abs(meff.stderr), "meff fit-error")

    if cov is None:
        mcov, mcovinv, sqrt_mcovinv = covariance_matrix(G, mode = mode, sqrt = True, verbosity = verbosity)
    else:
        (mcov, mcovinv, sqrt_mcovinv) = cov[1]

    f = lambda nt, A: curve(nt, A, meff.n) # @todo: take the error of meff into account
    fit_params = lm.Parameters()
    fit_params.add('A',
        value = 0.01,
        min = np.finfo(np.float64).eps,
        max = 10.0
    )

    def residual(params, data, scovinv, frange, model):
        vals = params.valuesdict()
        return scovinv.dot(data - model(frange, vals['A']))

    res = lm.minimize(residual, fit_params,
        method = 'lbfgsb',
        nan_policy = 'omit',
        kws = {
            'data': G_mean, # ydata
            'scovinv': sqrt_mcovinv, # sqrt of inverse covariance matrix
            'frange': fit_range, # fit range
            'model': f # model hypothesis function
        },
    )

    if verbosity >= 2:
        print(lm.fit_report(res, modelpars = fit_params))
        print(format_chisq(res.redchi.real))

    it = iter(res.params.items())
    _, A = next(it)
    A = ufloat(np.abs(A.value.real), np.abs(A.stderr), "amplitude A fit-error")
    if fit in ['exp_cosh', 'exp_sinh']:
        A *= (2*exp(-meff*fold_axis))

    result_tuple = (meff, A)
    info_dict = {
        'ob': ob,
        'redchi_m': redchi_m,
        'redchi_A': res.redchi.real,
        'lmfit_result': res
    }
    return result_tuple, info_dict

def mark_best_element(arr, mark, which, col):
    i = which(arr[:,col])
    arr[i,col] = mark(arr[i,col])
    return arr

def mark_elements(arr, mark, condition, col):
    for i, el in enumerate(arr[:,col]):
        try:
            cond = condition(el)
        except:
            cond = False

        if cond:
            arr[i,col] = mark(el)
    return arr

if __name__ == '__main__':
    parser = JsonArgumentParser(
        description = 'Plots vector vector correlators, mass-plateaus and extracts the mass and the amplitude.',
        formatter_class = argparse.RawTextHelpFormatter,
        exceptions = ['infile', 'outfile', 'a'],
        main_result = [
            'result.meff (jackknife).value&.4f?meff (jackknife)',
            'result.A (jackknife).value&.4e?A (jackknife)',
            'result.nconfig?#configs'
        ],
    )
    parser.add_argument("-v", "--verbosity", action="count", help="increase output verbosity", default=0)
    parser.add_argument('-n', '--name', help='name of the ensemble/plot (can contain latex)', default="Unknown ensemble")
    parser.add_argument('-a', help='value of the lattice constant in fm', type=lambda x: ufloat_fromstr(x, "lattice constant a"), default="0.0+/-0.0")
    parser.add_argument('--no-plot', help="don't show the plot", dest='noplot', default=False, action="store_true")
    parser.add_argument('-f', '--fit', help='\n'.join((
            'type of the fit, possible options are:',
            ' * exp: single exponential fit, ie: f(t) = A*exp(-mt)',
            ' * sinh: taking anti-periodicity into account, ie: f(t) = A*sinh(m(t-fold_axis))',
            ' * cosh: taking periodicity into account, ie: f(t) = A*cosh(m(t-fold_axis))',
            ' * exp_sinh: anti-periodicity and stability of the fit, ie: f(t) = 2A*exp(-mt)*sinh(m(t-fold_axis))',
            ' * exp_cosh: periodicity and stability of the fit, ie: f(t) = 2A*exp(-mt)*cosh(m(t-fold_axis))'
        )), choices=['exp', 'sinh', 'cosh', 'exp_cosh', 'exp_sinh'], default='exp')
    parser.add_argument('-p', '--parameter', help='How many parameters to fit, can be a list of 1 or 2. The last one is taken for the plots. (default: 1).', nargs='+', type=int, default=[1], choices = [1,2])
    parser.add_argument('-e', '--exclude', help='list of indices to exclude from the fits.', type=int, nargs='+', default=[])
    parser.add_argument('--nsrc', help='number of sources used', type=int, default=1)
    parser.add_argument('--show-all', help='show all data points in the plot', dest='show_all', default=False, action="store_true")
    parser.add_argument('--show-violin', help='also shows violin plots to check for the distribution', dest='show_violin', default=False, action="store_true")
    parser.add_argument('--fold',
        help = '\n'.join((
            'whether to fold aound the center to reduce some systematic errors or not. Possible values are:',
            ' * 0: no folding (default)',
            ' * 1: the obvious one, fold at 31.5 if N0=64, basically combine [0] and [63], [1] and [62], ..., [31] and [32]',
            ' * 2: remove first pt, middle counts twice, fold at 32 if N0=64, basically combine [1] and [63], [2] and [62], ..., [32] and [32]',
            ' * 3: remove last and middle pt, Anians method, basically combine [0] and [62], [1] and [61], ..., [30] and [32]',
            ' * 4: remove middle, first counts twice, basically combine [0] and [0], [1] and [63], ..., [31] and [31]'
        )),
        type = int,
        default = 0,
        choices = [0,1,2,3,4]
    )
    parser.add_argument('--fold-axis',
        help = '\n'.join((
            'Which point on the x-axis is the center point, where cosh is minimal or sinh switches sign. Default values are:',
            ' * if fold == 0: fold_axis = N0/2, eg: 32 if N0=64',
            ' * if fold == 1: fold_axis = (N0-1)/2, eg: 31.5 if N0=64',
            ' * if fold == 2: fold_axis = N0/2, eg: 32 if N0=64',
            ' * if fold == 3: fold_axis = N0/2, eg: 32 if N0=64',
            ' * if fold == 4: fold_axis = N0/2, eg: 32 if N0=64'
        )),
        type = float,
        dest = 'fold_axis',
    )

    parser.add_argument('-l', '--linear', help='plot the correlator in a linear instead of a log-plot', default=False, action="store_true")

    parser.add_argument('--ref', help='a reference value visible in the plot', type=lambda x: ufloat_fromstr(x, "reference value"), default="0.0+/-0.0")
    parser.add_argument('-j', '--jackknife', help='calculate the result using the jackknife method', default=False, action="store_true")
    parser.add_argument('-b', '--bootstrap', help='calculate the result using the bootstrap method. The integer value gives the number of bootstrap samples to take.', type=int, default=0)

    parser.add_argument('-m', '--mode', help='mode for the covariance matrix. Can be either full:n, diag:n,  or normalized:n, where n denotes the number of lowest eigenvalues to truncate. n can also be -1, which cut all negative eigenvalues automatically. (default: diag:0)', default='diag:0')
    parser.add_argument('--remove-leading-exp', help='leading exponential, A exp(-m x), to remove from the correlator', type=float, nargs=2, default=[0,0], metavar=('A', 'm'))

    required = parser.add_argument_group('required named arguments')
    required.add_argument('-r', '--range', help='multiple ranges for the fit [r1, r2], ..., [rn-1, rn], n must be even', type=int, nargs='+', required = True)
    required.add_argument('--vary-range', help='How much should the range be varied in order to obtain the systematic error that appears from its choice. Expects 3 numbers: A B C, A and B are the lower and upper end of the variation (including), and C denotes the minimal number of points that should be contained in a range.', type=int, nargs=3, metavar=('A', 'B', 'C'), default = None)
    required.add_argument('-i', '--in',
        help = 'input file(s) containing the extracted data (from meson.c). If multiple files are provided, statistical errors will be in the plot',
        dest = 'infile',
        metavar = 'FILE',
        required = True,
        nargs = '+'
    )

    required.add_argument('-o', '--out',
        help = 'output file(s) containing the text and plot data.',
        dest = 'outfile',
        metavar = 'FILE',
        default = ('-', '-'),
        nargs = 2
    )

    # parsing and setup of the arguments
    args = parser.parse_args()
    if args.verbosity:
        print("Running with the following arguments:")
        print(json.dumps(vars(args), default=str, indent = 4))


    # reading all the infiles
    G, (N0, N1, N2, N3), _, nconfig = read_infiles(args.infile, verbose = args.verbosity)

    if args.verbosity: print("G.shape = {0} (initial)".format(G.shape))

    # average all nsrc sources of a given config
    G, Gerr = avg_sources(G, args.nsrc, flatten = False)
    if args.verbosity: print("G.shape = {0} (after averaging over sources)".format(G.shape))

    # fold according to folding variant
    G, Gerr, _, G1, G2, r1, r2, fold_axis = fold(G, args.fold, N0, Gerr = Gerr)
    fold_axis = fold_axis if args.fold_axis == None else args.fold_axis
    if args.verbosity: print("G.shape = {0} (after folding)".format(G.shape))

    if args.verbosity: print("Removing leading exponential A={0}, m={1}".format(*args.remove_leading_exp))
    leading_exp = args.remove_leading_exp[0]*np.exp(-args.remove_leading_exp[1]*np.arange(G.shape[1]))
    for n in range(G.shape[0]):
        G[n] -= leading_exp

    X = np.arange(G.shape[1])
    G_mean, G_err = avg_sources(G, G.shape[0], Gerr = Gerr, flatten = True) # average all remaining configs
    nh = int(len(args.range)/2)

    # setup of the model functions
    curve = get_curve(args.fit)

    # setup the meff values for the plot
    if not args.noplot:
        meff = np.zeros(G.shape, dtype=np.float64)*np.NaN # fill with nans
        solve_func = lambda nt, meff, ratio: curve(nt, 1.0, meff, x=0.0)/curve(nt+1, 1.0, meff, x=0.0) - ratio.real
        xtol = 1e-8
        for i in tqdm(range(G.shape[0]), desc = f"fitting every config to {args.fit}"): # over all configs
            ratio = (G[i]/np.roll(G[i], -1)).real # G(t)/G(t+1)
            for nt in np.arange(G.shape[1]-1): # over all temporal lattice points
                if not (np.isnan(ratio[nt]) and np.isinf(ratio[nt])) and ratio[nt] != 0:
                    f = lambda meff: solve_func(nt, meff, ratio[nt])
                    res = sc.optimize.fsolve(f, 1.0, xtol=xtol)[0]
                    if args.fit in ['cosh', 'exp_cosh']: res = np.abs(res) # cosh is even
                    # @todo: what about the others > 1e-8
                    if np.abs(f(res)) <= 1e-8:
                        meff[i,nt] = res # only take reasonable results

        meff_mean = np.nanmean(meff, axis=0) if meff.shape[0] > 1 else meff[0]
        meff_err  = np.nanstd(meff, axis=0) if meff.shape[0] > 1 else np.zeros_like(meff_mean)

    ###################################################

    ratio = G.real/np.roll(G.real, -1, axis=1) # G(t)/G(t+1), eq 6.56 in G&L
    #logratio = np.log(ratio)

    result_dict = {
        "nconfig": int(nconfig/args.nsrc),
    }

    txt = []
    if args.a != 0.0:
        txt.append(f"a                                       = ({args.a:.6f}) fm\n")

    for parameter in args.parameter:

        # setup all data and ranges for the range-variation
        # and obtain the systematic error due to the choice of range
        if args.vary_range is not None:
            fit_ranges = []
            data_ranges = []
            for i in range(args.vary_range[0], args.vary_range[1]+1):
                lower = i+args.vary_range[2]-1
                upper = np.minimum(int(fold_axis), args.vary_range[1]+1)
                for j in range(lower, upper):
                    r = obtain_range([i,j], args.exclude)
                    fit_ranges.append(r)

                    if parameter == 1:
                        obs = 'observable_chi2_1par_DD'
                        data = np.zeros(G[:,r].shape + (2,))
                        data[:,:,0] = ratio[:,r]
                        data[:,:,1] = G[:,r]
                        data_ranges.append(data)

                    elif parameter == 2:
                        obs = 'observable_chi2_2par_DD'
                        data_ranges.append(G[:,r])
                        #data = G[:,r]

            observable_to_vary = lambda data, r: globals()[obs](data, r,
                mode = args.mode,
                verbosity = args.verbosity,
                reference = args.ref,
                fit = args.fit,
            )

            # extracts meff, A, and their reduced chi^2 values in a list
            def aggregate(i, retval, *parameters):
                meff, A = retval[0]
                r = parameters[1]
                redchi_m = retval[1]['redchi_m']
                redchi_A = retval[1]['redchi_A']
                return np.array([i+1, r, meff, meff.s/meff.n, A, A.s/A.n, redchi_m, redchi_A, np.sqrt((redchi_m-1)**2 + (redchi_A-1)**2)], dtype=object)

            # prints a table and returns the error of meff and A as ufloats
            def collect(retval):
                retval = np.array(retval, dtype=object)
                sigma_meff = np.nanstd([m.n for m in retval[:,2]])
                sigma_A = np.nanstd([m.n for m in retval[:,4]])
                
                retval = mark_best_element(retval, which=lambda x: np.argmin(np.abs(x-1)), mark=green, col=6)
                retval = mark_best_element(retval, which=lambda x: np.argmin(np.abs(x-1)), mark=green, col=7)
                retval = mark_best_element(retval, which=np.argmin, mark=green, col=8)
                retval = mark_best_element(retval, which=np.argmin, mark=green, col=3)
                retval = mark_best_element(retval, which=np.argmin, mark=green, col=5)
                retval = mark_elements(retval, condition=lambda x: 0.9 <= x <= 1.1, mark=yellow, col=6)
                retval = mark_elements(retval, condition=lambda x: 0.9 <= x <= 1.1, mark=yellow, col=7)

                text = tabulate.tabulate(retval,
                    headers=["#", "range", "meff", "meff_rel", "A", "A_rel", "chi_m", "chi_A", "norm"],
                    floatfmt=("d", "", "", ".2%", "", ".2%", ".4f", ".4f", ".4f"),
                )
                text += f"\nSystematic error coming from the choice of the range: {sigma_meff = }, {sigma_A = }"
                tag = "sys: choice of fit-range"
                return ufloat(0.0, sigma_meff, tag), ufloat(0.0, sigma_A, tag), text

            meff_range_err, A_range_err, table_range = vary_parameter(data_ranges, fit_ranges,
                f=observable_to_vary,
                vary_param="range",
                aggregate=aggregate,
                collect=collect
            )
        else:
            meff_range_err = ufloat(0.0, 0.0, "sys: choice of fit-range")
            A_range_err    = ufloat(0.0, 0.0, "sys: choice of fit-range")

        # @var list set of all point in the range, except the excluded ones
        r = obtain_range(args.range, args.exclude)

        if parameter == 1:
            # needs 2 covariance matrices, since we have 2 separate
            # one-parameter fits as well as 2 data sets; the ratio and the raw
            # correlator values
            obs = 'observable_chi2_1par_DD'
            data = np.zeros(G[:,r].shape + (2,))
            data[:,:,0] = ratio[:,r]
            data[:,:,1] = G[:,r]
            C1 = covariance_matrix(data[:,:,0], mode = args.mode, sqrt = True, verbosity = args.verbosity)
            C2 = covariance_matrix(data[:,:,1], mode = args.mode, sqrt = True, verbosity = args.verbosity)
            C = (C1, C2)

        elif parameter == 2:
            # only1  covariance matrix, for the correlated two-parameter fit
            obs = 'observable_chi2_2par_DD'
            data = G[:,r]
            C = covariance_matrix(data, mode = args.mode, sqrt = True, verbosity = args.verbosity)

        # @choice: with observable?
        observable = lambda data: globals()[obs](data, r,
            mode = args.mode,
            cov = C,
            verbosity = args.verbosity,
            reference = args.ref,
            fit = args.fit,
        )
    
        # calling of all the methods
        (amphys_mean, A_mean), info = observable(data)
        amphys_mean += meff_range_err
        A_mean += A_range_err
        result_dict['meff (error prop)'] = error_contributions_dict(amphys_mean, {'chisq': info['redchi_m']})
        result_dict['A (error prop)'] = error_contributions_dict(A_mean, {'chisq': info['redchi_A']})

        if args.jackknife:
            (amphys_jn, A_jn), info_jn = jackknife(data, observable)
            amphys_jn += meff_range_err
            A_jn += A_range_err
            chisq_jn_m = np.mean([ik['redchi_m'] for ik in info_jn])
            chisq_jn_A = np.mean([ik['redchi_A'] for ik in info_jn])
            result_dict['meff (jackknife)'] = error_contributions_dict(amphys_jn, {'chisq': chisq_jn_m})
            result_dict['A (jackknife)'] = error_contributions_dict(A_jn, {'chisq': chisq_jn_A})
        if args.bootstrap != 0:
            (amphys_bs, A_bs), info_bs = bootstrap(data, observable, args.bootstrap)
            amphys_bs += meff_range_err
            A_bs += A_range_err
            chisq_bs_m = np.mean([ik['redchi_m'] for ik in info_bs])
            chisq_bs_A = np.mean([ik['redchi_A'] for ik in info_bs])
            result_dict['meff (bootstrap)'] = error_contributions_dict(amphys_bs, {'chisq': chisq_bs_m})
            result_dict['A (bootstrap)'] = error_contributions_dict(A_bs, {'chisq': chisq_bs_A})

        # printing information from all the methods
        text = ''
        if 'ob' in info: text += info['ob'] + '\n'
        text += f"m_eff (error prop)                      = {amphys_mean:.6f} ({format_chisq(info['redchi_m'])})\n"
        if args.jackknife:
            text += f"m_eff (jackknife in fit region)         = {amphys_jn:.6f} ({format_chisq(chisq_jn_m)})\n"
        if args.bootstrap != 0:
            text += f"m_eff (bootstrap in fit region, K={args.bootstrap:>4}) = {amphys_bs:.6f} ({format_chisq(chisq_bs_m)})\n"
        if not np.isnan(A_mean.n):
            text += f"A (error prop)                          = {A_mean:.6e} ({format_chisq(info['redchi_A'])})\n"
            if args.jackknife:
                text += f"A (jackknife in fit region)             = {A_jn:.6e} ({format_chisq(chisq_jn_A)})\n"
            if args.bootstrap != 0:
                text += f"A (bootstrap in fit region, K={args.bootstrap:>4})     = {A_bs:.6e} ({format_chisq(chisq_bs_A)})\n"

        if args.ref != 0.0:
            text += f"m_eff (reference value)                 = {args.ref:.6f}\n"

        if args.a.n != 0.0:
            mphys = np.abs(amphys_mean/(args.a/197.327))
            text += f"m_phys (error prop)                     = ({mphys:.6f}) MeV\n"
            if args.jackknife:
                mphys_jn = np.abs(amphys_jn/(args.a/197.327))
                text += f"m_phys (jackknife)                      = ({mphys_jn:.6f}) MeV\n"
            if args.bootstrap != 0:
                mphys_bs = np.abs(amphys_bs/(args.a/197.327))
                text += f"m_phys (bootstrap, K={args.bootstrap:>4})              = ({mphys_bs:.6f}) MeV\n"

        if args.ref != 0.0:
            text += "discrepancy (compared to error prop)    = {0} sigma ({1})\n".format(
                format_sigma(np.abs(amphys_mean.n-args.ref.n)/np.sqrt(args.ref.s**2 + amphys_mean.s**2), '{0:.1f}'),
                (red('no overlap') if np.abs(amphys_mean.n-args.ref.n) > args.ref.s+amphys_mean.s else green('overlap')) + ' with ref.')
            if args.jackknife:
                text += "discrepancy (compared to jackknife)     = {0} sigma ({1})\n".format(
                    format_sigma(np.abs(amphys_jn.n-args.ref.n)/np.sqrt(args.ref.s**2 + amphys_jn.s**2), '{0:.1f}'),
                    (red('no overlap') if np.abs(amphys_jn.n-args.ref.n) > args.ref.s+amphys_jn.s else green('overlap')) + ' with ref.')
            if args.bootstrap != 0:
                text += "discrepancy (compared to bootstrap)     = {0} sigma ({1})\n".format(
                    format_sigma(np.abs(amphys_bs.n-args.ref.n)/np.sqrt(args.ref.s**2 + amphys_bs.s**2), '{0:.1f}'),
                    (red('no overlap') if np.abs(amphys_bs.n-args.ref.n) > args.ref.s+amphys_bs.s else green('overlap')) + ' with ref.')
        txt.append(text)

        if args.jackknife:
            plot_am, plot_A = amphys_jn, A_jn
        elif args.bootstrap != 0:
            plot_am, plot_A = amphys_bs, A_bs
        else:
            plot_am, plot_A = amphys_mean, A_mean

        if args.fit == 'exp':
            plot_fit = lambda nt: [plot_A*exp(-plot_am*x) for x in nt]
        elif args.fit in ['cosh', 'exp_cosh']:
            plot_fit = lambda nt: [plot_A*cosh(clip(((x - fold_axis)*plot_am), a_min = -700, a_max = 700)) for x in nt]
        elif args.fit in ['sinh', 'exp_sinh']:
            plot_fit = lambda nt: [plot_A*sinh((x - fold_axis)*plot_am) for x in nt]


    title = r'{name} ({nconfig} config(s), {nsrc} sources each, {fit}-fit, fit-range: {rng})'.format(
        name = bold(args.name),
        nconfig = int(nconfig/args.nsrc),
        nsrc = args.nsrc,
        fit = args.fit,
        rng = np.array2string(np.array(r), threshold = 3, edgeitems = 1)
    )

    _, _, fprint = get_target(args.outfile, '.txt')
    if args.vary_range is not None:
        fprint("Variation table for fit-range")
        fprint(table_range, end='\n\n')

    fprint(box(txt, title = title))
    fprint(error_contributions(amphys_mean, 'amphys (error prop)'))
    fprint(error_contributions(A_mean, 'A (error prop)'))
    if args.jackknife:
        fprint(error_contributions(amphys_jn, 'amphys (jackknife)'))
        fprint(error_contributions(A_jn, 'A (jackknife)'))
    if args.bootstrap != 0:
        fprint(error_contributions(amphys_bs, f'amphys (bootstrap, K={args.bootstrap})'))
        fprint(error_contributions(A_bs, f'A (bootstrap, K={args.bootstrap})'))

    meta = parser.set_result(result_dict)
    fprint(f"This took {meta['duration']:.2f} seconds.")

    ####################################################################################################
    ####################################### Plotting starts here #######################################
    ####################################################################################################
    if args.noplot: exit()

    fig = plt.figure()

    gs = fig.add_gridspec(2, 1, height_ratios = [3,1])
    ax = fig.add_subplot(gs[0, 0])
    fig.set_size_inches(18.5*0.8, 10.5*0.8, forward=True)
    ax.set_title(clean(title))

    if args.fit == 'exp':
        label_fit = r'effective mass fit (+error), fit function $f(x) = A \exp(-m_{{eff}}x)$'
    elif args.fit in ['sinh', 'exp_sinh']:
        label_fit = r'effective mass fit (+error), fit function $f(x) = A \sinh((x - N0/2)m_{{eff}})$'
    elif args.fit in ['cosh', 'exp_cosh']:
        label_fit = r'effective mass fit (+error), fit function $f(x) = A \cosh((x - N0/2)m_{{eff}})$'

    text = re.sub(r"\ +", " ", text) # multiple spaces -> one space
    text = re.sub(r"\x1b\[([0-9]+)m", "", text) # remove the colors, eg: \x1b[91mtest\x1b[0m -> test

    ax.annotate(text, xy=(0, -0.1), xytext=(12, -12), va='top',
                xycoords='axes fraction', textcoords='offset points',
                bbox={'facecolor': 'red', 'alpha': 0.5, 'pad': 10})

    ax2 = ax.twinx()  # instantiate a second axis that shares the same x-axis
    axins = inset_axes(ax2,
        width = "100%",
        height = "100%",
        bbox_to_anchor = (1.0-0.6, -0.1-0.4, 0.6, 0.4), # (left, bottom, width, height)
        bbox_transform = ax2.transAxes,
        loc = upper_left,
        borderpad = 0
    )
    axes = [ax2, axins]
    included = np.setdiff1d(np.arange(0, X.shape[0]), args.exclude)

    total = len(axes)*nh + 2 + len(axes)*2
    total += G.shape[0]+len(axes)*meff.shape[0] if args.show_all else 0
    total += 1+len(axes)*len(X[included]) if args.show_violin else 0
    total += 2 if args.fold != 0 else 0
    total += len(axes) if args.ref != 0.0 else 0

    bar = tqdm(total = total)
    bar.set_description("plotting")

    for ri in range(nh):
        for axis in axes:
            eps = 0.1
            label = 'fit region'
            axis.axvspan(args.range[2*ri]-eps, args.range[2*ri+1]+eps,
                alpha = 0.1,
                color = 'green',
                label = label if ri == 0 and axis == ax2 else None
            )
            bar.update()

    if args.show_all:
        for i in range(G.shape[0]):
            label = 'correlator for all configs'
            ax.plot(X, G[i].real, 'o',
                color = 'tab:red',
                alpha = 0.1,
                label = label if i == 0 else None
            )
            bar.update()

        for j, axis in enumerate(axes):
            label = '$m_{{eff}}$ for all configs (ignoring those with error > {0:.2e})'.format(xtol)
            for i in range(meff.shape[0]):
                axis.plot(X[included], meff[i,included], '.',
                    color = 'tab:blue',
                    alpha = 0.1,
                    label = label if i == 0 and axis == ax2 else None
                )
                bar.update()

    if args.show_violin:
        parts = ax.violinplot(G.real, X, 
            widths = 0.6,
            showmeans = False,
            showextrema = False,
            showmedians = False
        )
        bar.update()
        for body in parts['bodies']:
            body.set_facecolor('tab:red')
            body.set_edgecolor('tab:red')
            body.set_alpha(0.1)

        for axis in axes:
            for i in X[included]:
                # remove all nans from the array
                mask = np.logical_not(np.isnan(meff[:,i]))
                # only if non-empty
                if len(meff[mask,i]) > 0:
                    parts = axis.violinplot([meff[mask,i]], [X[i]], 
                        widths = 0.5,
                        showmeans = False,
                        showextrema = False,
                        showmedians = False
                    )
                    for body in parts['bodies']:
                        body.set_facecolor('tab:blue')
                        body.set_edgecolor('tab:blue')
                        body.set_alpha(0.1)
                bar.update()

    # left and right contribution to G mean
    if args.fold != 0:
        X = np.arange(len(r1))
        (_, caps, _) = ax.errorbar(X, np.mean(G1.real, axis=0),
            xerr = 0,
            yerr = np.nanstd(G1.real, axis=0),
            fmt = '.:',
            color = 'green',
            label = 'mean of correlator (first half, --fold {0})'.format(args.fold),
            elinewidth = 2,
            capsize = 6
        )
        for cap in caps: cap.set_markeredgewidth(2)
        bar.update()
        (_, caps, _) = ax.errorbar(X, np.mean(G2.real, axis=0),
            xerr = 0,
            yerr = np.nanstd(G2.real, axis=0),
            fmt = '.:',
            color = 'green',
            label = 'mean of correlator (second half --fold {0})'.format(args.fold),
            elinewidth = 2,
            capsize = 6
        )
        for cap in caps: cap.set_markeredgewidth(2)
        bar.update()

    # G mean plot
    ax.errorbar(X, G_mean.real,
        xerr = 0,
        yerr = G_err.flatten(),
        fmt = 'o-',
        color = 'red',
        label = 'mean of correlator{0}'.format(' (folded)' if args.fold != 0 else ''),
        capsize = 3
    )
    bar.update()

    if not args.linear: ax.set_yscale("log")

    # fit plot
    x = np.linspace(0, N0+2, 500)
    Y = plot_fit(x)
    ax.autoscale(False)
    ax.plot(x, [y.n for y in Y], color = 'black', label = label_fit)
    ax.fill_between(x, [y.n-y.s for y in Y], [y.n+y.s for y in Y], alpha = 0.2, label = label_fit)
    bar.update()

    # meff mean plot
    for axis in axes:
        axis.errorbar(X[included], meff_mean[included],
            xerr = None,
            yerr = meff_err[included],
            fmt = 'x',
            color = 'blue',
            label = '$m_{{eff}}$, {0}-fit'.format(args.fit) if axis == ax2 else None,
            capsize = 6
        )
        bar.update()

    # meff fit value
    for axis in axes:
        axis.plot([0, G.shape[1]], [plot_am.n,plot_am.n], color = 'black', label = label_fit)
        stderr = 0.0 if np.isnan(plot_am.s) else plot_am.s
        axis.fill_between([0, G.shape[1]], plot_am.n - stderr, plot_am.n + stderr, alpha = 0.2, label = label_fit)
        bar.update()

    # meff reference value
    if args.ref != 0.0:
        for axis in axes:
            label = 'Reference value (+ error)' if axis == ax2 else None
            axis.plot([0, G.shape[1]], [args.ref.n, args.ref.n], '-.',
                color = 'black',
                label = label
            )
            axis.fill_between([0, G.shape[1]], args.ref.n - args.ref.s, args.ref.n + args.ref.s, alpha = 0.2, label = label)
            bar.update()

    bar.close()

    # axis labels
    ax.set_xlabel(r'$n_0$')
    ax.set_ylabel(r'$\Re(G(a n_0))$', color='tab:red')
    ax.tick_params(axis='y', labelcolor='tab:red')
    axins.yaxis.set_label_position("right")
    axins.yaxis.tick_right()
    axins.set_xlabel(r'$n_0$')
    for axis in axes:
        axis.set_ylabel('$m_{eff}$', color='tab:blue')
        axis.tick_params(axis='y', labelcolor='tab:blue')
        axis.xaxis.set_major_locator(MaxNLocator(integer=True)) # only integers

    for axis in [ax, ax2]:
        axis.set_xlim(0, G.shape[1])

    # sub region of the original image
    x1, x2 = limits(args.range, epsilon = 1.1, no_zeros = False)
    y1, y2 = limits(meff_mean[r] - meff_err[r], meff_mean[r] + meff_err[r], args.ref, plot_am, shift = 0.1)
    axins.set_xlim(x1, x2)
    axins.set_ylim(y1, y2)

    # draw a bbox of the region of the inset axes in the parent axes and
    # connecting lines between the bbox and the inset axes area
    mark_inset(ax2, axins, loc1=upper_left, loc2=upper_right, fc="none", ec="0.5")

    h, l = compactify_legend([ax, ax2])
    fig.legend(h, l)

    target, file, _ = get_target(args.outfile, '.pdf')
    if file:
        plt.savefig(target)
    else:
        plt.draw()
        plt.show()
