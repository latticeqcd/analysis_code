#!/usr/bin/env python3
#
# Edit correlators.
#

import argparse
import numpy as np
import json
import logging
from tqdm import tqdm
import gauge_noise as gn
import difflib
import os

logger = logging.getLogger("utils")

def create_fname(file1, file2, op="+", outname=None):
    s = difflib.SequenceMatcher(None, file1, file2)
    fname = ""
    for pre, curr, _ in gn.previous_and_next(s.get_matching_blocks()[:-1]):
        if (pre is not None):
            if outname is None:
                fname += f"({file1[pre.a+pre.size:curr.a]}{op}{file2[pre.b+pre.size:curr.b]})"
            else:
                fname += outname
        fname += file1[curr.a:curr.a+curr.size]
    return fname

def main():
    parser = argparse.ArgumentParser(description = 'Edit correlator(s).')
    parser.add_argument("-v", "--verbosity", action="count", help="increase output verbosity", default=0)
    parser.add_argument('--op', help="operator", default="+", type=str, choices=['+'],)
    parser.add_argument('-o', '--outname', help='Replacement for output file name(s) (Example: if outname="ABC", then n1f3.dat + n2f4.dat -> nABCfABC.dat, if outname=None, then n1f3.dat + n2f4.dat -> n(1+2)f(3+4).dat).', type=str, default=None)
    parser.add_argument('-d', '--dry-run', help="Don't do anything, just show (use -v to see waht would happen)", default=False, action="store_true")
    parser.add_argument('-f', '--force', help="Force overwriting existing file(s)", default=False, action="store_true")

    required = parser.add_argument_group('required named arguments')
    required.add_argument('-i1', '--infile1', help='input file(s).', metavar='FILE', required=True, nargs='+')
    required.add_argument('-i2', '--infile2', help='input file(s).', metavar='FILE', required=True, nargs='+')
    required.add_argument('--outdir', help='Directory for output file(s).', metavar='DIR', required=True)

    args = parser.parse_args()
    logger.setLevel(logging.DEBUG if args.verbosity else logging.INFO)
    ch = logging.StreamHandler()
    ch.setFormatter(logging.Formatter('[%(funcName)s] %(message)s'))
    logger.addHandler(ch) # add handlers to logger

    logger.debug("Running with the following arguments:")
    logger.debug(json.dumps(vars(args), indent=4))

    infiles1 = gn.infile_list(args.infile1)
    infiles2 = gn.infile_list(args.infile2)
    if len(infiles1) != len(infiles2):
        raise ValueError(f"-i1 and -i2 need to have the same amount of files")
    for file1, file2 in tqdm(zip(infiles1, infiles2), desc=f"processing dat-files"):
        N1 = gn.read_coords(file1)
        N2 = gn.read_coords(file2)
        if N1 != N2:
            raise ValueError(f"Lattice dimensions do not coincide ({N1 = }, {N2 = })")
        if args.op == "+":
            outfile = create_fname(os.path.basename(file1), os.path.basename(file2), args.op, args.outname)
            outfile = os.path.abspath(f"{args.outdir}/{outfile}")
            logger.debug(f"Creating file as addition:\n * {file1}\n {args.op} {file2}\n = {outfile}")
            if not args.dry_run:
                if not args.force and os.path.isfile(outfile):
                    raise ValueError(f"File {outfile} already exists")
                gn.write_dat(gn.read_dat(file1) + gn.read_dat(file2), N1, outfile)

if __name__ == '__main__':
    main()