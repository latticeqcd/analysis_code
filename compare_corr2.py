#!/usr/bin/env python3
#
# Compare correlators.
#

import argparse
import numpy as np
import scipy as sc
import json # json.dumps()
import logging
from tqdm import tqdm
import gauge_noise as gn
import matplotlib.pyplot as plt
import shlex
import sys
import os
from itertools import zip_longest, combinations
from uncertainties import ufloat, unumpy
from termcolor import colored

logger = logging.getLogger("utils")

def show(G, desc):
    for n in range(len(G)):
        dtype = G[n].dtype.name
        logger.debug(f"G[{n}].shape = {G[n].shape} ({dtype=}) ({desc})")

def get_plot_layout(nplots):
    if nplots in [4]:
        fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
        axes = [ax1, ax2, ax3, ax4]
    elif nplots in [5, 6]:
        fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(2, 3)
        axes = [ax1, ax2, ax3, ax4, ax5, ax6]
    elif nplots in [3]:
        fig = plt.figure()
        ax1 = fig.add_subplot(2, 2, (1,3))
        ax2 = fig.add_subplot(2, 2, 2)
        ax3 = fig.add_subplot(2, 2, 4)
        axes = [ax1, ax2, ax3]
    elif nplots == 1:
        fig, axes = plt.subplots(1, 1)
        axes = [axes]
    elif nplots > 0:
        fig, axes = plt.subplots(1, nplots)
    return fig, axes

def plot(ax, Y, add_shift = 0.0, **kwargs):
    X = np.arange(Y.shape[0]) + plot.shift*(-1.0)**plot.counter
    plot.shift += add_shift
    plot.counter += 1
    if not np.any(unumpy.std_devs(Y)):
        ax.plot(X, unumpy.nominal_values(np.real(Y)), **kwargs)
    else:
        opts = {
            'xerr': 0,
            'yerr': unumpy.std_devs(Y),
            'linewidth': 2,
            'capsize': 3,
            'alpha': 0.5
        }
        opts = dict(list(opts.items()) + list(kwargs.items()))
        ax.errorbar(X, unumpy.nominal_values(np.real(Y)), **opts)
plot.shift = 0.0
plot.counter = 0

def mean_sem(arr, **kwargs):
    return unumpy.uarray(np.mean(arr, **kwargs), sc.stats.sem(arr, **kwargs))
    #return gn.collapse(np.array(list(map(gn.ufloat_, zip(np.atleast_1d(np.mean(arr, **kwargs)), np.atleast_1d(sc.stats.sem(arr, **kwargs)))))))

def mean_std(arr, **kwargs):
    return unumpy.uarray(np.mean(arr, **kwargs), np.nanstd(arr, **kwargs))
    #return gn.collapse(np.array(list(map(gn.ufloat_, zip(np.atleast_1d(np.mean(arr, **kwargs)), np.atleast_1d(np.nanstd(arr, **kwargs)))))))

def rel_err(G):
    return unumpy.std_devs(G)/np.abs(unumpy.nominal_values(G))

def var(G):
    return unumpy.std_devs(G)**2

def err(G):
    return unumpy.std_devs(G)

def main():

    pre_parser = argparse.ArgumentParser(add_help=False)
    pre_parser.add_argument('-N', default=2, type=int)
    pre_args = pre_parser.parse_known_args()[0]
    N = pre_args.N

    parser = argparse.ArgumentParser(description = 'Compare and plot correlator(s).')
    parser.add_argument("-N", help="number of correlators to compare", type=int, default=N)
    parser.add_argument("-v", "--verbosity", action="count", help="increase output verbosity", default=0)
    parser.add_argument('--fold', help="fold the correlator(s)", default=False, action="store_true")
    parser.add_argument('-r', '--resampling', help="resampling method to use", default="jackknife", type=str, choices=['jackknife', 'std'],)
    parser.add_argument('-o', '--outfile', help='Output filename', default=None)
    parser.add_argument('--plot-rel-err', help="plot the relative errors", default=False, action="store_true")
    parser.add_argument('--plot-var', help="plot the variances", default=False, action="store_true")
    parser.add_argument('--nsrc', nargs=N, help='Limit the number of sources for the correlator, if not given or given 0, the program tries to determine itself', type=int, default=[0]*N)
    parser.add_argument('--nconfigs', nargs=N, help='Limit the number of configs for the correlator, if not given or given 0, the program tries to determine itself', type=int, default=[0]*N)
    parser.add_argument('-t', '--title', help='Plot title', default=None)
    parser.add_argument('-n', '--names', nargs=N, help='names of the ensembles/plots (can contain latex)', default=["Unknown ensemble #{0}".format(x) for x in range(N)])
    parser.add_argument('-c', '--construct', help='construct a new correlator by combining. Eg. "G[0]+G[1]" "name" adds the two correlators given by -i1 and -i2. It expect parameters in the format <expression> <string>', type=str, action='append', nargs=2, metavar=('corr_exp','name'), default=[])
    parser.add_argument('-a', '--additional-plot', help='add another plot by combining. Eg. "G[0]/G[1]" "name" divides the two correlators given by -i1 and -i2. It expects parameters in the format <expression> <string>', type=str, action='append', nargs=3, metavar=('plot_exp','name', 'color'), default=[])
    parser.add_argument('--plot-only', nargs='+', help='plot only these correlators', metavar='n', dest='plot_only', type=int, default=[])
    parser.add_argument('--plot-gauge-noise', nargs='+', help='plot gauge noise as well', metavar='FILE', default=[])

    required = parser.add_argument_group('required named arguments')
    for i in range(N):
        required.add_argument('-i{0}'.format(i), '--infile{0}'.format(i),
            help = 'input file(s) for ensemble {0} containing the extracted data (from meson.c).'.format(i),
            dest = 'infile{0}'.format(i),
            metavar = 'FILE',
            required = True,
            nargs = '+'
        )

    args = parser.parse_args()
    logger.setLevel(logging.DEBUG if args.verbosity else logging.INFO)
    ch = logging.StreamHandler()
    ch.setFormatter(logging.Formatter('[%(funcName)s] %(message)s'))
    logger.addHandler(ch) # add handlers to logger

    logger.debug("Running with the following arguments:")
    logger.debug(json.dumps(vars(args), indent=4))

    if args.resampling == 'std':
        method = lambda data, fn, **kwargs: fn(data)
    elif args.resampling == 'jackknife':
        method = gn.jackknife

    G = []
    G_var = {}
    src_indices = {}
    for i in range(N):
        infiles = gn.infile_list(getattr(args, "infile{0}".format(i)))
        N0, _, _, _ = gn.read_coords(infiles[0])
        N0h = int(N0/2)
        infiles, nsrc, nconfigs, run_name = gn.group_by_configs(infiles)
        args.nsrc[i] = nsrc if args.nsrc[i] == 0 else args.nsrc[i]
        args.nconfigs[i] = nconfigs if args.nconfigs[i] == 0 else args.nconfigs[i]
        G.append(np.zeros((args.nconfigs[i], args.nsrc[i], N0), dtype=np.complex128))

        if not (nsrc,args.nsrc[i]) in src_indices:
            src_indices[(nsrc,args.nsrc[i])] = np.random.choice(np.arange(nsrc), size=args.nsrc[i], replace=False)
        #src_indices = np.arange(args.nsrc[i])
        # print(f"{nsrc = }")
        # print(f"{args.nsrc[i] = }")
        # print(f"{src_indices = }")
        # print(f"{len(src_indices) = }")
        # exit()
        for n, (cfg, files) in tqdm(enumerate(infiles.items()), desc=f"reading dat-files for correlator {i}", total=len(infiles)):
            if n < args.nconfigs[i]:
                k = 0
                for m, file in enumerate(files):
                    # if m < args.nsrc[i]:
                    #     G[i][n,m] = gn.read_dat(file)
                    if m in src_indices[(nsrc,args.nsrc[i])]:
                        G[i][n,k] = gn.read_dat(file)
                        k += 1

    if args.plot_gauge_noise:
        gn_infiles = gn.infile_list(args.plot_gauge_noise)
        gn_infiles, nsrc, nconfigs, run_name = gn.group_by_configs(gn_infiles)
        GN = np.zeros((nconfigs, nsrc, N0), dtype=np.complex128)
        for n, (cfg, files) in tqdm(enumerate(gn_infiles.items()), desc="reading dat-files for gauge-noise", total=len(gn_infiles)):
            for m, file in enumerate(files):
                GN[n,m] = gn.read_dat(file)


    show(G, "initial")
    if args.fold:
        for n in range(N):
            G[n] = gn.fold(G[n])
        if args.plot_gauge_noise:
            GN = gn.fold(GN)
        show(G, "after folding")

    if args.plot_gauge_noise:
        sigma_vol = method(unumpy.nominal_values(np.real(GN)), gn.gauge_variance, axis=0,
            bar=gn.bar(desc=f"calculating sigma_vol"))

    for n in range(N):
        G[n] = np.mean(G[n], axis=1) # average over sources
    show(G, "after averaging over sources")

    for i, (corr_exp, name) in enumerate(args.construct):
        logger.debug(f"evaluating G[{N}] = {corr_exp} (name {name})")
        G.append(eval(corr_exp))
        args.names.append(name)
        N += 1
    show(G, "after constructing")

    for n in range(N):
        G_var[n] = np.var(G[n], axis=0)

    for n in range(N):
        # a = gn.jackknife(np.real(G[n]), lambda d: np.mean(d, axis=0))
        # b = mean_sem(np.real(G[n]), axis=0)
        # print(unumpy.nominal_values(a)-unumpy.nominal_values(b))
        # print(unumpy.std_devs(a)-unumpy.std_devs(b))
        # exit()
        G[n] = method(np.real(G[n]), lambda d: mean_sem(d, axis=0))
        #G[n] = method(np.real(G[n]), lambda d: np.mean(d, axis=0))
        #G[n] = gn.jackknife(np.real(G[n]), lambda d: np.mean(d, axis=0))
        #G[n] = mean_sem(np.real(G[n]), axis=0)
    show(G, "after final averaging")

    #G[-1] /= 1500.0

    for n,m in combinations(range(N), 2):
        Gn = unumpy.nominal_values(G[n])
        Gm = unumpy.nominal_values(G[m])
        eps = 100*np.finfo(Gn.dtype).eps

        if np.allclose(Gn, Gm, rtol=eps, atol=0.0):
            logger.debug(colored(f"G[{n}] and G[{m}] are equal up to {eps}", "green"))
        else:
            diff = abs((Gn-Gm)/Gm)
            logger.debug(colored(f"G[{n}] and G[{m}] differ relatively by avg: {np.mean(diff):.2e} (min: {diff.min():.2e}, max: {diff.max():.2e})", "red"))


    print(f"{G[0]/G[-1] = }")
    print(f"{abs((G[0]-G[-1])/G[-1]) = }")

    print(f"{np.sum(G[0]) = }")
    print(f"{np.sum(G[-1]) = }")
    print(f"{np.sum(G[0]) - np.sum(G[-1]) = }")

    G_plot = {}
    for i, (plot_exp, name, fmt) in enumerate(args.additional_plot):
        logger.debug(f"evaluating G_plot[{name}] = {plot_exp} (name {name})")
        G_plot[name] = np.array(eval(plot_exp))

    plots = 1
    corr = 0
    if args.plot_rel_err:
        rel = plots
        plots += 1
    if args.plot_var or args.plot_gauge_noise:
        avar = plots
        plots += 1
    if G_plot:
        addp = plots
        plots += 1

    if plots > 0:
        plt_kwargs_titles = {'fontsize': 20}
        plt_kwargs_labels = {'fontsize': 20}
        plt_kwargs_legend = {'fontsize': 12}

    fig, axes = get_plot_layout(plots)
    fig.set_size_inches(18.5, 10.5, forward=True)

    plt.title(args.title)
    for n, (name, nconf, nsrc) in enumerate(zip_longest(args.names, args.nconfigs, args.nsrc)):
        if not n in args.plot_only:
            continue

        label = fr"G[{n}], {name}, {nconf} conf, {nsrc} src"
        print(f"plotting {label}")
        plot(axes[corr], G[n], label=label)

        if args.plot_rel_err:
            plot(axes[rel], rel_err(G[n]), label=label)

        if args.plot_var:
            #plot(axes[avar], var(G[n]), label=label)
            plot(axes[avar], G_var[n])

    if args.plot_gauge_noise:
        plot(axes[avar], sigma_vol, label="gauge variance")

    for i, (plot_exp, name, fmt) in enumerate(args.additional_plot):
        axes[addp].plot(np.arange(G_plot[name].shape[0]), np.real(G_plot[name]), fmt, label=name)

    if args.plot_rel_err:
        axes[rel].axhline(y=1.0, color='black', linestyle='-')
        axes[rel].set_xlabel(r'$x_0$', **plt_kwargs_labels)
        axes[rel].set_ylabel(r'$\Delta G_i / G_i$', **plt_kwargs_labels)
        axes[rel].set_title(r'Relative error(s) on G', **plt_kwargs_titles)
        axes[rel].set_yscale("log")

    if args.plot_var or args.plot_gauge_noise:
        axes[avar].set_xlabel(r'$x_0$', **plt_kwargs_labels)
        axes[avar].set_ylabel(r'$var(G_i)$', **plt_kwargs_labels)
        axes[avar].set_title(r'Variance(s) on G', **plt_kwargs_titles)
        axes[avar].set_yscale("log")
        axes[avar].legend(**plt_kwargs_legend)

    if args.additional_plot:
        axes[addp].set_title(r'Additional plot', **plt_kwargs_titles)
        axes[addp].set_yscale('log')
        axes[addp].legend()

    axes[corr].set_yscale('log')
    axes[corr].legend(**plt_kwargs_legend)
    fig.tight_layout()

    if args.outfile is None:
        logger.debug(f"Showing plot")
        plt.draw()
        plt.show()
    else:
        logger.debug(f"Storing {args.outfile}")
        plt.savefig(args.outfile, dpi=fig.dpi, metadata={
            'Keywords': shlex.join(sys.argv),
            'Creator': os.getcwd(),
        })

if __name__ == '__main__':
    main()